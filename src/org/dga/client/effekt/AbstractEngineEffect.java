/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.effekt;

import com.jme3.scene.Node;
import org.dga.client.AbstractDgaJmeEffect;
import org.dga.client.DgaJmeAction;

/**
 * The abstract implementation of effects provided by an engine or motor that is 
 * a machine designed to convert one form of energy into mechanical energy.
 * 
 * @extends AbstractDgaJmeEffect
 * @implements EngineEffect
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 26.08.2018
 */
public abstract class AbstractEngineEffect extends AbstractDgaJmeEffect implements EngineEffect {
    private static final DgaJmeAction ACTION_ = DgaJmeAction.ENGINE;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new instance.
     */
    public AbstractEngineEffect() {
        super();
    }
    
    /**
     * The constructor creates a new instance with the supplied properties.
     * 
     * @param engineEffectNode The engine effect's node.
     */
    public AbstractEngineEffect(final Node engineEffectNode) {
        super(engineEffectNode);
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the action this effect is intended for.
     * 
     * @return DgaJmeAction The action this effect is intended for.
     */
    @Override
    public DgaJmeAction getAction() {
        return ACTION_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}