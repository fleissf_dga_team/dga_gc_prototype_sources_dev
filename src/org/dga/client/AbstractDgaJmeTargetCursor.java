/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The abstract implementation of a target cursor for game characters.
 *
 * @extends AbstractDgaJmeSpatial
 * @implements DgaJmeTargetCursor
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 17.7.2018
 */
public abstract class AbstractDgaJmeTargetCursor extends AbstractDgaJmeSpatialable 
    implements DgaJmeTargetCursor {
    private float targetOffset_ = 0f;
    private final Map<DgaJmeModel, Float> ATTACHED_EQUIPMENT_ = 
        Collections.synchronizedMap(new HashMap<>());
    private final Map<DgaJmeModel, Float> DISTANT_EQUIPMENT_ = 
        Collections.synchronizedMap(new HashMap<>());
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new target cursor with the supplied properties.
     *
     * @param targetModel The target cursor's model.
     */
    public AbstractDgaJmeTargetCursor(final DgaJmeTargetCursorModel targetModel) {
        super(targetModel.getSpatial(), targetModel.getScale());
        targetOffset_ = targetModel.getOffset();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method sets an offset distance for the target cursor.
     *
     * @param targetOffset An offset distance for the target cursor.
     */
    @Override
    public void setOffset(final float targetOffset) {
        targetOffset_ = targetOffset;
    }

    /**
     * The method returns the target cursor's offset distance.
     *
     * @return float The target cursor's offset distance.
     */
    @Override
    public float getOffset() {
        return targetOffset_;
    }
    //
    // *************************************************************************
    //
    /**
     * The method adds attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @param attachedEquipment Supplementary equipment attached to the game object.
     */
    @Override
    public void addAttachedEquipment(final DgaJmeModel... attachedEquipment) {
        if (attachedEquipment.length > 0) {
            for (DgaJmeModel e_ : attachedEquipment) {
                ATTACHED_EQUIPMENT_.put(e_, 0f);
            }
        }
    }
    
//    /**
//     * The method adds attached equipment mounted on the game object with the 
//     * distance between the game object and the equipment. Attached equipment 
//     * includes all supplementary objects immediately adjoined to the main object.
//     * 
//     * @param attachedEquipment Supplementary equipment attached to the game 
//     * object with the distance between the game object and the equipment.
//     */
//    @Override
//    public void addAttachedEquipment(final Map.Entry<DgaJmeModel, Float>... attachedEquipment) {
//        if (attachedEquipment.length > 0) {
//            for (Map.Entry<DgaJmeModel, Float> e_ : attachedEquipment) {
//                ATTACHED_EQUIPMENT_.put(e_.getKey(), e_.getValue());
//            }
//        }
//    }
    
    /**
     * The method adds distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @param distantEquipment Supplementary equipment tied to the game object.
     */
    @Override
    public void addDistantEquipment(final DgaJmeModel... distantEquipment) {
        if (distantEquipment.length > 0) {
            for (DgaJmeModel e_ : distantEquipment) {
                DISTANT_EQUIPMENT_.put(e_, 0f);
            }
        }
    }
    
//    /**
//     * The method adds distant equipment tied to the game object with the 
//     * distance between the game object and the equipment. Distant equipment 
//     * includes all supplementary objects not attached to the main object but 
//     * moving with it and rotating around it.
//     *
//     * @param distantEquipment Supplementary equipment tied to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    @Override
//    public void addDistantEquipment(
//        final Map.Entry<DgaJmeModel, Float>... distantEquipment) {
//        if (distantEquipment.length > 0) {
//            for (Map.Entry<DgaJmeModel, Float> e_ : distantEquipment) {
//                DISTANT_EQUIPMENT_.put(e_.getKey(), e_.getValue());
//            }
//            
//        }
//    }
    
    /**
     * The method returns attached equipment mounted on the game object. Attached 
     * equipment includes all supplementary objects immediately adjoined to the 
     * main object.
     * 
     * @return List<DgaJmeModel> Supplementary equipment attached to the game object.
     */
    @Override
    public List<DgaJmeModel> getAttachedEquipmentList() {
        return new ArrayList<>(ATTACHED_EQUIPMENT_.keySet());
    }
    
//    /**
//     * The method returns attached equipment mounted on the game object with the 
//     * distance between the game object and the equipment. Attached equipment 
//     * includes all supplementary objects immediately adjoined to the main object.
//     * 
//     * @return Map<M, Float> Supplementary equipment attached to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    @Override
//    public Map<DgaJmeModel, Float> getAttachedEquipmentMap() {
//        return ATTACHED_EQUIPMENT_;
//    }
    
    /**
     * The method returns distant equipment tied to the game object. Distant 
     * equipment includes all supplementary objects not attached to the main 
     * object but moving with it and rotating around it.
     *
     * @return List<DgaJmeModel> Supplementary equipment tied to the game object.
     */
    @Override
    public List<DgaJmeModel> getDistantEquipmentList() {
        return new ArrayList<>(DISTANT_EQUIPMENT_.keySet());
    }
    
//    /**
//     * The method returns distant equipment tied to the game object with the 
//     * distance between the game object and the equipment. Distant equipment 
//     * includes all supplementary objects not attached to the main object but 
//     * moving with it and rotating around it.
//     *
//     * @return Map<M, Float> Supplementary equipment tied to the game object 
//     * with the distance between the game object and the equipment.
//     */
//    @Override
//    public Map<DgaJmeModel, Float> getDistantEquipmentMap() {
//        return DISTANT_EQUIPMENT_;
//    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
