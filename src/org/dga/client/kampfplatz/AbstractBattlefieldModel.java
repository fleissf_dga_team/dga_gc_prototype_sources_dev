/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz;

import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a model for a battlefield.
 * 
 * @extends java.lang.Object
 * @implements BattlefieldModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 10.08.2018
 */
public abstract class AbstractBattlefieldModel extends Object implements BattlefieldModel {
    private AssetManager assetManager_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private Node rootNode_ = null;
    private AppStateManager stateManager_ = null;
    private Spatial spatialBf_ = null;
    private CollisionShape cshBf_ = null;
    private DgaJmeScale scaleBf_ = null;
    private float offsetBf_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param assetManager The game application's asset manager.
     * @param physicsSpace The game application's physics space.
     * @param rootNode The game application's scene root node.
     * @param stateManager The game application's state manager.
     * @param battlefieldScale The battlefield's scale.
     */
    public AbstractBattlefieldModel(final AssetManager assetManager, 
        final PhysicsSpace physicsSpace, final Node rootNode, 
        final AppStateManager stateManager, final DgaJmeScale battlefieldScale) {
        super();
        assetManager_ = assetManager;
        physicsSpace_ = physicsSpace;
        rootNode_ = rootNode;
        stateManager_ = stateManager;
        scaleBf_ = battlefieldScale;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the battlefield's spatial.
     * 
     * @return Spatial The battlefield's spatial.
     */
    @Override
    public Spatial getSpatial() {
        return spatialBf_;
    }
    
    /**
     * The method returns the battlefield's scale.
     * 
     * @return DgaJmeScale The battlefield's scale.
     */
    @Override
    public DgaJmeScale getScale() {
        return scaleBf_;
    }
    
    /**
     * The method returns the battlefield's collision shape.
     * 
     * @return CollisionShape The battlefield's collision shape.
     */
    @Override
    public CollisionShape getCollisionShape() {
        return cshBf_;
    }
    
    /**
     * The method returns an offset that this game object can have from another one.
     * 
     * @return float An offset that this game object can have from another one.
     */
    @Override
    public float getOffset() {
        return offsetBf_;
    }
    
    /**
     * The method sets an offset that this game object can have from another one.
     * 
     * @param offset An offset that this game object can have from another one.
     */
    @Override
    public void setOffset(final float offset) {
        offsetBf_ = offset;
    }
    //
    // *************************************************************************
    //
    /**
     * The method returns the game application's asset manager.
     * 
     * @return AssetManager The game application's asset manager.
     */
    @Override
    public AssetManager getAssetManager() {
        return assetManager_;
    }
        
    /**
     * The method returns the game application's physics space.
     * 
     * @return PhysicsSpace The game application's physics space.
     */
    @Override
    public PhysicsSpace getPhysicsSpace() {
        return physicsSpace_;
    }
    
    /**
     * The method returns the game application's scene root node.
     * 
     * @return Node The game application's scene root node.
     */
    @Override
    public Node getRootNode() {
        return rootNode_;
    }
    
    /**
     * The method returns the game application's state manager.
     * 
     * @return AppStateManager The game application's state manager.
     */
    @Override
    public AppStateManager getStateManager() {
        return stateManager_;
    }
    //
    // *************************************************************************
    //
    /**
     * The method sets the battlefield's spatial.
     * 
     * @param spatial The battlefield's spatial.
     */
    @Override
    public void setSpatial(final Spatial spatial) {
        spatialBf_ = spatial;
    }
    
    /**
     * The method sets the battlefield's scale.
     * 
     * @param battlefieldScale The battlefield's scale.
     */
    @Override
    public void setScale(final DgaJmeScale battlefieldScale) {
        scaleBf_ = battlefieldScale;
    }
    
    /**
     * The method sets the battlefield's collision shape.
     * 
     * @param collisionShape The battlefield's collision shape.
     */
    @Override
    public void setCollisionShape(final CollisionShape collisionShape) {
        cshBf_ = collisionShape;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}