/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz;

import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import org.dga.client.DgaJmeNameable;

/**
 * The common interface of a battlefield.
 *
 * @extends Terrainable
 * @extends DgaJmeNameable
 * @extends ActionListener
 * @extends AnalogListener
 * @extends PhysicsCollisionListener
 * @extends PhysicsTickListener
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 09.08.2018
 */
public interface Battlefield extends Terrainable, DgaJmeNameable, ActionListener, 
    AnalogListener, PhysicsCollisionListener, PhysicsTickListener {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method initializes the battlefield's spatials and controls.
     */
    public void initModel();
    
    /**
     * The method adds nodes, controls, and other required parts to the scene.
     */
    public void addToScene();
    
    /**
     * The method adds a new combat participator to the team of proponents acting 
     * on the battlefield.
     * 
     * @param kombattant A new combat participator to be added to the team of 
     * proponents acting on the battlefield.
     */
    public void addProponent(final Kombattant kombattant);
    
    /**
     * The method adds a new combat participator to the team of opponents acting 
     * on the battlefield.
     * 
     * @param kombattant A new combat participator to be added to the team of 
     * opponents acting on the battlefield.
     */
    public void addOpponent(final Kombattant kombattant);
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}