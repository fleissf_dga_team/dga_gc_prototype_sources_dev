/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kampfplatz.koeln;

import org.dga.client.DgaJmeUtils;
import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kampfplatz.AbstractBattlefield;
import org.dga.client.kampfplatz.Battlefield;
import org.dga.client.kampfplatz.BattlefieldControl;
import org.dga.client.kampfplatz.BattlefieldModel;
import org.dga.client.kampfplatz.DefaultBattlefieldControl;
import org.dga.client.kampfplatz.battlefieldobject.DefaultBattlefieldBorder;
import org.dga.client.kampfplatz.battlefieldobject.BattlefieldObjectModel;
import org.dga.client.kampfplatz.battlefieldobject.DefaultBattlefieldObjectModel;
import org.dga.client.kampfplatz.battlefieldobject.Ground;
import org.dga.client.kampfplatz.battlefieldobject.StartPoint;
import org.dga.client.kampfplatz.battlefieldobject.building.IndestructibleBuilding;
import org.dga.client.kombattant.Kombattant;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.collision.shapes.MeshCollisionShape;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import com.jme3.util.SkyFactory.EnvMapType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.dga.client.kampfplatz.battlefieldobject.building.IndestructibleGhostBuilding;
import org.dga.client.DgaJmeScale;

/**
 * The battlefield of Koeln in 1945 year.
 * 
 * @extends AbstractBattlefield
 * @implements Battlefield
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 10.08.2018
 */
public final class Koeln1945 extends AbstractBattlefield implements Battlefield {
    // The battlefield's name.
    private static final String KAMPFPLATZNAME_ = "Koeln1945";
    // The start point 1 for the proponents' team.
    private static final Vector3f PROPONENTENGRUPPE_STARTPUNKT1_ = new Vector3f(-200.0f, 0f, -175.0f);
    // The start point 1 for the opponents' team.
    private static final Vector3f OPPONENTENGRUPPE_STARTPUNKT1_ = new Vector3f(200.0f, 0f, 175.0f);
    // The combat participators of the proponents' team.
    private final List<Kombattant> PROPONENTENGRUPPE_ = Collections.synchronizedList(new ArrayList<>());
    // The combat participators of the opponents' team.
    private final List<Kombattant> OPPONENTENGRUPPE_ = Collections.synchronizedList(new ArrayList<>());
    //
    private boolean isOnStart_ = true;
    //
    private AssetManager assetManager_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private Node rootNode_ = null;
    private AppStateManager stateManager_ = null;
    //
    private DgaJmeScale scaleKampfplatz_ = DefaultDgaJmeScale.ORIGINAL_MODEL_SCALE;
    //
    private CompoundCollisionShape compoundKampfplatz_ = null;
    private BattlefieldControl controlKampfplatz_ = null;
    private Node nodeKampfplatz_ = null;
    private Spatial sptlKampfplatz_ = null;
    private BattlefieldModel modelKampfplatz_ = null;
    //
    private DefaultBattlefieldBorder controlWeltendeNord_ = null;
    private MeshCollisionShape cshWeltendeNord_ = null;
    private Node nodeWeltendeNord_ = null;
    private Spatial sptlWeltendeNord_ = null;
    private Geometry geomWeltendeNord_ = null;
    private Vector3f centerWeltendeNord_ = null;
    private BoundingBox bbWeltendeNord_ = null;
    private BattlefieldObjectModel modelWeltendeNord_ = null;
    //
    private DefaultBattlefieldBorder controlWeltendeSued_ = null;
    private MeshCollisionShape cshWeltendeSued_ = null;
    private Node nodeWeltendeSued_ = null;
    private Spatial sptlWeltendeSued_ = null;
    private Geometry geomWeltendeSued_ = null;
    private Vector3f centerWeltendeSued_ = null;
    private BoundingBox bbWeltendeSued_ = null;
    private BattlefieldObjectModel modelWeltendeSued_ = null;
    //
    private DefaultBattlefieldBorder controlWeltendeOst_ = null;
    private MeshCollisionShape cshWeltendeOst_ = null;
    private Node nodeWeltendeOst_ = null;
    private Spatial sptlWeltendeOst_ = null;
    private Geometry geomWeltendeOst_ = null;
    private Vector3f centerWeltendeOst_ = null;
    private BoundingBox bbWeltendeOst_ = null;
    private BattlefieldObjectModel modelWeltendeOst_ = null;
    //
    private DefaultBattlefieldBorder controlWeltendeWest_ = null;
    private MeshCollisionShape cshWeltendeWest_ = null;
    private Node nodeWeltendeWest_ = null;
    private Spatial sptlWeltendeWest_ = null;
    private Geometry geomWeltendeWest_ = null;
    private Vector3f centerWeltendeWest_ = null;
    private BoundingBox bbWeltendeWest_ = null;
    private BattlefieldObjectModel modelWeltendeWest_ = null;
    //
    private Ground controlGrund_ = null;
    private MeshCollisionShape cshGrund_ = null;
    private Node nodeGrund_ = null;
    private Spatial sptlGrund_ = null;
    private Geometry geomGrund_ = null;
    private Vector3f centerGrund_ = null;
    private BoundingBox bbGrund_ = null;
    private BattlefieldObjectModel modelGrund_ = null;
    //
    private IndestructibleBuilding controlDom_ = null;
    private MeshCollisionShape cshDom_ = null;
    private Node nodeDom_ = null;
    private Spatial sptlDom_ = null;
    private Geometry geomDom_ = null;
    private Vector3f centerDom_ = null;
    private BoundingBox bbDom_ = null;
    private BattlefieldObjectModel modelDom_ = null;
    //
    private IndestructibleBuilding controlHaus00_ = null;
    private MeshCollisionShape cshHaus00_ = null;
    private Node nodeHaus00_ = null;
    private Spatial sptlHaus00_ = null;
    private Geometry geomHaus00_ = null;
    private Vector3f centerHaus00_ = null;
    private BoundingBox bbHaus00_ = null;
    private BattlefieldObjectModel modelHaus00_ = null;
    //
    private IndestructibleBuilding controlHaus01_ = null;
    private MeshCollisionShape cshHaus01_ = null;
    private Node nodeHaus01_ = null;
    private Spatial sptlHaus01_ = null;
    private Geometry geomHaus01_ = null;
    private Vector3f centerHaus01_ = null;
    private BoundingBox bbHaus01_ = null;
    private BattlefieldObjectModel modelHaus01_ = null;
    //
    private IndestructibleBuilding controlHaus02_ = null;
    private MeshCollisionShape cshHaus02_ = null;
    private Node nodeHaus02_ = null;
    private Spatial sptlHaus02_ = null;
    private Geometry geomHaus02_ = null;
    private Vector3f centerHaus02_ = null;
    private BoundingBox bbHaus02_ = null;
    private BattlefieldObjectModel modelHaus02_ = null;
    //
    private IndestructibleBuilding controlHaus03_ = null;
    private MeshCollisionShape cshHaus03_ = null;
    private Node nodeHaus03_ = null;
    private Spatial sptlHaus03_ = null;
    private Geometry geomHaus03_ = null;
    private Vector3f centerHaus03_ = null;
    private BoundingBox bbHaus03_ = null;
    private BattlefieldObjectModel modelHaus03_ = null;
    //
    private IndestructibleBuilding controlHaus04_ = null;
    private MeshCollisionShape cshHaus04_ = null;
    private Node nodeHaus04_ = null;
    private Spatial sptlHaus04_ = null;
    private Geometry geomHaus04_ = null;
    private Vector3f centerHaus04_ = null;
    private BoundingBox bbHaus04_ = null;
    private BattlefieldObjectModel modelHaus04_ = null;
    //
    private IndestructibleBuilding controlHaus05_ = null;
    private MeshCollisionShape cshHaus05_ = null;
    private Node nodeHaus05_ = null;
    private Spatial sptlHaus05_ = null;
    private Geometry geomHaus05_ = null;
    private Vector3f centerHaus05_ = null;
    private BoundingBox bbHaus05_ = null;
    private BattlefieldObjectModel modelHaus05_ = null;
    //
    private IndestructibleBuilding controlHaus06_ = null;
    private MeshCollisionShape cshHaus06_ = null;
    private Node nodeHaus06_ = null;
    private Spatial sptlHaus06_ = null;
    private Geometry geomHaus06_ = null;
    private Vector3f centerHaus06_ = null;
    private BoundingBox bbHaus06_ = null;
    private BattlefieldObjectModel modelHaus06_ = null;
    //
    private IndestructibleBuilding controlHaus07_ = null;
    private MeshCollisionShape cshHaus07_ = null;
    private Node nodeHaus07_ = null;
    private Spatial sptlHaus07_ = null;
    private Geometry geomHaus07_ = null;
    private Vector3f centerHaus07_ = null;
    private BoundingBox bbHaus07_ = null;
    private BattlefieldObjectModel modelHaus07_ = null;
    //
    private IndestructibleBuilding controlHaus08_ = null;
    private MeshCollisionShape cshHaus08_ = null;
    private Node nodeHaus08_ = null;
    private Spatial sptlHaus08_ = null;
    private Geometry geomHaus08_ = null;
    private Vector3f centerHaus08_ = null;
    private BoundingBox bbHaus08_ = null;
    private BattlefieldObjectModel modelHaus08_ = null;
    //
    private IndestructibleBuilding controlHaus09_ = null;
    private MeshCollisionShape cshHaus09_ = null;
    private Node nodeHaus09_ = null;
    private Spatial sptlHaus09_ = null;
    private Geometry geomHaus09_ = null;
    private Vector3f centerHaus09_ = null;
    private BoundingBox bbHaus09_ = null;
    private BattlefieldObjectModel modelHaus09_ = null;
    //
    private IndestructibleBuilding controlHaus10_ = null;
    private MeshCollisionShape cshHaus10_ = null;
    private Node nodeHaus10_ = null;
    private Spatial sptlHaus10_ = null;
    private Geometry geomHaus10_ = null;
    private Vector3f centerHaus10_ = null;
    private BoundingBox bbHaus10_ = null;
    private BattlefieldObjectModel modelHaus10_ = null;
    //
    private IndestructibleBuilding controlHaus11_ = null;
    private MeshCollisionShape cshHaus11_ = null;
    private Node nodeHaus11_ = null;
    private Spatial sptlHaus11_ = null;
    private Geometry geomHaus11_ = null;
    private Vector3f centerHaus11_ = null;
    private BoundingBox bbHaus11_ = null;
    private BattlefieldObjectModel modelHaus11_ = null;
    //
    private IndestructibleBuilding controlHaus12_ = null;
    private MeshCollisionShape cshHaus12_ = null;
    private Node nodeHaus12_ = null;
    private Spatial sptlHaus12_ = null;
    private Geometry geomHaus12_ = null;
    private Vector3f centerHaus12_ = null;
    private BoundingBox bbHaus12_ = null;
    private BattlefieldObjectModel modelHaus12_ = null;
    //
    private IndestructibleBuilding controlHaus13_ = null;
    private MeshCollisionShape cshHaus13_ = null;
    private Node nodeHaus13_ = null;
    private Spatial sptlHaus13_ = null;
    private Geometry geomHaus13_ = null;
    private Vector3f centerHaus13_ = null;
    private BoundingBox bbHaus13_ = null;
    private BattlefieldObjectModel modelHaus13_ = null;
    //
    private IndestructibleBuilding controlHaus14_ = null;
    private MeshCollisionShape cshHaus14_ = null;
    private Node nodeHaus14_ = null;
    private Spatial sptlHaus14_ = null;
    private Geometry geomHaus14_ = null;
    private Vector3f centerHaus14_ = null;
    private BoundingBox bbHaus14_ = null;
    private BattlefieldObjectModel modelHaus14_ = null;
    //
    private IndestructibleBuilding controlHaus15_ = null;
    private MeshCollisionShape cshHaus15_ = null;
    private Node nodeHaus15_ = null;
    private Spatial sptlHaus15_ = null;
    private Geometry geomHaus15_ = null;
    private Vector3f centerHaus15_ = null;
    private BoundingBox bbHaus15_ = null;
    private BattlefieldObjectModel modelHaus15_ = null;
    //
    private IndestructibleBuilding controlHaus16_ = null;
    private MeshCollisionShape cshHaus16_ = null;
    private Node nodeHaus16_ = null;
    private Spatial sptlHaus16_ = null;
    private Geometry geomHaus16_ = null;
    private Vector3f centerHaus16_ = null;
    private BoundingBox bbHaus16_ = null;
    private BattlefieldObjectModel modelHaus16_ = null;
    //
    private IndestructibleBuilding controlHaus17_ = null;
    private MeshCollisionShape cshHaus17_ = null;
    private Node nodeHaus17_ = null;
    private Spatial sptlHaus17_ = null;
    private Geometry geomHaus17_ = null;
    private Vector3f centerHaus17_ = null;
    private BoundingBox bbHaus17_ = null;
    private BattlefieldObjectModel modelHaus17_ = null;
    //
    private IndestructibleBuilding controlHaus18_ = null;
    private MeshCollisionShape cshHaus18_ = null;
    private Node nodeHaus18_ = null;
    private Spatial sptlHaus18_ = null;
    private Geometry geomHaus18_ = null;
    private Vector3f centerHaus18_ = null;
    private BoundingBox bbHaus18_ = null;
    private BattlefieldObjectModel modelHaus18_ = null;
    //
    private IndestructibleBuilding controlHaus19_ = null;
    private MeshCollisionShape cshHaus19_ = null;
    private Node nodeHaus19_ = null;
    private Spatial sptlHaus19_ = null;
    private Geometry geomHaus19_ = null;
    private Vector3f centerHaus19_ = null;
    private BoundingBox bbHaus19_ = null;
    private BattlefieldObjectModel modelHaus19_ = null;
    //
    private IndestructibleBuilding controlHaus20_ = null;
    private MeshCollisionShape cshHaus20_ = null;
    private Node nodeHaus20_ = null;
    private Spatial sptlHaus20_ = null;
    private Geometry geomHaus20_ = null;
    private Vector3f centerHaus20_ = null;
    private BoundingBox bbHaus20_ = null;
    private BattlefieldObjectModel modelHaus20_ = null;
    //
    private IndestructibleBuilding controlHaus21_ = null;
    private MeshCollisionShape cshHaus21_ = null;
    private Node nodeHaus21_ = null;
    private Spatial sptlHaus21_ = null;
    private Geometry geomHaus21_ = null;
    private Vector3f centerHaus21_ = null;
    private BoundingBox bbHaus21_ = null;
    private BattlefieldObjectModel modelHaus21_ = null;
    //
    private IndestructibleBuilding controlKasten00_ = null;
    private MeshCollisionShape cshKasten00_ = null;
    private Node nodeKasten00_ = null;
    private Spatial sptlKasten00_ = null;
    private Geometry geomKasten00_ = null;
    private Vector3f centerKasten00_ = null;
    private BoundingBox bbKasten00_ = null;
    private BattlefieldObjectModel modelKasten00_ = null;
    //
    private IndestructibleBuilding controlKasten01_ = null;
    private MeshCollisionShape cshKasten01_ = null;
    private Node nodeKasten01_ = null;
    private Spatial sptlKasten01_ = null;
    private Geometry geomKasten01_ = null;
    private Vector3f centerKasten01_ = null;
    private BoundingBox bbKasten01_ = null;
    private BattlefieldObjectModel modelKasten01_ = null;
    //
    private IndestructibleBuilding controlKasten02_ = null;
    private MeshCollisionShape cshKasten02_ = null;
    private Node nodeKasten02_ = null;
    private Spatial sptlKasten02_ = null;
    private Geometry geomKasten02_ = null;
    private Vector3f centerKasten02_ = null;
    private BoundingBox bbKasten02_ = null;
    private BattlefieldObjectModel modelKasten02_ = null;
    //
    private IndestructibleBuilding controlKasten03_ = null;
    private MeshCollisionShape cshKasten03_ = null;
    private Node nodeKasten03_ = null;
    private Spatial sptlKasten03_ = null;
    private Geometry geomKasten03_ = null;
    private Vector3f centerKasten03_ = null;
    private BoundingBox bbKasten03_ = null;
    private BattlefieldObjectModel modelKasten03_ = null;
    //
    private IndestructibleBuilding controlKasten04_ = null;
    private MeshCollisionShape cshKasten04_ = null;
    private Node nodeKasten04_ = null;
    private Spatial sptlKasten04_ = null;
    private Geometry geomKasten04_ = null;
    private Vector3f centerKasten04_ = null;
    private BoundingBox bbKasten04_ = null;
    private BattlefieldObjectModel modelKasten04_ = null;
    //
    private IndestructibleBuilding controlKasten05_ = null;
    private MeshCollisionShape cshKasten05_ = null;
    private Node nodeKasten05_ = null;
    private Spatial sptlKasten05_ = null;
    private Geometry geomKasten05_ = null;
    private Vector3f centerKasten05_ = null;
    private BoundingBox bbKasten05_ = null;
    private BattlefieldObjectModel modelKasten05_ = null;
    //
    private IndestructibleBuilding controlKasten06_ = null;
    private MeshCollisionShape cshKasten06_ = null;
    private Node nodeKasten06_ = null;
    private Spatial sptlKasten06_ = null;
    private Geometry geomKasten06_ = null;
    private Vector3f centerKasten06_ = null;
    private BoundingBox bbKasten06_ = null;
    private BattlefieldObjectModel modelKasten06_ = null;
    //
    private IndestructibleBuilding controlKasten07_ = null;
    private MeshCollisionShape cshKasten07_ = null;
    private Node nodeKasten07_ = null;
    private Spatial sptlKasten07_ = null;
    private Geometry geomKasten07_ = null;
    private Vector3f centerKasten07_ = null;
    private BoundingBox bbKasten07_ = null;
    private BattlefieldObjectModel modelKasten07_ = null;
    //
    private IndestructibleBuilding controlKasten08_ = null;
    private MeshCollisionShape cshKasten08_ = null;
    private Node nodeKasten08_ = null;
    private Spatial sptlKasten08_ = null;
    private Geometry geomKasten08_ = null;
    private Vector3f centerKasten08_ = null;
    private BoundingBox bbKasten08_ = null;
    private BattlefieldObjectModel modelKasten08_ = null;
    //
    private IndestructibleBuilding controlKasten09_ = null;
    private MeshCollisionShape cshKasten09_ = null;
    private Node nodeKasten09_ = null;
    private Spatial sptlKasten09_ = null;
    private Geometry geomKasten09_ = null;
    private Vector3f centerKasten09_ = null;
    private BoundingBox bbKasten09_ = null;
    private BattlefieldObjectModel modelKasten09_ = null;
    //
    private IndestructibleBuilding controlKasten10_ = null;
    private MeshCollisionShape cshKasten10_ = null;
    private Node nodeKasten10_ = null;
    private Spatial sptlKasten10_ = null;
    private Geometry geomKasten10_ = null;
    private Vector3f centerKasten10_ = null;
    private BoundingBox bbKasten10_ = null;
    private BattlefieldObjectModel modelKasten10_ = null;
    //
    private IndestructibleBuilding controlKasten11_ = null;
    private MeshCollisionShape cshKasten11_ = null;
    private Node nodeKasten11_ = null;
    private Spatial sptlKasten11_ = null;
    private Geometry geomKasten11_ = null;
    private Vector3f centerKasten11_ = null;
    private BoundingBox bbKasten11_ = null;
    private BattlefieldObjectModel modelKasten11_ = null;
    //
    private StartPoint controlFeind1Startpunkt1_ = null;
    private MeshCollisionShape cshFeind1Startpunkt1_ = null;
    private Node nodeFeind1Startpunkt1_ = null;
    private Spatial sptlFeind1Startpunkt1_ = null;
    private Geometry geomFeind1Startpunkt1_ = null;
    private Vector3f centerFeind1Startpunkt1_ = null;
    private BoundingBox bbFeind1Startpunkt1_ = null;
    private BattlefieldObjectModel modelFeind1Startpunkt1_ = null;
    //
    private StartPoint controlFeind2Startpunkt1_ = null;
    private MeshCollisionShape cshFeind2Startpunkt1_ = null;
    private Node nodeFeind2Startpunkt1_ = null;
    private Spatial sptlFeind2Startpunkt1_ = null;
    private Geometry geomFeind2Startpunkt1_ = null;
    private Vector3f centerFeind2Startpunkt1_ = null;
    private BoundingBox bbFeind2Startpunkt1_ = null;
    private BattlefieldObjectModel modelFeind2Startpunkt1_ = null;
    //
    private Texture texTransparent_ = null;
    private Material matTransparent_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates an instance with the specified scale.
     * 
     * @param battlefieldModel The battlefield's model.
     */
    public Koeln1945(final BattlefieldModel battlefieldModel) {
        super(battlefieldModel.getScale());
        modelKampfplatz_ = battlefieldModel;
        assetManager_ = modelKampfplatz_.getAssetManager();
        physicsSpace_ = modelKampfplatz_.getPhysicsSpace();
        rootNode_ = modelKampfplatz_.getRootNode();
        stateManager_ = modelKampfplatz_.getStateManager();
        scaleKampfplatz_ = modelKampfplatz_.getScale();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the battlefield's name.
     * 
     * @return String The battlefield's name.
     */
    @Override
    public String getName() {
        return KAMPFPLATZNAME_;
    }
    
    /**
     * The method initializes the battlefield's spatials and controls.
     */
    @Override
    public void initModel() {
        // Start timer
        final long buildStart_ = System.currentTimeMillis();
        //
        physicsSpace_.addCollisionListener(this);
        //
        texTransparent_ = assetManager_.loadTexture("Materials/TransparentTexture.png");
        matTransparent_ = new Material(assetManager_, "Common/MatDefs/Misc/Unshaded.j3md");
        matTransparent_.setTexture("ColorMap", texTransparent_);
        matTransparent_.getAdditionalRenderState().setBlendMode(
            RenderState.BlendMode.Alpha);  // Activate transparency

        // Kampfplatz
        nodeKampfplatz_ = new Node("Kampfplatz");
        sptlKampfplatz_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Koeln1945.j3o");
        nodeKampfplatz_.attachChild(sptlKampfplatz_);
        compoundKampfplatz_ = new CompoundCollisionShape();
        modelKampfplatz_.setSpatial(nodeKampfplatz_);
        modelKampfplatz_.setCollisionShape(compoundKampfplatz_);
        controlKampfplatz_ = new DefaultBattlefieldControl(modelKampfplatz_);
        nodeKampfplatz_.addControl(controlKampfplatz_);
        controlKampfplatz_.setSpatial(nodeKampfplatz_);
        
        // Firmament
        Spatial firmament_ = SkyFactory.createSky(
            assetManager_, "Scenes/Beach/FullskiesSunset0068.dds", EnvMapType.CubeMap);
        firmament_.setLocalScale(350.0f);
        nodeKampfplatz_.attachChild(firmament_);

        // WeltendeNord
        nodeWeltendeNord_ = new Node("WeltendeNord");
        sptlWeltendeNord_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Specials/WeltendeNord.mesh.j3o");
        nodeWeltendeNord_.attachChild(sptlWeltendeNord_);
        geomWeltendeNord_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlWeltendeNord_, "WeltendeNord");
        bbWeltendeNord_ = (BoundingBox) geomWeltendeNord_.getModelBound();
        centerWeltendeNord_ = bbWeltendeNord_.getCenter();
        cshWeltendeNord_ = new MeshCollisionShape(geomWeltendeNord_.getMesh());
        compoundKampfplatz_.addChildShape(cshWeltendeNord_, Vector3f.ZERO);
        modelWeltendeNord_ = new DefaultBattlefieldObjectModel(
            this, sptlWeltendeNord_, scaleKampfplatz_, cshWeltendeNord_, "WeltendeNord");
        controlWeltendeNord_ = new DefaultBattlefieldBorder(modelWeltendeNord_);
        nodeWeltendeNord_.setMaterial(matTransparent_);
        
        // WeltendeSued
        nodeWeltendeSued_ = new Node("WeltendeSued");
        sptlWeltendeSued_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Specials/WeltendeSued.mesh.j3o");
        nodeWeltendeSued_.attachChild(sptlWeltendeSued_);
        geomWeltendeSued_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlWeltendeSued_, "WeltendeSued");
        bbWeltendeSued_ = (BoundingBox) geomWeltendeSued_.getModelBound();
        centerWeltendeSued_ = bbWeltendeSued_.getCenter();
        cshWeltendeSued_ = new MeshCollisionShape(geomWeltendeSued_.getMesh());
        compoundKampfplatz_.addChildShape(cshWeltendeSued_, Vector3f.ZERO);
        modelWeltendeSued_ = new DefaultBattlefieldObjectModel(
            this, sptlWeltendeSued_, scaleKampfplatz_, cshWeltendeSued_, "WeltendeSued");
        controlWeltendeSued_ = new DefaultBattlefieldBorder(modelWeltendeSued_);
        nodeWeltendeSued_.setMaterial(matTransparent_);
        
        // WeltendeOst
        nodeWeltendeOst_ = new Node("WeltendeOst");
        sptlWeltendeOst_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Specials/WeltendeOst.mesh.j3o");
        nodeWeltendeOst_.attachChild(sptlWeltendeOst_);
        geomWeltendeOst_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlWeltendeOst_, "WeltendeOst");
        bbWeltendeOst_ = (BoundingBox) geomWeltendeOst_.getModelBound();
        centerWeltendeOst_ = bbWeltendeOst_.getCenter();
        cshWeltendeOst_ = new MeshCollisionShape(geomWeltendeOst_.getMesh());
        compoundKampfplatz_.addChildShape(cshWeltendeOst_, Vector3f.ZERO);
        modelWeltendeOst_ = new DefaultBattlefieldObjectModel(
            this, sptlWeltendeOst_, scaleKampfplatz_, cshWeltendeOst_, "WeltendeOst");
        controlWeltendeOst_ = new DefaultBattlefieldBorder(modelWeltendeOst_);
        nodeWeltendeOst_.setMaterial(matTransparent_);
        
        // WeltendeWest
        nodeWeltendeWest_ = new Node("WeltendeWest");
        sptlWeltendeWest_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Specials/WeltendeWest.mesh.j3o");
        nodeWeltendeWest_.attachChild(sptlWeltendeWest_);
        geomWeltendeWest_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlWeltendeWest_, "WeltendeWest");
        bbWeltendeWest_ = (BoundingBox) geomWeltendeWest_.getModelBound();
        centerWeltendeWest_ = bbWeltendeWest_.getCenter();
        cshWeltendeWest_ = new MeshCollisionShape(geomWeltendeWest_.getMesh());
        compoundKampfplatz_.addChildShape(cshWeltendeWest_, Vector3f.ZERO);
        modelWeltendeWest_ = new DefaultBattlefieldObjectModel(
            this, sptlWeltendeWest_, scaleKampfplatz_, cshWeltendeWest_, "WeltendeWest");
        controlWeltendeWest_ = new DefaultBattlefieldBorder(modelWeltendeWest_);
        nodeWeltendeWest_.setMaterial(matTransparent_);
        
        // Grund
        nodeGrund_ = new Node("Grund");
        sptlGrund_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Grund.mesh.j3o");
        nodeGrund_.attachChild(sptlGrund_);
        geomGrund_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlGrund_, "Grund");
        bbGrund_ = (BoundingBox) geomGrund_.getModelBound();
        centerGrund_ = bbGrund_.getCenter();
        cshGrund_ = new MeshCollisionShape(geomGrund_.getMesh());
        compoundKampfplatz_.addChildShape(cshGrund_, Vector3f.ZERO);
        modelGrund_ = new DefaultBattlefieldObjectModel(
            this, sptlGrund_, scaleKampfplatz_, cshGrund_, "Grund");
        controlGrund_ = new Ground(modelGrund_);
        
        // Dom
        nodeDom_ = new Node("Dom");
        sptlDom_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Dom.mesh.j3o");
        nodeDom_.attachChild(sptlDom_);
        geomDom_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlDom_, "Dom");
        bbDom_ = (BoundingBox) geomDom_.getModelBound();
        centerDom_ = bbDom_.getCenter();
        cshDom_ = new MeshCollisionShape(geomDom_.getMesh());
        compoundKampfplatz_.addChildShape(cshDom_, Vector3f.ZERO);
        modelDom_ = new DefaultBattlefieldObjectModel(
            this, sptlDom_, scaleKampfplatz_, cshDom_, "Dom");
        controlDom_ = new IndestructibleGhostBuilding(modelDom_);
        
        // Haus00
        nodeHaus00_ = new Node("Haus00");
        sptlHaus00_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus00.mesh.j3o");
        nodeHaus00_.attachChild(sptlHaus00_);
        geomHaus00_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus00_, "Haus00");
        bbHaus00_ = (BoundingBox) geomHaus00_.getModelBound();
        centerHaus00_ = bbHaus00_.getCenter();
        cshHaus00_ = new MeshCollisionShape(geomHaus00_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus00_, Vector3f.ZERO);
        modelHaus00_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus00_, scaleKampfplatz_, cshHaus00_, "Haus00");
        controlHaus00_ = new IndestructibleGhostBuilding(modelHaus00_);
        
        // Haus01
        nodeHaus01_ = new Node("Haus01");
        sptlHaus01_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus01.mesh.j3o");
        nodeHaus01_.attachChild(sptlHaus01_);
        geomHaus01_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus01_, "Haus01");
        bbHaus01_ = (BoundingBox) geomHaus01_.getModelBound();
        centerHaus01_ = bbHaus01_.getCenter();
        cshHaus01_ = new MeshCollisionShape(geomHaus01_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus01_, Vector3f.ZERO);
        modelHaus01_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus01_, scaleKampfplatz_, cshHaus01_, "Haus01");
        controlHaus01_ = new IndestructibleGhostBuilding(modelHaus01_);
        
        // Haus02
        nodeHaus02_ = new Node("Haus02");
        sptlHaus02_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus02.mesh.j3o");
        nodeHaus02_.attachChild(sptlHaus02_);
        geomHaus02_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus02_, "Haus02");
        bbHaus02_ = (BoundingBox) geomHaus02_.getModelBound();
        centerHaus02_ = bbHaus02_.getCenter();
        cshHaus02_ = new MeshCollisionShape(geomHaus02_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus02_, Vector3f.ZERO);
        modelHaus02_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus02_, scaleKampfplatz_, cshHaus02_, "Haus02");
        controlHaus02_ = new IndestructibleGhostBuilding(modelHaus02_);
        
        // Haus03
        nodeHaus03_ = new Node("Haus03");
        sptlHaus03_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus03.mesh.j3o");
        nodeHaus03_.attachChild(sptlHaus03_);
        geomHaus03_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus03_, "Haus03");
        bbHaus03_ = (BoundingBox) geomHaus03_.getModelBound();
        centerHaus03_ = bbHaus03_.getCenter();
        cshHaus03_ = new MeshCollisionShape(geomHaus03_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus03_, Vector3f.ZERO);
        modelHaus03_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus03_, scaleKampfplatz_, cshHaus03_, "Haus03");
        controlHaus03_ = new IndestructibleGhostBuilding(modelHaus03_);
        
        // Haus04
        nodeHaus04_ = new Node("Haus04");
        sptlHaus04_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus04.mesh.j3o");
        nodeHaus04_.attachChild(sptlHaus04_);
        geomHaus04_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus04_, "Haus04");
        bbHaus04_ = (BoundingBox) geomHaus04_.getModelBound();
        centerHaus04_ = bbHaus04_.getCenter();
        cshHaus04_ = new MeshCollisionShape(geomHaus04_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus04_, Vector3f.ZERO);
        modelHaus04_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus04_, scaleKampfplatz_, cshHaus04_, "Haus04");
        controlHaus04_ = new IndestructibleGhostBuilding(modelHaus04_);
        
        // Haus05
        nodeHaus05_ = new Node("Haus05");
        sptlHaus05_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus05.mesh.j3o");
        nodeHaus05_.attachChild(sptlHaus05_);
        geomHaus05_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus05_, "Haus05");
        bbHaus05_ = (BoundingBox) geomHaus05_.getModelBound();
        centerHaus05_ = bbHaus05_.getCenter();
        cshHaus05_ = new MeshCollisionShape(geomHaus05_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus05_, Vector3f.ZERO);
        modelHaus05_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus05_, scaleKampfplatz_, cshHaus05_, "Haus05");
        controlHaus05_ = new IndestructibleGhostBuilding(modelHaus05_);
        
        // Haus06
        nodeHaus06_ = new Node("Haus06");
        sptlHaus06_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus06.mesh.j3o");
        nodeHaus06_.attachChild(sptlHaus06_);
        geomHaus06_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus06_, "Haus06");
        bbHaus06_ = (BoundingBox) geomHaus06_.getModelBound();
        centerHaus06_ = bbHaus06_.getCenter();
        cshHaus06_ = new MeshCollisionShape(geomHaus06_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus06_, Vector3f.ZERO);
        modelHaus06_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus06_, scaleKampfplatz_, cshHaus06_, "Haus06");
        controlHaus06_ = new IndestructibleGhostBuilding(modelHaus06_);
        
        // Haus07
        nodeHaus07_ = new Node("Haus07");
        sptlHaus07_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus07.mesh.j3o");
        nodeHaus07_.attachChild(sptlHaus07_);
        geomHaus07_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus07_, "Haus07");
        bbHaus07_ = (BoundingBox) geomHaus07_.getModelBound();
        centerHaus07_ = bbHaus07_.getCenter();
        cshHaus07_ = new MeshCollisionShape(geomHaus07_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus07_, Vector3f.ZERO);
        modelHaus07_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus07_, scaleKampfplatz_, cshHaus07_, "Haus07");
        controlHaus07_ = new IndestructibleGhostBuilding(modelHaus07_);
        
        // Haus08
        nodeHaus08_ = new Node("Haus08");
        sptlHaus08_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus08.mesh.j3o");
        nodeHaus08_.attachChild(sptlHaus08_);
        geomHaus08_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus08_, "Haus08");
        bbHaus08_ = (BoundingBox) geomHaus08_.getModelBound();
        centerHaus08_ = bbHaus08_.getCenter();
        cshHaus08_ = new MeshCollisionShape(geomHaus08_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus08_, Vector3f.ZERO);
        modelHaus08_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus08_, scaleKampfplatz_, cshHaus08_, "Haus08");
        controlHaus08_ = new IndestructibleGhostBuilding(modelHaus08_);
        
        // Haus09
        nodeHaus09_ = new Node("Haus09");
        sptlHaus09_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus09.mesh.j3o");
        nodeHaus09_.attachChild(sptlHaus09_);
        geomHaus09_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus09_, "Haus09");
        bbHaus09_ = (BoundingBox) geomHaus09_.getModelBound();
        centerHaus09_ = bbHaus09_.getCenter();
        cshHaus09_ = new MeshCollisionShape(geomHaus09_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus09_, Vector3f.ZERO);
        modelHaus09_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus09_, scaleKampfplatz_, cshHaus09_, "Haus09");
        controlHaus09_ = new IndestructibleGhostBuilding(modelHaus09_);
        
        // Haus10
        nodeHaus10_ = new Node("Haus10");
        sptlHaus10_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus10.mesh.j3o");
        nodeHaus10_.attachChild(sptlHaus10_);
        geomHaus10_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus10_, "Haus10");
        bbHaus10_ = (BoundingBox) geomHaus10_.getModelBound();
        centerHaus10_ = bbHaus10_.getCenter();
        cshHaus10_ = new MeshCollisionShape(geomHaus10_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus10_, Vector3f.ZERO);
        modelHaus10_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus10_, scaleKampfplatz_, cshHaus10_, "Haus10");
        controlHaus10_ = new IndestructibleGhostBuilding(modelHaus10_);
        
        // Haus11
        nodeHaus11_ = new Node("Haus11");
        sptlHaus11_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus11.mesh.j3o");
        nodeHaus11_.attachChild(sptlHaus11_);
        geomHaus11_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus11_, "Haus11");
        bbHaus11_ = (BoundingBox) geomHaus11_.getModelBound();
        centerHaus11_ = bbHaus11_.getCenter();
        cshHaus11_ = new MeshCollisionShape(geomHaus11_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus11_, Vector3f.ZERO);
        modelHaus11_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus11_, scaleKampfplatz_, cshHaus11_, "Haus11");
        controlHaus11_ = new IndestructibleGhostBuilding(modelHaus11_);
        
        // Haus12
        nodeHaus12_ = new Node("Haus12");
        sptlHaus12_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus12.mesh.j3o");
        nodeHaus12_.attachChild(sptlHaus12_);
        geomHaus12_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus12_, "Haus12");
        bbHaus12_ = (BoundingBox) geomHaus12_.getModelBound();
        centerHaus12_ = bbHaus12_.getCenter();
        cshHaus12_ = new MeshCollisionShape(geomHaus12_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus12_, Vector3f.ZERO);
        modelHaus12_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus12_, scaleKampfplatz_, cshHaus12_, "Haus12");
        controlHaus12_ = new IndestructibleGhostBuilding(modelHaus12_);
        
        // Haus13
        nodeHaus13_ = new Node("Haus13");
        sptlHaus13_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus13.mesh.j3o");
        nodeHaus13_.attachChild(sptlHaus13_);
        geomHaus13_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus13_, "Haus13");
        bbHaus13_ = (BoundingBox) geomHaus13_.getModelBound();
        centerHaus13_ = bbHaus13_.getCenter();
        cshHaus13_ = new MeshCollisionShape(geomHaus13_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus13_, Vector3f.ZERO);
        modelHaus13_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus13_, scaleKampfplatz_, cshHaus13_, "Haus13");
        controlHaus13_ = new IndestructibleGhostBuilding(modelHaus13_);
        
        // Haus14
        nodeHaus14_ = new Node("Haus14");
        sptlHaus14_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus14.mesh.j3o");
        nodeHaus14_.attachChild(sptlHaus14_);
        geomHaus14_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus14_, "Haus14");
        bbHaus14_ = (BoundingBox) geomHaus14_.getModelBound();
        centerHaus14_ = bbHaus14_.getCenter();
        cshHaus14_ = new MeshCollisionShape(geomHaus14_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus14_, Vector3f.ZERO);
        modelHaus14_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus14_, scaleKampfplatz_, cshHaus14_, "Haus14");
        controlHaus14_ = new IndestructibleGhostBuilding(modelHaus14_);
        
        // Haus15
        nodeHaus15_ = new Node("Haus15");
        sptlHaus15_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus15.mesh.j3o");
        nodeHaus15_.attachChild(sptlHaus15_);
        geomHaus15_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus15_, "Haus15");
        bbHaus15_ = (BoundingBox) geomHaus15_.getModelBound();
        centerHaus15_ = bbHaus15_.getCenter();
        cshHaus15_ = new MeshCollisionShape(geomHaus15_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus15_, Vector3f.ZERO);
        modelHaus15_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus15_, scaleKampfplatz_, cshHaus15_, "Haus15");
        controlHaus15_ = new IndestructibleGhostBuilding(modelHaus15_);
        
        // Haus16
        nodeHaus16_ = new Node("Haus16");
        sptlHaus16_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus16.mesh.j3o");
        nodeHaus16_.attachChild(sptlHaus16_);
        geomHaus16_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus16_, "Haus16");
        bbHaus16_ = (BoundingBox) geomHaus16_.getModelBound();
        centerHaus16_ = bbHaus16_.getCenter();
        cshHaus16_ = new MeshCollisionShape(geomHaus16_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus16_, Vector3f.ZERO);
        modelHaus16_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus16_, scaleKampfplatz_, cshHaus16_, "Haus16");
        controlHaus16_ = new IndestructibleGhostBuilding(modelHaus16_);
        
        // Haus17
        nodeHaus17_ = new Node("Haus17");
        sptlHaus17_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus17.mesh.j3o");
        nodeHaus17_.attachChild(sptlHaus17_);
        geomHaus17_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus17_, "Haus17");
        bbHaus17_ = (BoundingBox) geomHaus17_.getModelBound();
        centerHaus17_ = bbHaus17_.getCenter();
        cshHaus17_ = new MeshCollisionShape(geomHaus17_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus17_, Vector3f.ZERO);
        modelHaus17_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus17_, scaleKampfplatz_, cshHaus17_, "Haus17");
        controlHaus17_ = new IndestructibleGhostBuilding(modelHaus17_);
        
        // Haus18
        nodeHaus18_ = new Node("Haus18");
        sptlHaus18_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus18.mesh.j3o");
        nodeHaus18_.attachChild(sptlHaus18_);
        geomHaus18_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus18_, "Haus18");
        bbHaus18_ = (BoundingBox) geomHaus18_.getModelBound();
        centerHaus18_ = bbHaus18_.getCenter();
        cshHaus18_ = new MeshCollisionShape(geomHaus18_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus18_, Vector3f.ZERO);
        modelHaus18_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus18_, scaleKampfplatz_, cshHaus18_, "Haus18");
        controlHaus18_ = new IndestructibleGhostBuilding(modelHaus18_);
        
        // Haus19
        nodeHaus19_ = new Node("Haus19");
        sptlHaus19_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus19.mesh.j3o");
        nodeHaus19_.attachChild(sptlHaus19_);
        geomHaus19_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus19_, "Haus19");
        bbHaus19_ = (BoundingBox) geomHaus19_.getModelBound();
        centerHaus19_ = bbHaus19_.getCenter();
        cshHaus19_ = new MeshCollisionShape(geomHaus19_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus19_, Vector3f.ZERO);
        modelHaus19_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus19_, scaleKampfplatz_, cshHaus19_, "Haus19");
        controlHaus19_ = new IndestructibleGhostBuilding(modelHaus19_);
        
        // Haus20
        nodeHaus20_ = new Node("Haus20");
        sptlHaus20_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus20.mesh.j3o");
        nodeHaus20_.attachChild(sptlHaus20_);
        geomHaus20_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus20_, "Haus20");
        bbHaus20_ = (BoundingBox) geomHaus20_.getModelBound();
        centerHaus20_ = bbHaus20_.getCenter();
        cshHaus20_ = new MeshCollisionShape(geomHaus20_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus20_, Vector3f.ZERO);
        modelHaus20_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus20_, scaleKampfplatz_, cshHaus20_, "Haus20");
        controlHaus20_ = new IndestructibleGhostBuilding(modelHaus20_);
        
        // Haus21
        nodeHaus21_ = new Node("Haus21");
        sptlHaus21_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Haus21.mesh.j3o");
        nodeHaus21_.attachChild(sptlHaus21_);
        geomHaus21_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlHaus21_, "Haus21");
        bbHaus21_ = (BoundingBox) geomHaus21_.getModelBound();
        centerHaus21_ = bbHaus21_.getCenter();
        cshHaus21_ = new MeshCollisionShape(geomHaus21_.getMesh());
        compoundKampfplatz_.addChildShape(cshHaus21_, Vector3f.ZERO);
        modelHaus21_ = new DefaultBattlefieldObjectModel(
            this, sptlHaus21_, scaleKampfplatz_, cshHaus21_, "Haus21");
        controlHaus21_ = new IndestructibleGhostBuilding(modelHaus21_);
        
        // Kasten00
        nodeKasten00_ = new Node("Kasten00");
        sptlKasten00_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten00.mesh.j3o");
        nodeKasten00_.attachChild(sptlKasten00_);
        geomKasten00_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten00_, "Kasten00");
        bbKasten00_ = (BoundingBox) geomKasten00_.getModelBound();
        centerKasten00_ = bbKasten00_.getCenter();
        cshKasten00_ = new MeshCollisionShape(geomKasten00_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten00_, Vector3f.ZERO);
        modelKasten00_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten00_, scaleKampfplatz_, cshKasten00_, "Kasten00");
        controlKasten00_ = new IndestructibleGhostBuilding(modelKasten00_);
        
        // Kasten01
        nodeKasten01_ = new Node("Kasten01");
        sptlKasten01_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten01.mesh.j3o");
        nodeKasten01_.attachChild(sptlKasten01_);
        geomKasten01_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten01_, "Kasten01");
        bbKasten01_ = (BoundingBox) geomKasten01_.getModelBound();
        centerKasten01_ = bbKasten01_.getCenter();
        cshKasten01_ = new MeshCollisionShape(geomKasten01_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten01_, Vector3f.ZERO);
        modelKasten01_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten01_, scaleKampfplatz_, cshKasten01_, "Kasten01");
        controlKasten01_ = new IndestructibleGhostBuilding(modelKasten01_);
        
        // Kasten02
        nodeKasten02_ = new Node("Kasten02");
        sptlKasten02_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten02.mesh.j3o");
        nodeKasten02_.attachChild(sptlKasten02_);
        geomKasten02_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten02_, "Kasten02");
        bbKasten02_ = (BoundingBox) geomKasten02_.getModelBound();
        centerKasten02_ = bbKasten02_.getCenter();
        cshKasten02_ = new MeshCollisionShape(geomKasten02_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten02_, Vector3f.ZERO);
        modelKasten02_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten02_, scaleKampfplatz_, cshKasten02_, "Kasten02");
        controlKasten02_ = new IndestructibleGhostBuilding(modelKasten02_);
        
        // Kasten03
        nodeKasten03_ = new Node("Kasten03");
        sptlKasten03_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten03.mesh.j3o");
        nodeKasten03_.attachChild(sptlKasten03_);
        geomKasten03_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten03_, "Kasten03");
        bbKasten03_ = (BoundingBox) geomKasten03_.getModelBound();
        centerKasten03_ = bbKasten03_.getCenter();
        cshKasten03_ = new MeshCollisionShape(geomKasten03_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten03_, Vector3f.ZERO);
        modelKasten03_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten03_, scaleKampfplatz_, cshKasten03_, "Kasten03");
        controlKasten03_ = new IndestructibleGhostBuilding(modelKasten03_);
        
        // Kasten04
        nodeKasten04_ = new Node("Kasten04");
        sptlKasten04_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten04.mesh.j3o");
        nodeKasten04_.attachChild(sptlKasten04_);
        geomKasten04_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten04_, "Kasten04");
        bbKasten04_ = (BoundingBox) geomKasten04_.getModelBound();
        centerKasten04_ = bbKasten04_.getCenter();
        cshKasten04_ = new MeshCollisionShape(geomKasten04_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten04_, Vector3f.ZERO);
        modelKasten04_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten04_, scaleKampfplatz_, cshKasten04_, "Kasten04");
        controlKasten04_ = new IndestructibleGhostBuilding(modelKasten04_);
        
        // Kasten05
        nodeKasten05_ = new Node("Kasten05");
        sptlKasten05_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten05.mesh.j3o");
        nodeKasten05_.attachChild(sptlKasten05_);
        geomKasten05_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten05_, "Kasten05");
        bbKasten05_ = (BoundingBox) geomKasten05_.getModelBound();
        centerKasten05_ = bbKasten05_.getCenter();
        cshKasten05_ = new MeshCollisionShape(geomKasten05_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten05_, Vector3f.ZERO);
        modelKasten05_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten05_, scaleKampfplatz_, cshKasten05_, "Kasten05");
        controlKasten05_ = new IndestructibleGhostBuilding(modelKasten05_);
        
        // Kasten06
        nodeKasten06_ = new Node("Kasten06");
        sptlKasten06_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten06.mesh.j3o");
        nodeKasten06_.attachChild(sptlKasten06_);
        geomKasten06_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten06_, "Kasten06");
        bbKasten06_ = (BoundingBox) geomKasten06_.getModelBound();
        centerKasten06_ = bbKasten06_.getCenter();
        cshKasten06_ = new MeshCollisionShape(geomKasten06_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten06_, Vector3f.ZERO);
        modelKasten06_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten06_, scaleKampfplatz_, cshKasten06_, "Kasten06");
        controlKasten06_ = new IndestructibleGhostBuilding(modelKasten06_);
        
        // Kasten07
        nodeKasten07_ = new Node("Kasten07");
        sptlKasten07_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten07.mesh.j3o");
        nodeKasten07_.attachChild(sptlKasten07_);
        geomKasten07_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten07_, "Kasten07");
        bbKasten07_ = (BoundingBox) geomKasten07_.getModelBound();
        centerKasten07_ = bbKasten07_.getCenter();
        cshKasten07_ = new MeshCollisionShape(geomKasten07_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten07_, Vector3f.ZERO);
        modelKasten07_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten07_, scaleKampfplatz_, cshKasten07_, "Kasten07");
        controlKasten07_ = new IndestructibleGhostBuilding(modelKasten07_);
        
        // Kasten08
        nodeKasten08_ = new Node("Kasten08");
        sptlKasten08_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten08.mesh.j3o");
        nodeKasten08_.attachChild(sptlKasten08_);
        geomKasten08_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten08_, "Kasten08");
        bbKasten08_ = (BoundingBox) geomKasten08_.getModelBound();
        centerKasten08_ = bbKasten08_.getCenter();
        cshKasten08_ = new MeshCollisionShape(geomKasten08_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten08_, Vector3f.ZERO);
        modelKasten08_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten08_, scaleKampfplatz_, cshKasten08_, "Kasten08");
        controlKasten08_ = new IndestructibleGhostBuilding(modelKasten08_);
        
        // Kasten09
        nodeKasten09_ = new Node("Kasten09");
        sptlKasten09_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten09.mesh.j3o");
        nodeKasten09_.attachChild(sptlKasten09_);
        geomKasten09_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten09_, "Kasten09");
        bbKasten09_ = (BoundingBox) geomKasten09_.getModelBound();
        centerKasten09_ = bbKasten09_.getCenter();
        cshKasten09_ = new MeshCollisionShape(geomKasten09_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten09_, Vector3f.ZERO);
        modelKasten09_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten09_, scaleKampfplatz_, cshKasten09_, "Kasten09");
        controlKasten09_ = new IndestructibleGhostBuilding(modelKasten09_);
        
        // Kasten10
        nodeKasten10_ = new Node("Kasten10");
        sptlKasten10_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten10.mesh.j3o");
        nodeKasten10_.attachChild(sptlKasten10_);
        geomKasten10_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten10_, "Kasten10");
        bbKasten10_ = (BoundingBox) geomKasten10_.getModelBound();
        centerKasten10_ = bbKasten10_.getCenter();
        cshKasten10_ = new MeshCollisionShape(geomKasten10_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten10_, Vector3f.ZERO);
        modelKasten10_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten10_, scaleKampfplatz_, cshKasten10_, "Kasten10");
        controlKasten10_ = new IndestructibleGhostBuilding(modelKasten10_);
        
        // Kasten11
        nodeKasten11_ = new Node("Kasten11");
        sptlKasten11_ = assetManager_.loadModel("Models/Kampfplatz/Koeln1945/Kasten11.mesh.j3o");
        nodeKasten11_.attachChild(sptlKasten11_);
        geomKasten11_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlKasten11_, "Kasten11");
        bbKasten11_ = (BoundingBox) geomKasten11_.getModelBound();
        centerKasten11_ = bbKasten11_.getCenter();
        cshKasten11_ = new MeshCollisionShape(geomKasten11_.getMesh());
        compoundKampfplatz_.addChildShape(cshKasten11_, Vector3f.ZERO);
        modelKasten11_ = new DefaultBattlefieldObjectModel(
            this, sptlKasten11_, scaleKampfplatz_, cshKasten11_, "Kasten11");
        controlKasten11_ = new IndestructibleGhostBuilding(modelKasten11_);
        
        // Feind1Startpunkt1
        nodeFeind1Startpunkt1_ = new Node("Feind1Startpunkt1");
        sptlFeind1Startpunkt1_ = assetManager_.loadModel(
            "Models/Kampfplatz/Koeln1945/Specials/Feind1Startpunkt1.mesh.j3o");
        nodeFeind1Startpunkt1_.attachChild(sptlFeind1Startpunkt1_);
        geomFeind1Startpunkt1_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlFeind1Startpunkt1_, "Feind1Startpunkt1");
        bbFeind1Startpunkt1_ = (BoundingBox) geomFeind1Startpunkt1_.getModelBound();
        centerFeind1Startpunkt1_ = bbFeind1Startpunkt1_.getCenter();
        cshFeind1Startpunkt1_ = new MeshCollisionShape(geomFeind1Startpunkt1_.getMesh());
        compoundKampfplatz_.addChildShape(cshFeind1Startpunkt1_, Vector3f.ZERO);
        modelFeind1Startpunkt1_ = new DefaultBattlefieldObjectModel(
            this, sptlFeind1Startpunkt1_, scaleKampfplatz_, cshFeind1Startpunkt1_, "Feind1Startpunkt1");
        controlFeind1Startpunkt1_ = new StartPoint(modelFeind1Startpunkt1_);
        
        // Feind2Startpunkt1
        nodeFeind2Startpunkt1_ = new Node("Feind2Startpunkt1");
        sptlFeind2Startpunkt1_ = assetManager_.loadModel(
            "Models/Kampfplatz/Koeln1945/Specials/Feind2Startpunkt1.mesh.j3o");
        nodeFeind2Startpunkt1_.attachChild(sptlFeind2Startpunkt1_);
        geomFeind2Startpunkt1_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlFeind2Startpunkt1_, "Feind2Startpunkt1");
        bbFeind2Startpunkt1_ = (BoundingBox) geomFeind2Startpunkt1_.getModelBound();
        centerFeind2Startpunkt1_ = bbFeind2Startpunkt1_.getCenter();
        cshFeind2Startpunkt1_ = new MeshCollisionShape(geomFeind2Startpunkt1_.getMesh());
        compoundKampfplatz_.addChildShape(cshFeind2Startpunkt1_, Vector3f.ZERO);
        modelFeind2Startpunkt1_ = new DefaultBattlefieldObjectModel(
            this, sptlFeind2Startpunkt1_, scaleKampfplatz_, cshFeind2Startpunkt1_, "Feind2Startpunkt1");
        controlFeind2Startpunkt1_ = new StartPoint(modelFeind2Startpunkt1_);
        
        //
//        nodeKampfplatz_.attachChild(nodeWeltende_);
//        nodeKampfplatz_.attachChild(nodeGrund_);
//        nodeKampfplatz_.attachChild(nodeDom_);
//        nodeKampfplatz_.attachChild(nodeHaus00_);
//        nodeKampfplatz_.attachChild(nodeHaus01_);
//        nodeKampfplatz_.attachChild(nodeHaus02_);
//        nodeKampfplatz_.attachChild(nodeHaus03_);
//        nodeKampfplatz_.attachChild(nodeHaus04_);
//        nodeKampfplatz_.attachChild(nodeHaus05_);
//        nodeKampfplatz_.attachChild(nodeHaus06_);
//        nodeKampfplatz_.attachChild(nodeHaus07_);
//        nodeKampfplatz_.attachChild(nodeHaus08_);
//        nodeKampfplatz_.attachChild(nodeHaus09_);
//        nodeKampfplatz_.attachChild(nodeHaus10_);
//        nodeKampfplatz_.attachChild(nodeHaus11_);
//        nodeKampfplatz_.attachChild(nodeHaus12_);
//        nodeKampfplatz_.attachChild(nodeHaus13_);
//        nodeKampfplatz_.attachChild(nodeHaus14_);
//        nodeKampfplatz_.attachChild(nodeHaus15_);
//        nodeKampfplatz_.attachChild(nodeHaus16_);
//        nodeKampfplatz_.attachChild(nodeHaus17_);
//        nodeKampfplatz_.attachChild(nodeHaus18_);
//        nodeKampfplatz_.attachChild(nodeHaus19_);
//        nodeKampfplatz_.attachChild(nodeHaus20_);
//        nodeKampfplatz_.attachChild(nodeHaus21_);
//        nodeKampfplatz_.attachChild(nodeKasten00_);
//        nodeKampfplatz_.attachChild(nodeKasten01_);
//        nodeKampfplatz_.attachChild(nodeKasten02_);
//        nodeKampfplatz_.attachChild(nodeKasten03_);
//        nodeKampfplatz_.attachChild(nodeKasten04_);
//        nodeKampfplatz_.attachChild(nodeKasten05_);
//        nodeKampfplatz_.attachChild(nodeKasten06_);
//        nodeKampfplatz_.attachChild(nodeKasten07_);
//        nodeKampfplatz_.attachChild(nodeKasten08_);
//        nodeKampfplatz_.attachChild(nodeKasten09_);
//        nodeKampfplatz_.attachChild(nodeKasten10_);
//        nodeKampfplatz_.attachChild(nodeKasten11_);
//        nodeKampfplatz_.attachChild(nodeFeind1Startpunkt1_);
//        nodeKampfplatz_.attachChild(nodeFeind2Startpunkt1_);
        
        // Stop timer
        final long buildStop_ = System.currentTimeMillis();
        final float buildTime_ = ((float) (buildStop_ - buildStart_)) / 1000.0f;
        System.out.println("Battlefield build time: " + buildTime_ + " sec." );
    }
    
    /**
     * The method adds nodes, controls, and other required parts to the scene.
     */
    @Override
    public void addToScene() {
        //
        // The jMonkeyEngine v.3.2 has a strange bug with scaling that appears
        // as scaling is applying twice for spatials however only once for collision 
        // shapes. The following block of code is the workaround for this bug.
        float s_ = scaleKampfplatz_.getFloatValue();
        Vector3f sv_ = new Vector3f(s_, s_, s_);
        controlKampfplatz_.getSpatial().scale(s_);
        controlKampfplatz_.getCollisionShape().setScale(sv_);
        controlWeltendeNord_.getCollisionShape().setScale(sv_);
        controlWeltendeSued_.getCollisionShape().setScale(sv_);
        controlWeltendeOst_.getCollisionShape().setScale(sv_);
        controlWeltendeWest_.getCollisionShape().setScale(sv_);
        controlGrund_.getCollisionShape().setScale(sv_);
        controlDom_.getCollisionShape().setScale(sv_);
        controlHaus00_.getCollisionShape().setScale(sv_);
        controlHaus01_.getCollisionShape().setScale(sv_);
        controlHaus02_.getCollisionShape().setScale(sv_);
        controlHaus03_.getCollisionShape().setScale(sv_);
        controlHaus04_.getCollisionShape().setScale(sv_);
        controlHaus05_.getCollisionShape().setScale(sv_);
        controlHaus06_.getCollisionShape().setScale(sv_);
        controlHaus07_.getCollisionShape().setScale(sv_);
        controlHaus08_.getCollisionShape().setScale(sv_);
        controlHaus09_.getCollisionShape().setScale(sv_);
        controlHaus10_.getCollisionShape().setScale(sv_);
        controlHaus11_.getCollisionShape().setScale(sv_);
        controlHaus12_.getCollisionShape().setScale(sv_);
        controlHaus13_.getCollisionShape().setScale(sv_);
        controlHaus14_.getCollisionShape().setScale(sv_);
        controlHaus15_.getCollisionShape().setScale(sv_);
        controlHaus16_.getCollisionShape().setScale(sv_);
        controlHaus17_.getCollisionShape().setScale(sv_);
        controlHaus18_.getCollisionShape().setScale(sv_);
        controlHaus19_.getCollisionShape().setScale(sv_);
        controlHaus20_.getCollisionShape().setScale(sv_);
        controlHaus21_.getCollisionShape().setScale(sv_);
        controlKasten00_.getCollisionShape().setScale(sv_);
        controlKasten01_.getCollisionShape().setScale(sv_);
        controlKasten02_.getCollisionShape().setScale(sv_);
        controlKasten03_.getCollisionShape().setScale(sv_);
        controlKasten04_.getCollisionShape().setScale(sv_);
        controlKasten05_.getCollisionShape().setScale(sv_);
        controlKasten06_.getCollisionShape().setScale(sv_);
        controlKasten07_.getCollisionShape().setScale(sv_);
        controlKasten08_.getCollisionShape().setScale(sv_);
        controlKasten09_.getCollisionShape().setScale(sv_);
        controlKasten10_.getCollisionShape().setScale(sv_);
        controlKasten11_.getCollisionShape().setScale(sv_);
        controlFeind1Startpunkt1_.getCollisionShape().setScale(sv_);
        controlFeind2Startpunkt1_.getCollisionShape().setScale(sv_);
        //
        rootNode_.attachChild(nodeKampfplatz_);
        rootNode_.attachChild(nodeWeltendeNord_);
        rootNode_.attachChild(nodeWeltendeSued_);
        rootNode_.attachChild(nodeWeltendeOst_);
        rootNode_.attachChild(nodeWeltendeWest_);
        rootNode_.attachChild(nodeFeind1Startpunkt1_);
        rootNode_.attachChild(nodeFeind2Startpunkt1_);
        physicsSpace_.add(controlKampfplatz_);
        physicsSpace_.add(controlWeltendeNord_);
        physicsSpace_.add(controlWeltendeSued_);
        physicsSpace_.add(controlWeltendeOst_);
        physicsSpace_.add(controlWeltendeWest_);
        physicsSpace_.add(controlGrund_);
        physicsSpace_.add(controlDom_);
        physicsSpace_.add(controlHaus00_);
        physicsSpace_.add(controlHaus01_);
        physicsSpace_.add(controlHaus02_);
        physicsSpace_.add(controlHaus03_);
        physicsSpace_.add(controlHaus04_);
        physicsSpace_.add(controlHaus05_);
        physicsSpace_.add(controlHaus06_);
        physicsSpace_.add(controlHaus07_);
        physicsSpace_.add(controlHaus08_);
        physicsSpace_.add(controlHaus09_);
        physicsSpace_.add(controlHaus10_);
        physicsSpace_.add(controlHaus11_);
        physicsSpace_.add(controlHaus12_);
        physicsSpace_.add(controlHaus13_);
        physicsSpace_.add(controlHaus14_);
        physicsSpace_.add(controlHaus15_);
        physicsSpace_.add(controlHaus16_);
        physicsSpace_.add(controlHaus17_);
        physicsSpace_.add(controlHaus18_);
        physicsSpace_.add(controlHaus19_);
        physicsSpace_.add(controlHaus20_);
        physicsSpace_.add(controlHaus21_);
        physicsSpace_.add(controlKasten00_);
        physicsSpace_.add(controlKasten01_);
        physicsSpace_.add(controlKasten02_);
        physicsSpace_.add(controlKasten03_);
        physicsSpace_.add(controlKasten04_);
        physicsSpace_.add(controlKasten05_);
        physicsSpace_.add(controlKasten06_);
        physicsSpace_.add(controlKasten07_);
        physicsSpace_.add(controlKasten08_);
        physicsSpace_.add(controlKasten09_);
        physicsSpace_.add(controlKasten10_);
        physicsSpace_.add(controlKasten11_);
        physicsSpace_.add(controlFeind1Startpunkt1_);
        physicsSpace_.add(controlFeind2Startpunkt1_);
    }
    
    /**
     * The method is called to update this application state. This method will
     * be called every render pass if the <code>AppState</code> is both attached
     * and enabled.
     *
     * @param tpf Time since the last call to update(), in seconds.
     */
    @Override
    public void update(final float tpf) {
        if (isOnStart_ == true) {
            if (PROPONENTENGRUPPE_.isEmpty() == false) {
                for (Kombattant k_ : PROPONENTENGRUPPE_) {
                    k_.getSpatial().setLocalTranslation(PROPONENTENGRUPPE_STARTPUNKT1_);
                }
            }
            if (OPPONENTENGRUPPE_.isEmpty() == false) {
                for (Kombattant k_ : OPPONENTENGRUPPE_) {
                    k_.getSpatial().setLocalTranslation(OPPONENTENGRUPPE_STARTPUNKT1_);
                }
            }
            isOnStart_ = false;
        }
    }
    
    /**
     * The method is called when an input to which this listener is registered
     * to is invoked.
     *
     * @param binding The name of the mapping that was invoked.
     * @param isPressed True if the action is "pressed", false otherwise.
     * @param tpf The time per frame value.
     */
    @Override
    public void onAction(final String binding, final boolean isPressed, final float tpf) {
        // TODO
    }

    /**
     * The method realizes the analog handler for mouse movement events. It is
     * assumed that we want horizontal movements to turn the character, while
     * vertical movements only make the camera rotate up or down.
     *
     * @param binding The name of the mapping that was invoked.
     * @param value Value of the axis, from 0 to 1.
     * @param tpf The time per frame value.
     */
    @Override
    public void onAnalog(final String binding, final float value, final float tpf) {
        // TODO
    }

    /**
     * The method is called when a collision happened in the PhysicsSpace, and
     * it is called from the render thread. Do not store the event object as it
     * will be cleared after the method has finished.
     *
     * @param event The collision event.
     */
    @Override
    public void collision(final PhysicsCollisionEvent event) {
        // TODO
    }

    /**
     * The method is called before the physics is actually stepped, use to apply 
     * forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called before each step, 
     * and here you can apply forces (change the state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void prePhysicsTick(final PhysicsSpace space, final float tpf) {
        // TODO
    }

    /**
     * The method is called after the physics has been stepped, use to check for 
     * forces etc.
     * 
     * The jBullet Physics implementation is stepped at a constant 60 physics 
     * ticks per second frame rate. Applying forces or checking for overlaps 
     * only has an effect right at a physics update cycle, which is not every 
     * frame. If you do physics interactions at arbitrary spots in the 
     * simpleUpdate() loop, calls will be dropped at irregular intervals, 
     * because they happen out of cycle. This method is called after each step, 
     * and here you can poll the results (get the current state).
     * 
     * @param space The physics space.
     * @param tpf The time per frame in seconds.
     */
    @Override
    public void physicsTick(final PhysicsSpace space, final float tpf) {
        // TODO
    }
    
    /**
     * The method adds a new combat participator to the team of proponents acting 
     * on the battlefield.
     * 
     * @param kombattant A new combat participator to be added to the team of 
     * proponents acting on the battlefield.
     */
    @Override
    public void addProponent(final Kombattant kombattant) {
        if (kombattant != null) {
            PROPONENTENGRUPPE_.add(kombattant);
        }
    }
    
    /**
     * The method adds a new combat participator to the team of opponents acting 
     * on the battlefield.
     * 
     * @param kombattant A new combat participator to be added to the team of 
     * opponents acting on the battlefield.
     */
    @Override
    public void addOpponent(final Kombattant kombattant) {
        if (kombattant != null) {
            OPPONENTENGRUPPE_.add(kombattant);
        }
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}