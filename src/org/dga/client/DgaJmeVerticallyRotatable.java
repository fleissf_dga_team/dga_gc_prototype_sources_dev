/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client;

/**
 * The common interface for vertically rotatable game objects.
 *
 * @extends DgaJmeRotatable
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 13.7.2018
 */
public interface DgaJmeVerticallyRotatable extends DgaJmeRotatable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method sets the maximum elevation angle for the game object in degrees.
     *
     * @param angle The maximum elevation angle for the game object in degrees.
     */
    public void setMaxVerticalAngle(final int angle);

    /**
     * The method sets the maximum elevation angle for the game object in radians.
     *
     * @param angle The maximum elevation angle for the game object in radians.
     */
    public void setMaxVerticalAngle(final float angle);

    /**
     * The method returns the maximum elevation angle for the game object in radians.
     *
     * @return float The maximum elevation angle for the game object in radians.
     */
    public float getMaxVerticalAngle();

    /**
     * The method sets the minimum depression angle for the game object in degrees.
     *
     * @param angle The minimum depression angle for the game object in degrees.
     */
    public void setMinVerticalAngle(final int angle);

    /**
     * The method sets the minimum depression angle for the game object in radians.
     *
     * @param angle The minimum depression angle for the game object in radians.
     */
    public void setMinVerticalAngle(final float angle);

    /**
     * The method returns the minimum depression angle for the game object in radians.
     *
     * @return float The minimum depression angle for the game object in radians.
     */
    public float getMinVerticalAngle();
    
    /**
     * The method sets a vertical angle for the game object in radians.
     *
     * @param angle A vertical angle for the game object in radians.
     */
    public void setVerticalRotation(final float angle);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
