/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kamera;

import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.Trigger;
import com.jme3.math.Vector3f;
import com.jme3.scene.control.Control;
import com.jme3.util.clone.JmeCloneable;

/**
 * The interface of a chase camera.
 * 
 * @extends com.jme3.scene.control.Control
 * @extends com.jme3.util.clone.JmeCloneable
 * @extends com.jme3.input.controls.ActionListener
 * @extends com.jme3.input.controls.AnalogListener
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 30.4.2018
 */
public interface DgaChaseCamera extends DgaCamera, Control, JmeCloneable, 
    ActionListener, AnalogListener {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method registers inputs with the input manager.
     *
     * @param inputManager The input manager.
     */
    public void registerWithInput(final InputManager inputManager);
    
    /**
     * The method cleans up the input mappings from the input manager. This method
     * undoes the work of the method registerWithInput.
     *
     * @param inputManager The input manager from which to cleanup mappings.
     */
    public void cleanUpWithInput(final InputManager inputManager);
    
    /**
     * The method sets custom triggers for toggling the rotation of the camera.
     * Defaults are left mouse button MouseButtonTrigger(MouseInput.BUTTON_LEFT)
     * and right mouse button MouseButtonTrigger(MouseInput.BUTTON_RIGHT).
     *
     * @param triggers Custom triggers for toggling the rotation of the camera.
     */
    public void setToggleRotationTrigger(final Trigger... triggers);
    
    /**
     * The method sets custom triggers for zooming in the camera. Default is mouse
     * wheel up MouseAxisTrigger(MouseInput.AXIS_WHEEL, true).
     *
     * @param triggers  Custom triggers for zooming in the camera.
     */
    public void setZoomInTrigger(final Trigger... triggers);
    
    /**
     * The method sets custom triggers for zooming out the camera. Default is 
     * mouse wheel down MouseAxisTrigger(MouseInput.AXIS_WHEEL, false).
     *
     * @param triggers Custom triggers for zooming out the camera.
     */
    public void setZoomOutTrigger(final Trigger... triggers);
    
    /**
     * The method returns whether the camera is enabled or not.
     *
     * @return boolean Whether the camera is enabled or not.
     */
    public boolean isEnabled();
    
    /**
     * The method enables or disables the camera.
     *
     * @param enabled True to enable, false otherwise.
     */
    public void setEnabled(final boolean enabled);
    
    /**
     * The method returns the maximum zoom distance of the camera (default is 40).
     *
     * @return float The maximum zoom distance of the camera.
     */
    public float getMaxDistance();
    
    /**
     * The method sets the maximum zoom distance of the camera (default is 40).
     *
     * @param maxDistance The maximum zoom distance of the camera.
     */
    public void setMaxDistance(final float maxDistance);
    
    /**
     * The method returns the minimal zoom distance of the camera (default is 1).
     *
     * @return minDistance The minimal zoom distance of the camera.
     */
    public float getMinDistance();
    
    /**
     * The method sets the minimal zoom distance of the camera (default is 1).
     *
     * @param minDistance The minimal zoom distance of the camera.
     */
    public void setMinDistance(final float minDistance);
    
    /**
     * The method returns the maximum vertical rotation of the camera.
     * 
     * @return float The maximal vertical rotation angle in radian of the camera
     * around the target.
     */
    public float getMaxVerticalRotation();
    
    /**
     * The method sets the maximum vertical rotation angle in radians of the
     * camera around the target. Default is Pi/2.
     *
     * @param maxVerticalRotation The maximum vertical rotation angle in radians.
     */
    public void setMaxVerticalRotation(final float maxVerticalRotation);
    
    /**
     * The method returns the minimal vertical rotation of the camera.
     * 
     * @return float The minimal vertical rotation angle in radians of the camera
     * around the target.
     */
    public float getMinVerticalRotation();
    
    /**
     * The method sets the minimal vertical rotation angle in radians of the
     * camera around the target. Default is 0.
     *
     * @param minHeight The minimal vertical rotation angle in radians.
     */
    public void setMinVerticalRotation(final float minHeight);
    
    /**
     * The method returns whether the camera's smooth motion is enabled or not.
     * 
     * @return boolean True is smooth motion is enabled for this camera or false otherwise.
     */
    public boolean isSmoothMotion();
    
    /**
     * The method enables smooth motion for this camera.
     *
     * @param smoothMotion The flag of smooth motion.
     */
    public void setSmoothMotion(final boolean smoothMotion);
    
    /**
     * The method returns the chasing sensitivity of the camera.
     *
     * @return float The chasing sensitivity of the camera.
     */
    public float getChasingSensitivity();
    
    /**
     * The method sets the chasing sensitivity. The lower the value is the slower
     * the camera will follow the target when it moves. Default is 5. Only has
     * an effect if smoothMotion is set to true and trailing is enabled.
     *
     * @param chasingSensitivity The chasing sensitivity.
     */
    public void setChasingSensitivity(final float chasingSensitivity);
    
    /**
     * The method returns the rotation sensitivity of the camera.
     *
     * @return float The rotation sensitivity of the camera.
     */
    public float getRotationSensitivity();
    
    /**
     * The method sets the rotation sensitivity. The lower the value is the slower
     * the camera will rotates around the target when dragging with the mouse.
     * Default is 5, values over 5 should have no effect. If you want a significant
     * slow down then try values below 1. Only has an effect if smoothMotion is set
     * to true.
     *
     * @param rotationSensitivity The rotation sensitivity.
     */
    public void setRotationSensitivity(final float rotationSensitivity);
    
    /**
     * The method returns true if the trailing is enabled.
     *
     * @return boolean The flag of trailing for the camera.
     */
    public boolean isTrailingEnabled();
    
    /**
     * The method enables the camera trailing. The camera can smoothly go in the
     * targets trail when it moves. The method only has an effect if smoothMotion
     * is set to true.
     *
     * @param trailingEnabled The flag of camera trailing.
     */
    public void setTrailingEnabled(final boolean trailingEnabled);
    
    /**
     * The method returns the trailing rotation inertia of the camera.
     *
     * @return float The value of trailing rotation inertia.
     */
    public float getTrailingRotationInertia();
    
    /**
     * The method sets the trailing rotation inertia of the camera. Default is 0.1.
     * This prevents the camera to roughly stop when the target stops or moving
     * before the camera reached the trail position. Only has an effect if
     * smoothMotion is set to true and trailing is enabled
     *
     * @param trailingRotationInertia The trailing rotation inertia of the camera.
     */
    public void setTrailingRotationInertia(final float trailingRotationInertia);
    
    /**
     * The method returns the trailing sensitivity of the camera.
     *
     * @return float The trailing sensitivity of the camera.
     */
    public float getTrailingSensitivity();
    
    /**
     * The method sets the trailing sensitivity for this camera. The lower the
     * value is the slower the camera will go in the target trail when it moves.
     * Default is 0.5. Only has an effect if smoothMotion is set to true and
     * trailing is enabled.
     *
     * @param trailingSensitivity The trailing sensitivity for this camera.
     */
    public void setTrailingSensitivity(final float trailingSensitivity);
    
    /**
     * The method returns the zoom sensitivity.
     *
     * @return float The value of zoom sensitivity.
     */
    public float getZoomSensitivity();
    
    /**
     * The method sets the zoom sensitivity for this camera. The lower the value
     * is the slower the camera will zoom in and out. Default is 2.
     *
     * @param zoomSensitivity The zoom sensitivity for this camera.
     */
    public void setZoomSensitivity(final float zoomSensitivity);
    
    /**
     * The method returns the rotation speed when the mouse is moved.
     *
     * @return float The rotation speed when the mouse is moved.
     */
    public float getRotationSpeed();
    
    /**
     * The method sets the rotation amount when user moves the mouse. The lower
     * the value is the slower the camera will rotate. Default is 1.
     *
     * @param rotationSpeed The rotation speed on mouse movement.
     */
    public void setRotationSpeed(final float rotationSpeed);
    
    /**
     * The method sets the default distance at the application's start.
     *
     * @param defaultDistance The default distance at the application's start.
     */
    public void setDefaultDistance(final float defaultDistance);
    
    /**
     * The method sets the default horizontal rotation in radians of the camera
     * at the application's start.
     *
     * @param angleInRadians The default horizontal rotation in radians.
     */
    public void setDefaultHorizontalRotation(final float angleInRadians);
    
    /**
     * The method sets the default vertical rotation in radians of the camera at
     * the application's start.
     *
     * @param angleInRadians The default vertical rotation in radians.
     */
    public void setDefaultVerticalRotation(final float angleInRadians);
    
    /**
     * The method returns whether drag capability to rotate is set or not.
     * 
     * @return boolean True if drag to rotate feature is enabled.
     * 
     * @see FlyByCamera#setDragToRotate(boolean)
     */
    public boolean isDragToRotate();
    
    /**
     * The method sets drag capability to rotate. When it sets to true, the user
     * must hold the mouse button and drag over the screen to rotate the camera,
     * and the cursor is visible until dragged. Otherwise, the cursor is invisible
     * at all times and holding the mouse button is not needed to rotate the camera.
     * This feature is disabled by default.
     * 
     * @param dragToRotate Drag capability to rotate.
     */
    public void setDragToRotate(final boolean dragToRotate);
    
    /**
     * The method sets down rotate capability on close view only. When this flag
     * is set to false the camera will always rotate around its spatial
     * independently of their distance to one another. If set to true, the chase
     * camera will only be allowed to rotated below the "horizon" when the distance
     * is smaller than minDistance + 1.0f (when fully zoomed-in).
     * 
     * @param rotateOnlyWhenClose  Down rotate capability on close view only.
     */
    public void setDownRotateOnCloseViewOnly(final boolean rotateOnlyWhenClose);
    
    /**
     * The method returns down rotate capability on close view only. The method
     * returns true if rotation below the vertical plane of the spatial tied to
     * the camera is allowed only when zoomed in at minDistance + 1.0f. The 
     * method returns false if vertical rotation is always allowed.
     * 
     * @return boolean True if down rotate capability on close view only is enabled.
     */
    public boolean getDownRotateOnCloseViewOnly();
    
    /**
     * The method returns the current distance from the camera to the target.
     *
     * @return float The current distance from the camera to the target.
     */
    public float getDistanceToTarget();
    
    /**
     * The method returns the current horizontal rotation around the target in radians.
     *
     * @return float The current horizontal rotation around the target in radians.
     */
    public float getHorizontalRotation();
    
    /**
     * The method returns the current vertical rotation around the target in radians.
     *
     * @return float The current vertical rotation around the target in radians.
     */
    public float getVerticalRotation();
    
    /**
     * The method returns the offset from the target's position where the camera looks at.
     *
     * @return Vector3f The offset from the target's position where the camera looks at.
     */
    public Vector3f getLookAtOffset();
    
    /**
     * The method sets the offset from the target's position where the camera looks at.
     *
     * @param lookAtOffset The offset from the target's position where the camera looks at.
     */
    public void setLookAtOffset(final Vector3f lookAtOffset);
    
    /**
     * The method sets the up vector of the camera used for the lookAt on the target.
     *
     * @param up The up vector of the camera used for the lookAt on the target.
     */
    public void setUpVector(final Vector3f up);
    
    /**
     * The method returns the up vector of the camera used for the lookAt on the target.
     *
     * @return Vector3f The up vector of the camera used for the lookAt on the target.
     */
    public Vector3f getUpVector();
    
    /**
     * The method returns the hide cursor property on rotate.
     * 
     * @return boolean The flag of the hide cursor property on rotate.
     */
    public boolean isHideCursorOnRotate();
    
    /**
     * The method sets the hide cursor property on rotate.
     * 
     * @param hideCursorOnRotate The hide cursor property on rotate.
     */
    public void setHideCursorOnRotate(final boolean hideCursorOnRotate);
    
    /**
     * The method inverts the vertical axis movement of the mouse.
     *
     * @param invertYaxis The flag to invert the vertical axis movement of the mouse.
     */
    public void setInvertVerticalAxis(final boolean invertYaxis);
    
    /**
     * The method inverts the horizontal axis movement of the mouse.
     *
     * @param invertXaxis The flag to invert the horizontal axis movement of the mouse.
     */
    public void setInvertHorizontalAxis(final boolean invertXaxis);
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
