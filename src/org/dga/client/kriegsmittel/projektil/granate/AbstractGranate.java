/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.projektil.granate;

import org.dga.client.kriegsmittel.projektil.AbstractFeuerprojektil;
import org.dga.client.kraftwerk.Schaden;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import org.dga.client.effekt.DefaultExplosionEffect;
import org.dga.client.effekt.ExplosionEffect;

/**
 * The abstract implementation of a shell.
 *
 * @extends AbstractFeuerprojektil
 * @implements Granate
 * @implements com.jme3.bullet.control.PhysicsControl
 * @implements com.jme3.util.clone.JmeCloneable
 * @implements com.jme3.bullet.collision.PhysicsCollisionListener
 * @implements com.jme3.bullet.PhysicsTickListener
 * @pattern Adapter
 * @pattern Template Method
 * @pattern Inversion of Control (IoC)
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 8.5.2018
 */
public abstract class AbstractGranate extends AbstractFeuerprojektil implements Granate {
    private static final float EFFECT_MAX_TIME_ = 3.0f;
    private static final float FLY_OUT_MAX_TIME_ = 6.0f;
    private AssetManager assetManager_ = null;
    private GranateModel modelGranate_ = null;
    private float effectRadius_ = 10.0f;
    private float explosionRadius_ = 10.0f;
    private float emitterRadius_ = 4.0f;
    private float forceFactor_ = 10.0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param granateModel The shell's model.
     */
    public AbstractGranate(final GranateModel granateModel) {
        super(granateModel);
        modelGranate_ = granateModel;
        assetManager_ = modelGranate_.getAssetManager();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the effects provided by the projectile in time of 
     * impact on the obstacle.
     * 
     * @return ExplosionEffect The effects provided by the projectile in time of 
     * impact on the obstacle.
     */
    @Override
    public ExplosionEffect getExplosionEffect() {
        return DefaultExplosionEffect.newBuilder(assetManager_)
            .addFlame(false, 64)
                .setFlameStartSize(2.0f).setFlameEndSize(4.0f)
                .setFlameGravity(new Vector3f(0f, -5.0f, 0f))
                .setFlameLowLife(0.8f).setFlameHighLife(1.0f)
                .setFlameEmitterRadius(emitterRadius_)
            .addExplosionObject(effectRadius_)
            .build();
    }

    /**
     * The method returns the effect radius of the projectile.
     * 
     * @return float The effect radius of the projectile.
     */
    @Override
    public float getEffectRadius() {
        return effectRadius_;
    }

    /**
     * The method sets the effect radius of the projectile.
     * 
     * @param effectRadius The effect radius of the projectile.
     */
    @Override
    public void setEffectRadius(final float effectRadius) {
        effectRadius_ = effectRadius;
    }
    
    /**
     * The method returns the explosion radius of the projectile.
     * 
     * @return float The explosion radius of the projectile.
     */
    @Override
    public float getExplosionRadius() {
        return explosionRadius_;
    }

    /**
     * The method sets the explosion radius of the projectile.
     * 
     * @param explosionRadius The explosion radius of the projectile.
     */
    @Override
    public void setExplosionRadius(final float explosionRadius) {
        explosionRadius_ = explosionRadius;
    }

    /**
     * The method returns the emitter radius of the projectile.
     * 
     * @return float The emitter radius of the projectile.
     */
    @Override
    public float getEmitterRadius() {
        return emitterRadius_;
    }

    /**
     * The method sets the emitter radius of the projectile.
     * 
     * @param emitterRadius The emitter radius of the projectile.
     */
    @Override
    public void setEmitterRadius(final float emitterRadius) {
        emitterRadius_ = emitterRadius;
    }
    
    /**
     * The method returns the force factor of the projectile.
     * 
     * @return float The force factor of the projectile.
     */
    @Override
    public float getForceFactor() {
        return forceFactor_;
    }

    /**
     * The method sets the force factor of the projectile.
     * 
     * @param forceFactor The force factor of the projectile.
     */
    @Override
    public void setForceFactor(final float forceFactor) {
        forceFactor_ = forceFactor;
    }
    
    /**
     * The method returns the damage which the shell can apply to an armored 
     * weapon and by that diminish its fighting capacity.
     * 
     * @return Panzerungschaden The damage which the shell can apply to an 
     * armored weapon and by that diminish its fighting capacity.
     */
    @Override
    public abstract Schaden getSchaden();
    
    /**
     * The method returns the maximum time of visibility in seconds for a 
     * projectile flying out of the battlefield.
     * 
     * @return float The maximum time of visibility in seconds for a projectile 
     * flying out of the battlefield.
     */
    @Override
    public float getFlyingOutMaxTime() {
        return FLY_OUT_MAX_TIME_;
    }
    
    /**
     * The method returns the maximum time in seconds the effect works out.
     * 
     * @return float The maximum time in seconds the effect works out.
     */
    @Override
    public float getEffectMaxTime() {
        return EFFECT_MAX_TIME_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
