/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import org.dga.client.DgaJmeMassable;

/**
 * The interface of a weapon.
 * 
 * @extends Kriegsmittel
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.4.2018
 */
public interface Waffe extends Kriegsmittel, DgaJmeMassable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the weapon's world translation which is defined by the 
     * type of weapons, e.g. for a cannon it is tranlsation of the cannon's barrel.
     * 
     * @return Vector3f The weapon's world translation which is defined by the 
     * type of weapons, e.g. for a cannon it is tranlsation of the cannon's barrel.
     */
    public Vector3f getWaffeWorldTranslation();
    
    /**
     * The method returns the weapon's world rotation which is defined by the 
     * type of weapons, e.g. for a cannon it is tranlsation of the cannon's barrel.
     * 
     * @return Quaternion The weapon's world rotation which is defined by the 
     * type of weapons, e.g. for a cannon it is tranlsation of the cannon's barrel.
     */
    public Quaternion getWaffeWorldRotation();
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
