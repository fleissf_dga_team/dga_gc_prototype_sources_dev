/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.panzerung;

import org.dga.client.kriegsmittel.SchutzModel;
import com.jme3.bounding.BoundingBox;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 * The interface of a model for an armour.
 * 
 * @extends SchutzModel
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 5.7.2018
 */
public interface PanzerungModel extends SchutzModel {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the front armoring value.
     * 
     * @return float The front armoring value.
     */
    public float getFrontArmoring();

    /**
     * The method sets the front armoring value.
     * 
     * @param frontArmoring The front armoring value.
     */
    public void setFrontArmoring(final float frontArmoring);

    /**
     * The method returns the left armoring value.
     * 
     * @return float The left armoring value.
     */
    public float getLeftArmoring();

    /**
     * The method sets the left armoring value.
     * 
     * @param leftArmoring The left armoring value.
     */
    public void setLeftArmoring(final float leftArmoring);

    /**
     * The method returns the right armoring value.
     * 
     * @return float The right armoring value.
     */
    public float getRightArmoring();

    /**
     * The method sets the right armoring value.
     * 
     * @param rightArmoring The right armoring value.
     */
    public void setRightArmoring(final float rightArmoring);

    /**
     * The method returns the rear armoring value.
     * 
     * @return float The rear armoring value.
     */
    public float getRearArmoring();

    /**
     * The method sets the rear armoring value.
     * 
     * @param backArmoring The rear armoring value.
     */
    public void setRearArmoring(final float backArmoring);

    /**
     * The method returns the top armoring value.
     * 
     * @return float The top armoring value.
     */
    public float getTopArmoring();

    /**
     * The method sets the top armoring value.
     * 
     * @param topArmoring The top armoring value.
     */
    public void setTopArmoring(final float topArmoring);

    /**
     * The method returns the bottom armoring value.
     * 
     * @return float The bottom armoring value.
     */
    public float getBottomArmoring();

    /**
     * The method sets the bottom armoring value.
     * 
     * @param bottomArmoring The front armoring value.
     */
    public void setBottomArmoring(final float bottomArmoring);
    
    /**
     * The method returns the front orientation point.
     * 
     * @return Spatial The front orientation point.
     */
    public Spatial getVorpunkt();

    /**
     * The method sets the front orientation point.
     * 
     * @param vorpunktSpatial The front orientation point.
     */
    public void setVorpunkt(final Spatial vorpunktSpatial);

    /**
     * The method returns the flag whether the model has a front orientation point or not.
     * 
     * @return boolean The flag whether the model has a front orientation point or not.
     */
    public boolean hasVorpunkt();
    
    /**
     * The method sets the bounding box defining the model's measures.
     * 
     * @param measures The bounding box defining the model's measures.
     */
    public void setBoundingBox(final BoundingBox measures);
    
    /**
     * The method returns the model's geometric center updating during the movement.
     * 
     * @return Vector3f The model's geometric center updating during the movement.
     */
    public Vector3f getCenter();
    
    /**
     * The method returns the model's length.
     * 
     * @return float The model's length.
     */
    public float getLength();
    
    /**
     * The method returns the model's width.
     * 
     * @return float The model's width.
     */
    public float getWidth();
    
    /**
     * The method returns the model's height.
     * 
     * @return float The model's height.
     */
    public float getHeight();
    
    /**
     * The method returns the model's half-length.
     * 
     * @return float The model's half-length.
     */
    public float getHalfLength();
    
    /**
     * The method returns the model's half-width.
     * 
     * @return float The model's half-width.
     */
    public float getHalfWidth();
    
    /**
     * The method returns the model's half-height.
     * 
     * @return float The model's half-height.
     */
    public float getHalfHeight();
    
    /**
     * The method returns the flag whether the model has measures or not.
     * 
     * @return boolean The flag whether the model has measures or not.
     */
    public boolean hasMeasures();
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
