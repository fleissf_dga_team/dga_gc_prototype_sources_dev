/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import org.dga.client.effekt.FireEffect;
import org.dga.client.kriegsmittel.controller.KriegsmittelFreeRotatableControl;
import org.dga.client.kriegsmittel.controller.KriegsmittelGhostControl;
import java.util.List;
import org.dga.client.DgaJmeScale;

/**
 * The interface of a shooting armament.
 * 
 * @param <M> The shooting armament's ammunition.
 * @extends Waffe
 * @extends KriegsmittelGhostControl
 * @extends KriegsmittelFreeRotatableControl
 * @extends com.jme3.bullet.control.PhysicsControl
 * @extends com.jme3.util.clone.JmeCloneable
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 24.4.2018
 */
public interface Schusswaffe<M extends Munition> 
    extends Waffe, KriegsmittelGhostControl, KriegsmittelFreeRotatableControl {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method sets the maximum horizontal angle of the shooting armament's fire sector in degrees.
     *
     * @param angle The maximum horizontal angle of the shooting armament's fire sector in degrees.
     */
    public void setMaxFireSectorAngle(final int angle);

    /**
     * The method sets the maximum horizontal angle of the shooting armament's fire sector in radians.
     *
     * @param angle The maximum horizontal angle of the shooting armament's fire sector in radians.
     */
    public void setMaxFireSectorAngle(final float angle);
    
    /**
     * The method returns the maximum horizontal angle of the shooting armament's fire sector in radians.
     *
     * @return float The maximum horizontal angle of the shooting armament's fire sector in radians.
     */
    public float getMaxFireSectorAngle();
    
    /**
     * The method realizes a shot from the shooting armament.
     */
    public void fire();
    
    /**
     * The method returns whether the shooting armament is ready to fire or not.
     * 
     * @return boolean The flag whether the shooting armament is ready to fire or not.
     */
    public boolean isFireReady();
    
    /**
     * The method sets whether the shooting armament is ready to fire or not.
     * 
     * @param isFireReady The flag whether the shooting armament is ready to fire or not.
     */
    public void setFireReady(final boolean isFireReady);
    
    /**
     * The method returns the time in seconds that the shooting armament needs 
     * to be loaded with a round.
     * 
     * @return float The time in seconds that the shooting armament needs to be 
     * loaded with a round.
     */
    public float getLoadTime();
    
    /**
     * The method sets the time in seconds that the shooting armament needs to be 
     * loaded with a round (cartridge).
     * 
     * @param loadTime The time in seconds that the shooting armament needs to be 
     * loaded with a round (cartridge).
     */
    public void setLoadTime(final float loadTime);
    
    /**
     * The method returns the capacity of the shooting armament's cartridge.
     * 
     * @return int The capacity of the shooting armament's cartridge.
     */
    public int getCartridgeCapacity();
    
    /**
     * The method returns a list of ammunition of the shooting armament.
     * 
     * @return List<M> The list of ammunition of the shooting armament.
     */
    public List<M> getMunitionList();
    
    /**
     * The method returns the scale used for the shooting armament's ammunition.
     * 
     * @return DgaJmeScale The scale used for the shooting armament's ammunition.
     */
    public DgaJmeScale getMunitionScale();
    
    /**
     * The method sets a new scale for the shooting armament's ammunition.
     * 
     * @param munitionScale A new scale for the shooting armament's ammunition.
     */
    public void setMunitionScale(final DgaJmeScale munitionScale);
    
    /**
     * The method returns an effect provided by the shooting armament during a shot.
     * 
     * @return FireEffect An effect provided by the shooting armament during a shot.
     */
    public FireEffect getFireEffect();
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
