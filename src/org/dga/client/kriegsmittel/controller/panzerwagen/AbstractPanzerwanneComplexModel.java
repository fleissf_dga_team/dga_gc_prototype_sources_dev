/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller.panzerwagen;

import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a complex model for an armored weapon's hull.
 * 
 * @extends AbstractPanzerwanneModel 
 * @implements PanzerwanneComplexModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 6.7.2018
 */
public abstract class AbstractPanzerwanneComplexModel extends AbstractPanzerwanneModel 
    implements PanzerwanneComplexModel {
    private Spatial seitenpunktLinkeSpatial_ = null;
    private boolean hasSeitenpunktLinke_ = false;
    private Spatial seitenpunktRechteSpatial_ = null;
    private boolean hasSeitenpunktRechte_ = false;
    private Spatial hinterpunktSpatial_ = null;
    private boolean hasHinterpunkt_ = false;
    private float leftUpperGlacisArmoring_ = 0f;
    private float leftLowerGlacisArmoring_ = 0f;
    private float rightUpperGlacisArmoring_ = 0f;
    private float rightLowerGlacisArmoring_ = 0f;
    private float rearUpperGlacisArmoring_ = 0f;
    private float rearLowerGlacisArmoring_ = 0f;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param kombattant The combat participator the model is created for.
     * @param panzerwanneSpatial The model's spatial.
     * @param panzerwanneScale The model's scale.
     * @param panzerwanneShape The model's collision shape.
     */
    public AbstractPanzerwanneComplexModel(final Kombattant kombattant, 
        final Spatial panzerwanneSpatial, final DgaJmeScale panzerwanneScale, 
        final CollisionShape panzerwanneShape) {
        super(kombattant, panzerwanneSpatial, panzerwanneScale, panzerwanneShape);
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the left side orientation point.
     * 
     * @return Spatial The left side orientation point.
     */
    @Override
    public Spatial getSeitenpunktLinke() {
        return seitenpunktLinkeSpatial_;
    }

    /**
     * The method sets the left side orientation point.
     * 
     * @param seitenpunktLinke The left side orientation point.
     */
    @Override
    public void setSeitenpunktLinke(final Spatial seitenpunktLinke) {
        seitenpunktLinkeSpatial_ = seitenpunktLinke;
        hasSeitenpunktLinke_ = true;
    }

    /**
     * The method returns the flag whether the model has a left side orientation point or not.
     * 
     * @return boolean The flag whether the model has a left side orientation point or not.
     */
    @Override
    public boolean hasSeitenpunktLinke() {
        return hasSeitenpunktLinke_;
    }

    /**
     * The method returns the right side orientation point.
     * 
     * @return Spatial The right side orientation point.
     */
    @Override
    public Spatial getSeitenpunktRechte() {
        return seitenpunktRechteSpatial_;
    }

    /**
     * The method sets the right side orientation point.
     * 
     * @param seitenpunktRechte The right side orientation point.
     */
    @Override
    public void setSeitenpunktRechte(final Spatial seitenpunktRechte) {
        seitenpunktRechteSpatial_ = seitenpunktRechte;
        hasSeitenpunktRechte_ = true;
    }

    /**
     * The method returns the flag whether the model has a right side orientation point or not.
     * 
     * @return boolean The flag whether the model has a right side orientation point or not.
     */
    @Override
    public boolean hasSeitenpunktRechte() {
        return hasSeitenpunktRechte_;
    }

    /**
     * The method returns the rear orientation point.
     * 
     * @return Spatial The rear orientation point.
     */
    @Override
    public Spatial getHinterpunkt() {
        return hinterpunktSpatial_;
    }

    /**
     * The method sets the rear orientation point.
     * 
     * @param hinterpunkt The rear orientation point.
     */
    @Override
    public void setHinterpunkt(final Spatial hinterpunkt) {
        hinterpunktSpatial_ = hinterpunkt;
        hasHinterpunkt_ = true;
    }

    /**
     * The method returns the flag whether the model has a rear orientation point or not.
     * 
     * @return boolean The flag whether the model has a rear orientation point or not.
     */
    @Override
    public boolean hasHinterpunkt() {
        return hasHinterpunkt_;
    }
    
    /**
     * The method returns the left upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The left upper glacis armoring value of the armored weapon's hull.
     */
    @Override
    public float getLeftUpperGlacisArmoring() {
        return leftUpperGlacisArmoring_;
    }

    /**
     * The method sets the left upper glacis armoring value of the armored weapon's hull.
     * 
     * @param leftUpperGlacisArmoring The left upper glacis armoring value of 
     * the armored weapon's hull.
     */
    @Override
    public void setLeftUpperGlacisArmoring(final float leftUpperGlacisArmoring) {
        leftUpperGlacisArmoring_ = leftUpperGlacisArmoring;
    }
    
    /**
     * The method returns the left lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The left lower glacis armoring value of the armored weapon's hull.
     */
    @Override
    public float getLeftLowerGlacisArmoring() {
        return leftLowerGlacisArmoring_;
    }

    /**
     * The method sets the left lower glacis armoring value of the armored weapon's hull.
     * 
     * @param leftLowerGlacisArmoring The left lower glacis armoring value of 
     * the armored weapon's hull.
     */
    @Override
    public void setLeftGlacisArmoring(final float leftLowerGlacisArmoring) {
        leftLowerGlacisArmoring_ = leftLowerGlacisArmoring;
    }
    
    /**
     * The method returns the right upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The right upper glacis armoring value of the armored weapon's hull.
     */
    @Override
    public float getRightUpperGlacisArmoring() {
        return rightUpperGlacisArmoring_;
    }

    /**
     * The method sets the right upper glacis armoring value of the armored weapon's hull.
     * 
     * @param rightUpperGlacisArmoring The right upper glacis armoring value of 
     * the armored weapon's hull.
     */
    @Override
    public void setRightUpperGlacisArmoring(final float rightUpperGlacisArmoring) {
        rightUpperGlacisArmoring_ = rightUpperGlacisArmoring;
    }

    /**
     * The method returns the right lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The right lower glacis armoring value of the armored weapon's hull.
     */
    @Override
    public float getRightLowerGlacisArmoring() {
        return rightLowerGlacisArmoring_;
    }

    /**
     * The method sets the right lower glacis armoring value of the armored weapon's hull.
     * 
     * @param rightLowerGlacisArmoring The right lower glacis armoring value of 
     * the armored weapon's hull.
     */
    @Override
    public void setRightLowerGlacisArmoring(final float rightLowerGlacisArmoring) {
        rightLowerGlacisArmoring_ = rightLowerGlacisArmoring;
    }
    
    /**
     * The method returns the rear upper glacis armoring value of the armored weapon's hull.
     * 
     * @return float The rear upper glacis armoring value of the armored weapon's hull.
     */
    @Override
    public float getRearUpperGlacisArmoring() {
        return rearUpperGlacisArmoring_;
    }

    /**
     * The method sets the rear upper glacis armoring value of the armored weapon's hull.
     * 
     * @param rearUpperGlacisArmoring The rear upper glacis armoring value of 
     * the armored weapon's hull.
     */
    @Override
    public void setRearUpperGlacisArmoring(final float rearUpperGlacisArmoring) {
        rearUpperGlacisArmoring_ = rearUpperGlacisArmoring;
    }

    /**
     * The method returns the rear lower glacis armoring value of the armored weapon's hull.
     * 
     * @return float The rear lower glacis armoring value of the armored weapon's hull.
     */
    @Override
    public float getRearLowerGlacisArmoring() {
        return rearLowerGlacisArmoring_;
    }

    /**
     * The method sets the rear lower glacis armoring value of the armored weapon's hull.
     * 
     * @param rearLowerGlacisArmoring The rear lower glacis armoring value of 
     * the armored weapon's hull.
     */
    @Override
    public void setRearLowerGlacisArmoring(final float rearLowerGlacisArmoring) {
        rearLowerGlacisArmoring_ = rearLowerGlacisArmoring;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
