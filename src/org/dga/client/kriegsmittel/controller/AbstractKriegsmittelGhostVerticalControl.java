/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel.controller;

import org.dga.client.kriegsmittel.panzerung.PanzerungModel;
import com.jme3.scene.Node;

/**
 * The abstract implementation of a control for a vertically rotatable weapon.
 *
 * @extends com.jme3.bullet.control.GhostControl
 * @implements KriegsmittelGhostControl
 * @implements KriegsmittelVerticallyRotatableControl
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 17.7.2018
 */
public abstract class AbstractKriegsmittelGhostVerticalControl 
    extends AbstractKriegsmittelGhostControl 
    implements KriegsmittelGhostControl, KriegsmittelVerticallyRotatableControl {
    private Node nodePivotPoint_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new control with the supplied properties.
     * 
     * @param kriegsmittelModel The weapon's model.
     * @param kriegsmittelPivotPoint The weapon's pivot point.
     */
    public AbstractKriegsmittelGhostVerticalControl(
        final PanzerungModel kriegsmittelModel, final Node kriegsmittelPivotPoint) {
        super(kriegsmittelModel);
        nodePivotPoint_ = kriegsmittelPivotPoint;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the pivot point about which the weapon can be rotated.
     * 
     * @return Node The pivot point about which the weapon can be rotated.
     */
    @Override
    public Node getPivotPoint() {
        return nodePivotPoint_;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
