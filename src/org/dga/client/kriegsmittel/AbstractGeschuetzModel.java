/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsmittel;

import org.dga.client.kombattant.Kombattant;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import org.dga.client.DgaJmeScale;

/**
 * The abstract implementation of a model for a shooting gun.
 * 
 * @extends AbstractSchusswaffeModel
 * @implements GeschuetzModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 25.07.2018
 */
public abstract class AbstractGeschuetzModel extends AbstractSchusswaffeModel 
    implements GeschuetzModel {
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new model with the supplied properties.
     *
     * @param kombattant The combat participator the shooting gun is created for.
     * @param geschuetzSpatial The shooting gun's spatial.
     * @param geschuetzScale The shooting gun's scale.
     * @param geschuetzShape The shooting gun's collision shape.
     * @param geschuetzPivotPoint The shooting gun's pivot point.
     * @param geschuetzShootingPoint The shooting gun's shooting point.
     */
    public AbstractGeschuetzModel(final Kombattant kombattant, 
        final Spatial geschuetzSpatial, final DgaJmeScale geschuetzScale, 
        final CollisionShape geschuetzShape, final Node geschuetzPivotPoint, 
        final Node geschuetzShootingPoint) {
        super(kombattant, geschuetzSpatial, geschuetzScale, geschuetzShape, 
            geschuetzPivotPoint, geschuetzShootingPoint);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************************************************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}