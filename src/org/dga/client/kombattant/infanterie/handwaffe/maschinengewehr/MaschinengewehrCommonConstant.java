/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr;

import org.dga.client.DgaJmeCommonConstant;

/**
 * The common constants for machine guns.
 *
 * @extends DgaJmeCommonConstant
 * @pattern Typesafe Enum
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 14.5.2018
 */
public class MaschinengewehrCommonConstant extends DgaJmeCommonConstant {
    /**
     * Maximum elevation angle for a machine gun.
     */
    public static final MaschinengewehrCommonConstant MASCHINENGEWEHR_MAX_ELEVATION_ANGLE = 
        new MaschinengewehrCommonConstant(5.0f);
    /**
     * Maximum depression angle for a machine gun.
     */
    public static final MaschinengewehrCommonConstant MASCHINENGEWEHR_MAX_DEPRESSION_ANGLE = 
        new MaschinengewehrCommonConstant(-5.0f);
    /**
     * Fire sector's horizontal angle for a machine gun.
     */
    public static final MaschinengewehrCommonConstant MASCHINENGEWEHR_FIRE_SECTOR_ANGLE = 
        new MaschinengewehrCommonConstant(10.0f);
    //
    // *************************** Constructors ********************************
    //
    /**
     * The float constructor.
     *
     * @param value
     */
    protected MaschinengewehrCommonConstant(final float value) {
        super(value);
    }
    //
    // *************************** Public Methods ******************************
    //
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
