/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant.kampffahrzeug.panzerwagen;

import com.jme3.scene.Geometry;
import org.dga.client.AbstractDgaJmeTargetCursorModel;
import com.jme3.scene.Node;
import org.dga.client.DgaJmeScale;

/**
 * The default target cursor model of armoured fighting carriages.
 *
 * @extends AbstractDgaJmeTargetCursorModel
 * @implements PanzerwagenTargetCursorModel
 * @pattern Adapter
 * @pattern Template Method
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 17.7.2018
 */
public final class DefaultPanzerwagenTargetCursorModel extends AbstractDgaJmeTargetCursorModel 
    implements PanzerwagenTargetCursorModel {
    private Node pivotPoint_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates a new target cursor with the supplied properties.
     *
     * @param targetGeometry The target cursor's geometry.
     * @param scale The target cursor's scale.
     */
    public DefaultPanzerwagenTargetCursorModel(final Geometry targetGeometry, 
        final DgaJmeScale scale) {
        super(targetGeometry, scale);
    }
    
    /**
     * The constructor creates a new target cursor with the supplied properties.
     *
     * @param targetGeometry The target cursor's geometry.
     * @param scale The target cursor's scale.
     * @param targetOffset The target cursor's offset.
     * @param pivotPoint The target cursor's pivot point.
     */
    public DefaultPanzerwagenTargetCursorModel(final Geometry targetGeometry, 
        final DgaJmeScale scale, final float targetOffset, final Node pivotPoint) {
        super(targetGeometry, scale, targetOffset);
        pivotPoint_ = pivotPoint;
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the target's pivot point.
     * 
     * @return Node The target's pivot point.
     */
    @Override
    public Node getPivotPoint() {
        return pivotPoint_;
    }
    
    /**
     * The method sets the game object's pivot point.
     * 
     * @param pivotPoint The game object's pivot point.
     */
    @Override
    public void setPivotPoint(final Node pivotPoint) {
        pivotPoint_ = pivotPoint;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
}
