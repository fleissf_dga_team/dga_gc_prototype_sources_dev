/*
 * DGA Game Client.
 *
 * Copyright (c) 2018 The FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kombattant;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import org.dga.client.DgaJmeScalable;
import org.dga.client.DgaJmeApplicationable;
import org.dga.client.DgaJmeApplicationRenderable;

/**
 * The interface of a model for a combat participator.
 *
 * @extends DgaJmeScalable
 * @extends DgaJmeApplicationable
 * @pattern Adapter
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @version 0.1
 * @date 19.07.2018
 */
public interface KombattantModel extends DgaJmeScalable, DgaJmeApplicationable, 
    DgaJmeApplicationRenderable {
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the combat participator's initial position.
     * 
     * @return Vector3f The combat participator's initial position.
     */
    public Vector3f getInitialPosition();

    /**
     * The method sets the combat participator's initial position.
     * 
     * @param initialPosition The combat participator's initial position.
     */
    public void setInitialPosition(final Vector3f initialPosition);

    /**
     * The method returns the combat participator's initial rotation.
     * 
     * @return Quaternion The combat participator's initial rotation.
     */
    public Quaternion getInitialRotation();

    /**
     * The method sets the combat participator's initial rotation.
     * 
     * @param initialRotation The combat participator's initial rotation.
     */
    public void setInitialRotation(final Quaternion initialRotation);
    
    /**
     * The method returns the combat participator's instance identifier.
     * 
     * @return String The combat participator's instance identifier.
     */
    public String getId();

    /**
     * The method sets the combat participator's instance identifier.
     * 
     * @param kombattantId The combat participator's instance identifier.
     */
    public void setId(final String kombattantId);
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************************************************************
}