/*
 * DGA Game Client.
 *
 * Copyright (c) 2018, FLEISS Software Foundation. All Rights Reserved.
 *
 * This program is part of the FLEISS Software Foundation's projects and is distributed
 * under the terms of the GNU General Public License version 3 or any later version.
 * You can use, redistribute and/or modify it under the terms of the GNU General
 * Public License version 3 or any later version. Please, see
 * <http://www.fleissf.org/licensing/> for details.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.dga.client.kriegsteilnehmer.kampfpanzer.deu.PzKpfwVPanther.AusfD_FP;

import static org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenControlBinding.*;

import org.dga.client.DgaJmeUtils;
import org.dga.client.DefaultDgaJmeScale;
import org.dga.client.kombattant.infanterie.handwaffe.Schiesspatrone;
import org.dga.client.kriegsteilnehmer.maschinengewehr.deu.MG34.KwMG34;
import org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr.DefaultMaschinengewehrModel;
import org.dga.client.kombattant.infanterie.handwaffe.maschinengewehr.MaschinengewehrModel;
import org.dga.client.kombattant.kampffahrzeug.artillerie.Kanonenpatrone;
import org.dga.client.kriegsteilnehmer.kampfwagenkanone.deu.KwK42.KwK42;
import org.dga.client.kombattant.kampffahrzeug.artillerie.kampfwagenkanone.DefaultPanzerkanoneModel;
import org.dga.client.kombattant.kampffahrzeug.artillerie.kampfwagenkanone.PanzerkanoneModel;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.DefaultPanzerwagenTargetCursor;
import org.dga.client.kriegsteilnehmer.kampfpanzer.deu.PzKpfwVPanther.AbstractPzKpfwVPanther;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer.DefaultKampfpanzerControl;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer.Kampfpanzer;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.kampfpanzer.KampfpanzerModel;
import org.dga.client.kriegsmittel.controller.panzerwagen.AbdeckungControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.AntriebsradControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultAbdeckungControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultAntriebsradControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultGleisketteControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultKanonenmaskeControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultKasteControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultKlappeControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultKommandantenkuppelControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultLaufradControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultLeitradControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultLukeControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultPanzerwanneControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultPanzerwanneModel;
import org.dga.client.kriegsmittel.controller.panzerwagen.DefaultTurmControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.GleisketteControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.KanonenmaskeControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.KasteControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.KlappeControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.KommandantenkuppelControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.LaufradControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.LeitradControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.LukeControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.PanzerwanneControl;
import org.dga.client.kriegsmittel.controller.panzerwagen.PanzerwanneModel;
import org.dga.client.kriegsmittel.controller.panzerwagen.TurmControl;
import org.dga.client.kriegsmittel.Feuerwaffe;
import org.dga.client.kriegsmittel.panzerung.DefaultPanzerungModel;
import org.dga.client.kriegsmittel.panzerung.DefaultPanzerungPhysicsModel;
import org.dga.client.kriegsmittel.panzerung.PanzerungModel;
import org.dga.client.kriegsmittel.panzerung.PanzerungPhysicsModel;
import org.dga.client.kraftwerk.DefaultKraft;
import org.dga.client.kraftwerk.Kraft;
import org.dga.client.kraftwerk.Schaden;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.CompoundCollisionShape;
import com.jme3.bullet.collision.shapes.HullCollisionShape;
import com.jme3.bullet.objects.PhysicsGhostObject;
import com.jme3.effect.ParticleEmitter;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Quad;
import com.jme3.system.AppSettings;
import com.jme3.texture.Texture;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.dga.client.effekt.EngineEffect;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.DefaultPanzerwagenTargetCursorModel;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenTargetCursor;
import org.dga.client.kombattant.kampffahrzeug.panzerwagen.PanzerwagenTargetCursorModel;
import org.dga.client.DgaJmeScale;

/**
 * The german battle tank Panzerkampfwagen Pz.Kpfw. V "Panther" (Sd.Kfz. 171) 
 * Ausfuehrung D Fruehe Produktion (Earlier Version).
 *
 * @extends AbstractPzKpfwVPanther
 * @implements Kampfpanzer
 * @implements com.jme3.app.state.AppState
 * @implements com.jme3.input.controls.ActionListener
 * @implements com.jme3.input.controls.AnalogListener
 * @implements com.jme3.bullet.collision.PhysicsCollisionListener
 * @pattern Adapter
 * @pattern Template Method
 * @pattern Inverstion of Control (IoC)
 * @author <a href="mailto:zss@fleissf.org">Zhanat S. Skokbayev</a>
 * @author <a href="http://olavz.com">Olav J. Hermansen</a>
 * @version 0.1
 * @date 4.5.2018
 */
public final class PzKpfwVPantherAusfD_FP extends AbstractPzKpfwVPanther implements Kampfpanzer {
    // The weapon's name.
    private static final String PANZER_MODEL_NAME_ = "Panzerkampfwagen Pz.Kpfw. V Panther Ausf. D (Sd.Kfz. 171)";
    // The battle tank's armament list.
    private static final List<Feuerwaffe> WAFFE_LIST_ = Collections.synchronizedList(new LinkedList<>());
    //
    private static final float WANNE_FRONT_UPPER_GLACIS_ARMORING_VALUE_ = 80.0f;
    private static final float WANNE_FRONT_LOWER_GLACIS_ARMORING_VALUE_ = 60.0f;
    private static final float WANNE_REAR_ARMORING_VALUE_ = 40.0f;
    private static final float WANNE_SIDE_ARMORING_VALUE_ = 40.0f;
    private static final float WANNE_TOP_ARMORING_VALUE_ = 17.0f;
    private static final float WANNE_BOTTOM_ARMORING_VALUE_ = 17.0f;
    private static final float TURM_FRONT_ARMORING_VALUE_ = 80.0f;
    private static final float TURM_REAR_ARMORING_VALUE_ = 45.0f;
    private static final float TURM_SIDE_ARMORING_VALUE_ = 45.0f;
    private static final float TURM_TOP_ARMORING_VALUE_ = 17.0f;
    private static final float TURMKANONENMASKE_FRONT_ARMORING_VALUE_ = 100.0f;
    private static final float TURMKANONENMASKE_SIDE_ARMORING_VALUE_ = 80.0f;
    private static final float TURMKANONENMASKE_TOP_ARMORING_VALUE_ = 80.0f;
    private static final float TURMKANONENMASKE_BOTTOM_ARMORING_VALUE_ = 80.0f;
    private static final float TURMKANONE_ARMORING_VALUE_ = 100.0f;
    private static final float GLEISKETTE_ARMORING_VALUE_ = 50.0f;
    private static final float ANTRIEBSRAD_FRTB_ARMORING_VALUE_ = 850.0f;
    private static final float ANTRIEBSRAD_SIDE_ARMORING_VALUE_ = 345.0f;
    private static final float LAUFRAD_FRTB_ARMORING_VALUE_ = 850.0f;
    private static final float LAUFRAD_SIDE_ARMORING_VALUE_ = 350.0f;
    private static final float LEITRAD_FRTB_ARMORING_VALUE_ = 600.0f;
    private static final float LEITRAD_SIDE_ARMORING_VALUE_ = 340.0f;
    private static final float FAHRERSEHKLAPPE_ARMORING_VALUE_ = 80.0f;
    private static final float MGKLAPPE_ARMORING_VALUE_ = 80.0f;
    private static final float KETTENABDECKUNG_ARMORING_VALUE_ = 5.0f;
    private static final float GEPAECKKASTEN_ARMORING_VALUE_ = 10.0f;
    private static final float KOMMANDANTENKUPPEL_SIDE_ARMORING_VALUE_ = 120.0f;
    private static final float KOMMANDANTENKUPPEL_TOP_ARMORING_VALUE_ = 17.0f;
    private static final float NOTAUSSTIEGLUKE_ARMORING_VALUE_ = 45.0f;
    private static final float TURMLUKE_ARMORING_VALUE_ = 17.0f;
    //
    private static final float ACCELERATION_FORCE_ = 40.0f;
    private static final float ROTATION_FORCE_ = 600.0f;
    private static final float BRAKING_FORCE_ = 40.0f;
    private static final float STEERING_FORCE_ = 1.5f;
    private static final float MAX_FORWARD_SPEED_ = 0.0f;
    private static final float MAX_BACKWARD_SPEED_ = 0.0f;
    private static final Vector3f GAS_FORCE_ = new Vector3f(0f, 1000.0f, 0f);
    // The Vehicle goes back
    //private static final float STIFFNESS_ = 120.0f; //200=f1 car
    //private static final float COMP_VALUE_ = 0.2f; //(lower than damp!)
    //private static final float DAMP_VALUE_ = 0.3f;
    // The vehicle stands
    private static final float STIFFNESS_ = 20.0f; //200=f1 car
    private static final float COMP_VALUE_ = 0.2f; //(lower than damp!)
    private static final float DAMP_VALUE_ = 0.3f;
    //
    private static final float PANZER_SPEED_ = 33.0f;
    private static final float PANZER_MASS_ = 40000.0f;
    private static final float TARGET_OFFSET_ = 4.0f;
    private static final float MAX_SUSPENSION_FORCE_ = 100.0f;
    private static final float FRONT_WHEEL_SUSPENSION_REST_LENGTH_ = 0.3f;
    private static final float MIDDLE_WHEEL_SUSPENSION_REST_LENGTH_ = 0.3f;
    private static final float REAR_WHEEL_SUSPENSION_REST_LENGTH_ = 0.3f;
    private static final int TURM_MG_FIRE_BURST_LENGTH_ = 10;
    //
    private static final float TURM_ROTATION_SPEED_ = FastMath.HALF_PI;
    private static final float TURMKANONE_ROTATION_SPEED_ = FastMath.HALF_PI;
    private static final float TURM_MG_ROTATION_SPEED_ = FastMath.HALF_PI;
    private static final int TURMKANONE_MAX_ELEVATION_ANGLE_ = 20;
    private static final int TURMKANONE_MAX_DEPRESSION_ANGLE_ = 8;
    private static final int TURM_MG_MAX_ELEVATION_ANGLE_ = 15;
    private static final int TURM_MG_MAX_DEPRESSION_ANGLE_ = 10;
    private static final int TURM_MG_FIRE_SECTOR_ANGLE_ = 5;
    //
    private DgaJmeScale panzerScale_ = DefaultDgaJmeScale.DEFAULT_MODEL_SCALE;
    private DgaJmeScale granateScale_ = DefaultDgaJmeScale.DEFAULT_GRANATE_SCALE;
    private DgaJmeScale geschossScale_ = DefaultDgaJmeScale.DEFAULT_GESCHOSS_SCALE;
    private DgaJmeScale targetScale_ = DefaultDgaJmeScale.DEFAULT_TARGET_CURSOR_SCALE;
    //
    private final Vector3f MOVE_DIRECTION_ = new Vector3f(0, 0, 0);
    private final Vector3f VIEW_DIRECTION_ = new Vector3f(0, 1, 0);
    //
    private AssetManager assetManager_ = null;
    private PhysicsSpace physicsSpace_ = null;
    private Node rootNode_ = null;
    private AppStateManager stateManager_ = null;
    private RenderManager renderManager_ = null;
    private AppSettings appSettings_ = null;
    private Node appGuiNode_ = null;
    //
    private final Kraft PANZER_KRAFT_ = new DefaultKraft(4000.0f);
    //
    private boolean isPanzerRotateLeftward_ = false;
    private boolean isPanzerRotateRightward_ = false;
    private boolean isPanzerMoveForward_ = false;
    private boolean isPanzerMoveBackward_ = false;
    private boolean isPanzerBreak_ = false;
    private boolean isStopForward_ = false;
    private boolean isStopBackward_ = false;
    private boolean isStopLeftward_ = false;
    private boolean isStopRightward_ = false;
    private boolean isStopLeftForward_ = false;
    private boolean isStopRightForward_ = false;
    private boolean isStopLeftBackward_ = false;
    private boolean isStopRightBackward_ = false;
    private boolean isStopUpward_ = false;
    private boolean isStopDownward_ = false;
    //
    private CompoundCollisionShape compoundPanzer_ = null;
    private DefaultKampfpanzerControl controlPanzer_ = null;
    private Node nodePanzer_ = null;
    private Node nodeTurmPivot_ = null;
    private Node nodeTurmkanonePivot_ = null;
    private PanzerungPhysicsModel modelPanzer_ = null;
    //
    private PanzerwanneControl controlWanne_ = null;
    private CompoundCollisionShape compoundWanne_ = null;
    private Node nodeWanne_ = null;
    private Spatial sptlWanne_ = null;
    private Geometry geomWanne_ = null;
    private Vector3f centerWanne_ = null;
    private BoundingBox bbWanne_ = null;
    private Geometry geomcshWanne_ = null;
    private Vector3f centercshWanne_ = null;
    private BoundingBox bbcshWanne_ = null;
    private HullCollisionShape cshWanne_ = null;
    private PanzerwanneModel modelWanne_ = null;
    //
    private KlappeControl controlFahrersehklappe_ = null;
    private CompoundCollisionShape compoundFahrersehklappe_ = null;
    private Node nodeFahrersehklappe_ = null;
    private Spatial sptlFahrersehklappe_ = null;
    private Geometry geomFahrersehklappe_ = null;
    private BoundingBox bbFahrersehklappe_ = null;
    private Vector3f centerFahrersehklappe_ = null;
    private Geometry geomcshFahrersehklappe_ = null;
    private Vector3f centercshFahrersehklappe_ = null;
    private BoundingBox bbcshFahrersehklappe_ = null;
    private HullCollisionShape cshFahrersehklappe_ = null;
    private PanzerungModel modelFahrersehklappe_ = null;
    //
    private AbdeckungControl controlKettenabdeckungLinke_ = null;
    private CompoundCollisionShape compoundKettenabdeckungLinke_ = null;
    private Node nodeKettenabdeckungLinke_ = null;
    private Spatial sptlKettenabdeckungLinke_ = null;
    private Geometry geomKettenabdeckungLinke_ = null;
    private BoundingBox bbKettenabdeckungLinke_ = null;
    private Vector3f centerKettenabdeckungLinke_ = null;
    private Geometry geomcshKettenabdeckungLinke_ = null;
    private BoundingBox bbcshKettenabdeckungLinke_ = null;
    private Vector3f centercshKettenabdeckungLinke_ = null;
    private HullCollisionShape cshKettenabdeckungLinke_ = null;
    private PanzerungModel modelKettenabdeckungLinke_ = null;
    //
    private AbdeckungControl controlKettenabdeckungRechte_ = null;
    private CompoundCollisionShape compoundKettenabdeckungRechte_ = null;
    private Node nodeKettenabdeckungRechte_ = null;
    private Spatial sptlKettenabdeckungRechte_ = null;
    private Geometry geomKettenabdeckungRechte_ = null;
    private BoundingBox bbKettenabdeckungRechte_ = null;
    private Vector3f centerKettenabdeckungRechte_ = null;
    private Geometry geomcshKettenabdeckungRechte_ = null;
    private BoundingBox bbcshKettenabdeckungRechte_ = null;
    private Vector3f centercshKettenabdeckungRechte_ = null;
    private HullCollisionShape cshKettenabdeckungRechte_ = null;
    private PanzerungModel modelKettenabdeckungRechte_ = null;
    //
    private KlappeControl controlMGklappe_ = null;
    private CompoundCollisionShape compoundMGklappe_ = null;
    private Node nodeMGklappe_ = null;
    private Spatial sptlMGklappe_ = null;
    private Geometry geomMGklappe_ = null;
    private BoundingBox bbMGklappe_ = null;
    private Vector3f centerMGklappe_ = null;
    private Geometry geomcshMGklappe_ = null;
    private BoundingBox bbcshMGklappe_ = null;
    private Vector3f centercshMGklappe_ = null;
    private HullCollisionShape cshMGklappe_ = null;
    private PanzerungModel modelMGklappe_ = null;
    //
    private KasteControl controlGepaeckkastenLinke_ = null;
    private CompoundCollisionShape compoundGepaeckkastenLinke_ = null;
    private Node nodeGepaeckkastenLinke_ = null;
    private Spatial sptlGepaeckkastenLinke_ = null;
    private Geometry geomGepaeckkastenLinke_ = null;
    private BoundingBox bbGepaeckkastenLinke_ = null;
    private Vector3f centerGepaeckkastenLinke_ = null;
    private Geometry geomcshGepaeckkastenLinke_ = null;
    private BoundingBox bbcshGepaeckkastenLinke_ = null;
    private Vector3f centercshGepaeckkastenLinke_ = null;
    private HullCollisionShape cshGepaeckkastenLinke_ = null;
    private PanzerungModel modelGepaeckkastenLinke_ = null;
    //
    private KasteControl controlGepaeckkastenRechte_ = null;
    private CompoundCollisionShape compoundGepaeckkastenRechte_ = null;
    private Node nodeGepaeckkastenRechte_ = null;
    private Spatial sptlGepaeckkastenRechte_ = null;
    private Geometry geomGepaeckkastenRechte_ = null;
    private BoundingBox bbGepaeckkastenRechte_ = null;
    private Vector3f centerGepaeckkastenRechte_ = null;
    private Geometry geomcshGepaeckkastenRechte_ = null;
    private BoundingBox bbcshGepaeckkastenRechte_ = null;
    private Vector3f centercshGepaeckkastenRechte_ = null;
    private HullCollisionShape cshGepaeckkastenRechte_ = null;
    private PanzerungModel modelGepaeckkastenRechte_ = null;
    //
    private TurmControl controlTurm_ = null;
    private CompoundCollisionShape compoundTurm_ = null;
    private Node nodeTurm_ = null;
    private Spatial sptlTurm_ = null;
    private Geometry geomTurm_ = null;
    private Vector3f centerTurm_ = null;
    private BoundingBox bbTurm_ = null;
    private Geometry geomcshTurm_ = null;
    private Vector3f centercshTurm_ = null;
    private HullCollisionShape cshTurm_ = null;
    private BoundingBox bbcshTurm_ = null;
    private PanzerungModel modelTurm_ = null;
    //
    private KommandantenkuppelControl controlKommandantenkuppel_ = null;
    private CompoundCollisionShape compoundKommandantenkuppel_ = null;
    private Node nodeKommandantenkuppel_ = null;
    private Spatial sptlKommandantenkuppel_ = null;
    private Geometry geomKommandantenkuppel_ = null;
    private BoundingBox bbKommandantenkuppel_ = null;
    private Vector3f centerKommandantenkuppel_ = null;
    private Geometry geomcshKommandantenkuppel_ = null;
    private BoundingBox bbcshKommandantenkuppel_ = null;
    private Vector3f centercshKommandantenkuppel_ = null;
    private HullCollisionShape cshKommandantenkuppel_ = null;
    private PanzerungModel modelKommandantenkuppel_ = null;
    //
    private LukeControl controlNotausstiegluke_ = null;
    private CompoundCollisionShape compoundNotausstiegluke_ = null;
    private Node nodeNotausstiegluke_ = null;
    private Spatial sptlNotausstiegluke_ = null;
    private Geometry geomNotausstiegluke_ = null;
    private BoundingBox bbNotausstiegluke_ = null;
    private Vector3f centerNotausstiegluke_ = null;
    private Geometry geomcshNotausstiegluke_ = null;
    private BoundingBox bbcshNotausstiegluke_ = null;
    private Vector3f centercshNotausstiegluke_ = null;
    private HullCollisionShape cshNotausstiegluke_ = null;
    private PanzerungModel modelNotausstiegluke_ = null;
    //
    private LukeControl controlTurmluke_ = null;
    private CompoundCollisionShape compoundTurmluke_ = null;
    private Node nodeTurmluke_ = null;
    private Spatial sptlTurmluke_ = null;
    private Geometry geomTurmluke_ = null;
    private BoundingBox bbTurmluke_ = null;
    private Vector3f centerTurmluke_ = null;
    private Geometry geomcshTurmluke_ = null;
    private BoundingBox bbcshTurmluke_ = null;
    private Vector3f centercshTurmluke_ = null;
    private HullCollisionShape cshTurmluke_ = null;
    private PanzerungModel modelTurmluke_ = null;
    //
    private KanonenmaskeControl controlTurmkanonenmaske_ = null;
    private CompoundCollisionShape compoundTurmkanonenmaske_ = null;
    private Node nodeTurmkanonenmaske_ = null;
    private Spatial sptlTurmkanonenmaske_ = null;
    private Geometry geomTurmkanonenmaske_ = null;
    private Vector3f centerTurmkanonenmaske_ = null;
    private BoundingBox bbTurmkanonenmaske_ = null;
    private Geometry geomcshTurmkanonenmaske_ = null;
    private Vector3f centercshTurmkanonenmaske_ = null;
    private HullCollisionShape cshTurmkanonenmaske_ = null;
    private BoundingBox bbcshTurmkanonenmaske_ = null;
    private PanzerungModel modelTurmkanonenmaske_ = null;
    //
    private Spatial sptlTurmkanonenmaskeModel_ = null;
    private Geometry geomTurmkanonenmaskeModel_ = null;
    private BoundingBox bbTurmkanonenmaskeModel_ = null;
    private Vector3f centerTurmkanonenmaskeModel_ = null;
    //
    private KwK42 controlTurmkanone_ = null;
    private Node nodeTurmkanone_ = null;
    private Spatial sptlTurmkanone_ = null;
    private Geometry geomTurmkanone_ = null;
    private Vector3f centerTurmkanone_ = null;
    private BoundingBox bbTurmkanone_ = null;
    private Geometry geomcshTurmkanone_ = null;
    private Vector3f centercshTurmkanone_ = null;
    private HullCollisionShape cshTurmkanone_ = null;
    private BoundingBox bbcshTurmkanone_ = null;
    private PanzerungModel modelTurmkanone_ = null;
    //
    private Spatial sptlTurmkanoneModel_ = null;
    private Geometry geomTurmkanoneModel_ = null;
    private BoundingBox bbTurmkanoneModel_ = null;
    private Vector3f centerTurmkanoneModel_ = null;
    //
    private Node nodeTurmkanonenFrPkt_ = null;
    private Spatial sptlTurmkanonenFrPkt_ = null;
    private Geometry geomTurmkanonenFrPkt_ = null;
    private BoundingBox bbTurmkanonenFrPkt_ = null;
    private Vector3f centerTurmkanonenFrPkt_ = null;
    private HullCollisionShape cshTurmkanonenFrPkt_ = null;
    private CompoundCollisionShape compoundTurmkanonenFrPkt_ = null;
    private PanzerkanoneModel modelTurmkanonenFrPkt_ = null;
    //
    private Spatial sptlTurmkanonenFrPktModel_ = null;
    private Geometry geomTurmkanonenFrPktModel_ = null;
    private BoundingBox bbTurmkanonenFrPktModel_ = null;
    private Vector3f centerTurmkanonenFrPktModel_ = null;
    //
    private Node nodeTurmkanonenEffPkt_ = null;
    private Spatial sptlTurmkanonenEffPkt_ = null;
    private Geometry geomTurmkanonenEffPkt_ = null;
    private BoundingBox bbTurmkanonenEffPkt_ = null;
    private Vector3f centerTurmkanonenEffPkt_ = null;
    private HullCollisionShape cshTurmkanonenEffPkt_ = null;
    private PanzerungModel modelTurmkanonenEffPkt_ = null;
    //
    private Spatial sptlTurmkanonenEffPktModel_ = null;
    private Geometry geomTurmkanonenEffPktModel_ = null;
    private BoundingBox bbTurmkanonenEffPktModel_ = null;
    private Vector3f centerTurmkanonenEffPktModel_ = null;
    //
    private KwMG34 controlTurmMG_ = null;
    private Node nodeTurmMG_ = null;
    private Spatial sptlTurmMG_ = null;
    private Geometry geomTurmMG_ = null;
    private Vector3f centerTurmMG_ = null;
    private BoundingBox bbTurmMG_ = null;
    private Geometry geomcshTurmMG_ = null;
    private Vector3f centercshTurmMG_ = null;
    private HullCollisionShape cshTurmMG_ = null;
    private BoundingBox bbcshTurmMG_ = null;
    private PanzerungModel modelTurmMG_ = null;
    //
    private Spatial sptlTurmMGModel_ = null;
    private Geometry geomTurmMGModel_ = null;
    private BoundingBox bbTurmMGModel_ = null;
    private Vector3f centerTurmMGModel_ = null;
    //
    private Node nodeTurmMGFrPkt_ = null;
    private Spatial sptlTurmMGFrPkt_ = null;
    private Geometry geomTurmMGFrPkt_ = null;
    private BoundingBox bbTurmMGFrPkt_ = null;
    private Vector3f centerTurmMGFrPkt_ = null;
    private HullCollisionShape cshTurmMGFrPkt_ = null;
    private CompoundCollisionShape compoundTurmMGFrPkt_ = null;
    private MaschinengewehrModel modelTurmMGFrPkt_ = null;
    //
    private Spatial sptlTurmMGFrPktModel_ = null;
    private Geometry geomTurmMGFrPktModel_ = null;
    private BoundingBox bbTurmMGFrPktModel_ = null;
    private Vector3f centerTurmMGFrPktModel_ = null;
    //
    private Node nodeTurmMGEffPkt_ = null;
    private Spatial sptlTurmMGEffPkt_ = null;
    private Geometry geomTurmMGEffPkt_ = null;
    private BoundingBox bbTurmMGEffPkt_ = null;
    private Vector3f centerTurmMGEffPkt_ = null;
    private HullCollisionShape cshTurmMGEffPkt_ = null;
    private PanzerungModel modelTurmMGEffPkt_ = null;
    //
    private Spatial sptlTurmMGEffPktModel_ = null;
    private Geometry geomTurmMGEffPktModel_ = null;
    private BoundingBox bbTurmMGEffPktModel_ = null;
    private Vector3f centerTurmMGEffPktModel_ = null;
    //
    private GleisketteControl controlGleisketteLinke_ = null;
    private CompoundCollisionShape compoundGleisketteLinke_ = null;
    private Node nodeGleisketteLinke_ = null;
    private Spatial sptlGleisketteLinke_ = null;
    private Geometry geomGleisketteLinke_ = null;
    private Vector3f centerGleisketteLinke_ = null;
    private BoundingBox bbGleisketteLinke_ = null;
    private Geometry geomcshGleisketteLinke_ = null;
    private Vector3f centercshGleisketteLinke_ = null;
    private HullCollisionShape cshGleisketteLinke_ = null;
    private BoundingBox bbcshGleisketteLinke_ = null;
    private PanzerungModel modelGleisketteLinke_ = null;
    //
    private GleisketteControl controlGleisketteRechte_ = null;
    private CompoundCollisionShape compoundGleisketteRechte_ = null;
    private Node nodeGleisketteRechte_ = null;
    private Spatial sptlGleisketteRechte_ = null;
    private Geometry geomGleisketteRechte_ = null;
    private Vector3f centerGleisketteRechte_ = null;
    private BoundingBox bbGleisketteRechte_ = null;
    private Geometry geomcshGleisketteRechte_ = null;
    private Vector3f centercshGleisketteRechte_ = null;
    private HullCollisionShape cshGleisketteRechte_ = null;
    private BoundingBox bbcshGleisketteRechte_ = null;
    private PanzerungModel modelGleisketteRechte_ = null;
    //
    private AntriebsradControl controlAntriebsradLinke_ = null;
    private CompoundCollisionShape compoundAntriebsradLinke_ = null;
    private Node nodeAntriebsradLinke_ = null;
    private Spatial sptlAntriebsradLinke_ = null;
    private Geometry geomAntriebsradLinke_ = null;
    private Vector3f centerAntriebsradLinke_ = null;
    private BoundingBox bbAntriebsradLinke_ = null;
    private HullCollisionShape cshAntriebsradLinke_ = null;
    private PanzerungModel modelAntriebsradLinke_ = null;
    //
    private LaufradControl controlLaufradLinke1_ = null;
    private CompoundCollisionShape compoundLaufradLinke1_ = null;
    private Node nodeLaufradLinke1_ = null;
    private Spatial sptlLaufradLinke1_ = null;
    private Geometry geomLaufradLinke1_ = null;
    private Vector3f centerLaufradLinke1_ = null;
    private BoundingBox bbLaufradLinke1_ = null;
    private HullCollisionShape cshLaufradLinke1_ = null;
    private PanzerungModel modelLaufradLinke1_ = null;
    //
    private LaufradControl controlLaufradLinke2_ = null;
    private CompoundCollisionShape compoundLaufradLinke2_ = null;
    private Node nodeLaufradLinke2_ = null;
    private Spatial sptlLaufradLinke2_ = null;
    private Geometry geomLaufradLinke2_ = null;
    private Vector3f centerLaufradLinke2_ = null;
    private BoundingBox bbLaufradLinke2_ = null;
    private HullCollisionShape cshLaufradLinke2_ = null;
    private PanzerungModel modelLaufradLinke2_ = null;
    //
    private LaufradControl controlLaufradLinke3_ = null;
    private CompoundCollisionShape compoundLaufradLinke3_ = null;
    private Node nodeLaufradLinke3_ = null;
    private Spatial sptlLaufradLinke3_ = null;
    private Geometry geomLaufradLinke3_ = null;
    private Vector3f centerLaufradLinke3_ = null;
    private BoundingBox bbLaufradLinke3_ = null;
    private HullCollisionShape cshLaufradLinke3_ = null;
    private PanzerungModel modelLaufradLinke3_ = null;
    //
    private LaufradControl controlLaufradLinke4_ = null;
    private CompoundCollisionShape compoundLaufradLinke4_ = null;
    private Node nodeLaufradLinke4_ = null;
    private Spatial sptlLaufradLinke4_ = null;
    private Geometry geomLaufradLinke4_ = null;
    private Vector3f centerLaufradLinke4_ = null;
    private BoundingBox bbLaufradLinke4_ = null;
    private HullCollisionShape cshLaufradLinke4_ = null;
    private PanzerungModel modelLaufradLinke4_ = null;
    //
    private LaufradControl controlLaufradLinke5_ = null;
    private CompoundCollisionShape compoundLaufradLinke5_ = null;
    private Node nodeLaufradLinke5_ = null;
    private Spatial sptlLaufradLinke5_ = null;
    private Geometry geomLaufradLinke5_ = null;
    private Vector3f centerLaufradLinke5_ = null;
    private BoundingBox bbLaufradLinke5_ = null;
    private HullCollisionShape cshLaufradLinke5_ = null;
    private PanzerungModel modelLaufradLinke5_ = null;
    //
    private LaufradControl controlLaufradLinke6_ = null;
    private CompoundCollisionShape compoundLaufradLinke6_ = null;
    private Node nodeLaufradLinke6_ = null;
    private Spatial sptlLaufradLinke6_ = null;
    private Geometry geomLaufradLinke6_ = null;
    private Vector3f centerLaufradLinke6_ = null;
    private BoundingBox bbLaufradLinke6_ = null;
    private HullCollisionShape cshLaufradLinke6_ = null;
    private PanzerungModel modelLaufradLinke6_ = null;
    //
    private LaufradControl controlLaufradLinke7_ = null;
    private CompoundCollisionShape compoundLaufradLinke7_ = null;
    private Node nodeLaufradLinke7_ = null;
    private Spatial sptlLaufradLinke7_ = null;
    private Geometry geomLaufradLinke7_ = null;
    private Vector3f centerLaufradLinke7_ = null;
    private BoundingBox bbLaufradLinke7_ = null;
    private HullCollisionShape cshLaufradLinke7_ = null;
    private PanzerungModel modelLaufradLinke7_ = null;
    //
    private LaufradControl controlLaufradLinke8_ = null;
    private CompoundCollisionShape compoundLaufradLinke8_ = null;
    private Node nodeLaufradLinke8_ = null;
    private Spatial sptlLaufradLinke8_ = null;
    private Geometry geomLaufradLinke8_ = null;
    private Vector3f centerLaufradLinke8_ = null;
    private BoundingBox bbLaufradLinke8_ = null;
    private HullCollisionShape cshLaufradLinke8_ = null;
    private PanzerungModel modelLaufradLinke8_ = null;
    //
    private LeitradControl controlLeitradLinke_ = null;
    private CompoundCollisionShape compoundLeitradLinke_ = null;
    private Node nodeLeitradLinke_ = null;
    private Spatial sptlLeitradLinke_ = null;
    private Geometry geomLeitradLinke_ = null;
    private Vector3f centerLeitradLinke_ = null;
    private BoundingBox bbLeitradLinke_ = null;
    private HullCollisionShape cshLeitradLinke_ = null;
    private PanzerungModel modelLeitradLinke_ = null;
    //
    private AntriebsradControl controlAntriebsradRechte_ = null;
    private CompoundCollisionShape compoundAntriebsradRechte_ = null;
    private Node nodeAntriebsradRechte_ = null;
    private Spatial sptlAntriebsradRechte_ = null;
    private Geometry geomAntriebsradRechte_ = null;
    private Vector3f centerAntriebsradRechte_ = null;
    private BoundingBox bbAntriebsradRechte_ = null;
    private HullCollisionShape cshAntriebsradRechte_ = null;
    private PanzerungModel modelAntriebsradRechte_ = null;
    //
    private LaufradControl controlLaufradRechte1_ = null;
    private CompoundCollisionShape compoundLaufradRechte1_ = null;
    private Node nodeLaufradRechte1_ = null;
    private Spatial sptlLaufradRechte1_ = null;
    private Geometry geomLaufradRechte1_ = null;
    private Vector3f centerLaufradRechte1_ = null;
    private BoundingBox bbLaufradRechte1_ = null;
    private HullCollisionShape cshLaufradRechte1_ = null;
    private PanzerungModel modelLaufradRechte1_ = null;
    //
    private LaufradControl controlLaufradRechte2_ = null;
    private CompoundCollisionShape compoundLaufradRechte2_ = null;
    private Node nodeLaufradRechte2_ = null;
    private Spatial sptlLaufradRechte2_ = null;
    private Geometry geomLaufradRechte2_ = null;
    private Vector3f centerLaufradRechte2_ = null;
    private BoundingBox bbLaufradRechte2_ = null;
    private HullCollisionShape cshLaufradRechte2_ = null;
    private PanzerungModel modelLaufradRechte2_ = null;
    //
    private LaufradControl controlLaufradRechte3_ = null;
    private CompoundCollisionShape compoundLaufradRechte3_ = null;
    private Node nodeLaufradRechte3_ = null;
    private Spatial sptlLaufradRechte3_ = null;
    private Geometry geomLaufradRechte3_ = null;
    private Vector3f centerLaufradRechte3_ = null;
    private BoundingBox bbLaufradRechte3_ = null;
    private HullCollisionShape cshLaufradRechte3_ = null;
    private PanzerungModel modelLaufradRechte3_ = null;
    //
    private LaufradControl controlLaufradRechte4_ = null;
    private CompoundCollisionShape compoundLaufradRechte4_ = null;
    private Node nodeLaufradRechte4_ = null;
    private Spatial sptlLaufradRechte4_ = null;
    private Geometry geomLaufradRechte4_ = null;
    private Vector3f centerLaufradRechte4_ = null;
    private BoundingBox bbLaufradRechte4_ = null;
    private HullCollisionShape cshLaufradRechte4_ = null;
    private PanzerungModel modelLaufradRechte4_ = null;
    //
    private LaufradControl controlLaufradRechte5_ = null;
    private CompoundCollisionShape compoundLaufradRechte5_ = null;
    private Node nodeLaufradRechte5_ = null;
    private Spatial sptlLaufradRechte5_ = null;
    private Geometry geomLaufradRechte5_ = null;
    private Vector3f centerLaufradRechte5_ = null;
    private BoundingBox bbLaufradRechte5_ = null;
    private HullCollisionShape cshLaufradRechte5_ = null;
    private PanzerungModel modelLaufradRechte5_ = null;
    //
    private LaufradControl controlLaufradRechte6_ = null;
    private CompoundCollisionShape compoundLaufradRechte6_ = null;
    private Node nodeLaufradRechte6_ = null;
    private Spatial sptlLaufradRechte6_ = null;
    private Geometry geomLaufradRechte6_ = null;
    private Vector3f centerLaufradRechte6_ = null;
    private BoundingBox bbLaufradRechte6_ = null;
    private HullCollisionShape cshLaufradRechte6_ = null;
    private PanzerungModel modelLaufradRechte6_ = null;
    //
    private LaufradControl controlLaufradRechte7_ = null;
    private CompoundCollisionShape compoundLaufradRechte7_ = null;
    private Node nodeLaufradRechte7_ = null;
    private Spatial sptlLaufradRechte7_ = null;
    private Geometry geomLaufradRechte7_ = null;
    private Vector3f centerLaufradRechte7_ = null;
    private BoundingBox bbLaufradRechte7_ = null;
    private HullCollisionShape cshLaufradRechte7_ = null;
    private PanzerungModel modelLaufradRechte7_ = null;
    //
    private LaufradControl controlLaufradRechte8_ = null;
    private CompoundCollisionShape compoundLaufradRechte8_ = null;
    private Node nodeLaufradRechte8_ = null;
    private Spatial sptlLaufradRechte8_ = null;
    private Geometry geomLaufradRechte8_ = null;
    private Vector3f centerLaufradRechte8_ = null;
    private BoundingBox bbLaufradRechte8_ = null;
    private HullCollisionShape cshLaufradRechte8_ = null;
    private PanzerungModel modelLaufradRechte8_ = null;
    //
    private LeitradControl controlLeitradRechte_ = null;
    private CompoundCollisionShape compoundLeitradRechte_ = null;
    private Node nodeLeitradRechte_ = null;
    private Spatial sptlLeitradRechte_ = null;
    private Geometry geomLeitradRechte_ = null;
    private Vector3f centerLeitradRechte_ = null;
    private BoundingBox bbLeitradRechte_ = null;
    private HullCollisionShape cshLeitradRechte_ = null;
    private PanzerungModel modelLeitradRechte_ = null;
    //
    private Node nodePanzerwannenvorpunkt_ = null;
    private Spatial sptlPanzerwannenvorpunkt_ = null;
    private Geometry geomPanzerwannenvorpunkt_ = null;
    private BoundingBox bbPanzerwannenvorpunkt_ = null;
    private Vector3f centerPanzerwannenvorpunkt_ = null;
    //
    private Node nodeTurmvorpunkt_ = null;
    private Spatial sptlTurmvorpunkt_ = null;
    private Geometry geomTurmvorpunkt_ = null;
    private BoundingBox bbTurmvorpunkt_ = null;
    private Vector3f centerTurmvorpunkt_ = null;
    //
    private PanzerwagenTargetCursor controlTC_ = null;
    private Quad quadTC_ = null;
    private Geometry geomTC_ = null;
    private Texture texTC_ = null;
    private Material matTC_ = null;
    private Vector3f centerTC_ = null;
    private BoundingBox bbTC_ = null;
    private PanzerwagenTargetCursorModel modelTC_ = null;
    //
    private Texture texTransparent_ = null;
    private Material matTransparent_ = null;
    //
    private Vector3f initPosition_ = new Vector3f(0f, 0f, 0f);
    private Quaternion initRotation_ = Quaternion.ZERO;
    private String panzerId_ = "Id";
    //
    private EngineEffect engineEffect_ = null;
    private Node nodeEngineEffect_ = null;
    private ParticleEmitter peExhaustLeft_ = null;
    private ParticleEmitter peExhaustRight_ = null;
    private ParticleEmitter peExhaustBlastLeft_ = null;
    private ParticleEmitter peExhaustBlastRight_ = null;
    private AudioNode anodePzStanding_ = null;
    private AudioNode anodePzRunning_ = null;
    //
    // *************************** Constructors ********************************
    //
    /**
     * The constructor creates new instance of the battle tank.
     * 
     * @param kampfpanzerModel The battle tank's model.
     */
    public PzKpfwVPantherAusfD_FP(final KampfpanzerModel kampfpanzerModel) {
        super(kampfpanzerModel);
        assetManager_ = kampfpanzerModel.getAssetManager();
        physicsSpace_ = kampfpanzerModel.getPhysicsSpace();
        rootNode_ = kampfpanzerModel.getRootNode();
        stateManager_ = kampfpanzerModel.getStateManager();
        renderManager_ = kampfpanzerModel.getRenderManager();
        appSettings_ = kampfpanzerModel.getAppSettings();
        appGuiNode_ = kampfpanzerModel.getGuiNode();
        panzerScale_ = kampfpanzerModel.getScale();
        initPosition_ = kampfpanzerModel.getInitialPosition();
        initRotation_ = kampfpanzerModel.getInitialRotation();
        panzerId_ = kampfpanzerModel.getId();
        modelTC_ = kampfpanzerModel.getTargetCursorModel();
    }
    //
    // *************************** Public Methods ******************************
    //
    /**
     * The method returns the name of the weapon.
     * 
     * @return String The name of the weapon.
     */
    @Override
    public String getName() {
        return PANZER_MODEL_NAME_;
    }
    
    /**
     * The method returns the armored weapon's fighting capacity.
     * 
     * @return Panzerungkraft The armored weapon's fighting capacity.
     */
    @Override
    public Kraft getPower() {
        return new DefaultKraft((DefaultKraft) PANZER_KRAFT_);
    }
    
    /**
     * The method sets damage applying to the armored weapon and diminishing its 
     * fighting capacity.
     * 
     * @param damage The damage applying to the armored weapon and diminishing 
     * its fighting capacity.
     */
    @Override
    public void setDamage(final Schaden damage) {
        PANZER_KRAFT_.diminish(damage.getFloatValue());
        
        // TEMP CODE
        System.out.println(PANZER_MODEL_NAME_ + ": Panzerkraft: " + PANZER_KRAFT_.getCurrentFloatValue());
        // TEMP CODE
        
    }
    
    /**
     * The method returns the primary cannon's ammunition list. Normally the 
     * primary cannon is the turret cannon. The method can return an empty list 
     * if the battle tank has no cannons.
     * 
     * @return List<Kanonenpatrone> The primary cannon's ammunition list.
     */
    @Override
    public List<Kanonenpatrone> getPrimaryKanoneMunitionList() {
        return controlTurmkanone_.getMunitionList();
    }
   
    /**
     * The method loads the primary cannon with the specified type of rounds 
     * (cartridges). Normally the primary cannon is the turret cannon. The method 
     * can do nothing if the battle tank has no cannons.
     * 
     * @param patroneType The type of the primary cannon's rounds (cartridges).
     */
    @Override
    public void loadPrimaryKanone(final Kanonenpatrone patroneType) {
        controlTurmkanone_.load(patroneType);
    }
    
    /**
     * The method returns the secondary cannon's ammunition list. Normally the 
     * secondary cannon is the hull cannon. The method can return an empty list 
     * if the battle tank has only one primary cannon.
     * 
     * @return List<Kanonenpatrone> The secondary cannon's ammunition list.
     */
    @Override
    public List<Kanonenpatrone> getSecondaryKanoneMunitionList() {
        return controlTurmkanone_.getMunitionList();
    }
    
    /**
     * The method loads the secondary cannon with the specified type of rounds 
     * (cartridges). Normally the secondary cannon is the hull cannon. The method 
     * can do nothing if the battle tank has only one primary cannon.
     * 
     * @param patroneType The type of the seconary cannon's rounds (cartridges).
     */
    @Override
    public void loadSecondaryKanone(final Kanonenpatrone patroneType) {
        controlTurmkanone_.load(patroneType);
    }
    
    /**
     * The method returns the coaxial machine gun's ammunition list. Normally 
     * this machine gun is coaxial with the primary cannon located in the turret. 
     * The method can return an empty list if the battle tank has no coaxial 
     * machine gun.
     * 
     * @return List<Schiesspatrone> The coaxial machine gun's ammunition list.
     */
    @Override
    public List<Schiesspatrone> getCoaxialMaschinengewehrMunitionList() {
        return controlTurmMG_.getMunitionList();
    }
    
    /**
     * The method loads the coaxial machine gun with the specified type of rounds 
     * (cartridges). Normally this machine gun is coaxial with the primary cannon 
     * located in the turret. The method can do nothing if the battle tank has 
     * no coaxial machine gun.
     * 
     * @param patroneType The type of the coaxial machine gun's rounds (cartridges).
     */
    @Override
    public void loadCoaxialMaschinengewehr(final Schiesspatrone patroneType) {
        controlTurmMG_.load(patroneType);
    }
    
    /**
     * The method returns the course machine gun's ammunition list. Normally 
     * this machine gun is located in the front side of the hull. The method can 
     * return an empty list if the battle tank has no course machine gun.
     * 
     * @return List<Schiesspatrone> The coaxial machine gun's ammunition list.
     */
    @Override
    public List<Schiesspatrone> getCourseMaschinengewehrMunitionList() {
        return controlTurmMG_.getMunitionList();
    }
    
    /**
     * The method loads the course machine gun with the specified type of rounds 
     * (cartridges). Normally this machine gun is located in the front side of 
     * the hull. The method can do nothing if the battle tank has no course 
     * machine gun.
     * 
     * @param patroneType The type of the coaxial machine gun's rounds (cartridges).
     */
    @Override
    public void loadCourseMaschinengewehr(final Schiesspatrone patroneType) {
        controlTurmMG_.load(patroneType);
    }
    
    /**
     * The method returns the cupola machine gun's ammunition list. Normally 
     * this machine gun is located beside the commander's cupola. The method can 
     * return an empty list if the battle tank has no cupola machine gun.
     * 
     * @return List<Schiesspatrone> The cupola machine gun's ammunition list.
     */
    @Override
    public List<Schiesspatrone> getCupolaMaschinengewehrMunitionList() {
        return controlTurmMG_.getMunitionList();
    }
    
    /**
     * The method loads the cupola machine gun with the specified type of rounds 
     * (cartridges). Normally this machine gun is located beside the commander's 
     * cupola. The method can do nothing if the battle tank has no cupola machine 
     * gun.
     * 
     * @param patroneType The type of the cupola machine gun's rounds (cartridges).
     */
    @Override
    public void loadCupolaMaschinengewehr(final Schiesspatrone patroneType) {
        controlTurmMG_.load(patroneType);
    }
    
    /**
     * The method sets a scale for the battle tank's shells.
     * 
     * @param granateScale A scale for the battle tank's shells.
     */
    @Override
    public void setGranateScale(final DgaJmeScale granateScale) {
        granateScale_ = granateScale;
        if (controlTurmkanone_ != null) {
            controlTurmkanone_.setMunitionScale(granateScale_);
        }
    }
    
    /**
     * The method sets a scale for the battle tank's bullets.
     * 
     * @param geschossScale A scale for the battle tank's bullets.
     */
    @Override
    public void setGeschossScale(final DgaJmeScale geschossScale) {
        geschossScale_ = geschossScale;
        if (controlTurmMG_ != null) {
            controlTurmMG_.setMunitionScale(geschossScale_);
        }
    }
    //
    // *************************************************************************
    //
    /**
     * The method initializes the armoured fighting carriage's spatial and
     * controls.
     */
    @Override
    public void initModel() {
        // Start timer
        final long buildStart_ = System.currentTimeMillis();
        //
        physicsSpace_.addCollisionListener(this);
        //
        texTransparent_ = assetManager_.loadTexture("Materials/TransparentTexture.png");
        matTransparent_ = new Material(assetManager_, "Common/MatDefs/Misc/Unshaded.j3md");
        matTransparent_.setTexture("ColorMap", texTransparent_);
        matTransparent_.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);  // Activate transparency
        
        // Panzer
        nodePanzer_ = new Node("Panzer");
        nodePanzer_.setLocalTranslation(initPosition_);
        nodePanzer_.setLocalRotation(initRotation_);
        compoundPanzer_ = new CompoundCollisionShape();
        //
        modelPanzer_ = new DefaultPanzerungPhysicsModel(
            this, nodePanzer_, panzerScale_, compoundPanzer_, PANZER_MASS_);
        // Setting the scale for the whole model only leads to the performance loss 
        // that is stipulated by the jME, so the scale is set for every part separately.
        //
        controlPanzer_ = new DefaultKampfpanzerControl(modelPanzer_);
        nodePanzer_.addControl(controlPanzer_);
        controlPanzer_.setSpatial(nodePanzer_);
        // Sets the node to kinematic mode. in this mode the node is not affected
        // by physics but affects other physics objects. Its kinetic force is
        // calculated by the amount of movement it is exposed to and its weight.
        //controlPanzer_.setKinematic(true);
        // Sets this control to kinematic spatial mode so that the spatials transform
        // will be applied to the rigidbody in kinematic mode, defaults to true.
        //controlPanzer_.setKinematicSpatial(true);
        
        // Panzerwannenvorpunkt
        nodePanzerwannenvorpunkt_ = new Node("Panzerwannenvorpunkt");
        sptlPanzerwannenvorpunkt_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Panzerwannenvorpunkt.mesh.j3o");
        nodePanzerwannenvorpunkt_.attachChild(sptlPanzerwannenvorpunkt_);
        //nodePanzerwannenvorpunkt_.setMaterial(matTransparent_);
        geomPanzerwannenvorpunkt_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlPanzerwannenvorpunkt_, "Panzerwannenvorpunkt");
        bbPanzerwannenvorpunkt_ = (BoundingBox) geomPanzerwannenvorpunkt_.getModelBound();
        centerPanzerwannenvorpunkt_ = bbPanzerwannenvorpunkt_.getCenter();
        
        // Turmvorpunkt
        nodeTurmvorpunkt_ = new Node("Turmvorpunkt");
        sptlTurmvorpunkt_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmvorpunkt.mesh.j3o");
        nodeTurmvorpunkt_.attachChild(sptlTurmvorpunkt_);
        //nodeTurmvorpunkt_.setMaterial(matTransparent_);
        geomTurmvorpunkt_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmvorpunkt_, "Turmvorpunkt");
        bbTurmvorpunkt_ = (BoundingBox) geomTurmvorpunkt_.getModelBound();
        centerTurmvorpunkt_ = bbTurmvorpunkt_.getCenter();
        
        // Panzerwanne
        nodeWanne_ = new Node("Panzerwanne");
        sptlWanne_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Panzerwanne.mesh.j3o");
        nodeWanne_.attachChild(sptlWanne_);
        geomWanne_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlWanne_, "Panzerwanne");
        bbWanne_ = (BoundingBox) geomWanne_.getModelBound();
        centerWanne_ = bbWanne_.getCenter();
        //
        geomcshWanne_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Panzerwanne_CollisionShape.mesh.j3o"),
            "Panzerwanne_CollisionShape");
        bbcshWanne_ = (BoundingBox) geomcshWanne_.getModelBound();
        centercshWanne_ = bbcshWanne_.getCenter();
        cshWanne_ = new HullCollisionShape(geomcshWanne_.getMesh());
        compoundWanne_ = new CompoundCollisionShape();
        compoundWanne_.addChildShape(cshWanne_, new Vector3f(
            centerWanne_.getX() - centercshWanne_.getX(),
            centerWanne_.getY() - centercshWanne_.getY() - (bbcshWanne_.getYExtent() - bbWanne_.getYExtent()),
            centerWanne_.getZ() - centercshWanne_.getZ() - (bbcshWanne_.getZExtent() - bbWanne_.getZExtent())));
        //
        modelWanne_ = new DefaultPanzerwanneModel(this, nodeWanne_, panzerScale_, compoundWanne_);
        modelWanne_.setFrontUpperGlacisArmoring(WANNE_FRONT_UPPER_GLACIS_ARMORING_VALUE_);
        modelWanne_.setFrontLowerGlacisArmoring(WANNE_FRONT_LOWER_GLACIS_ARMORING_VALUE_);
        modelWanne_.setRearArmoring(WANNE_REAR_ARMORING_VALUE_);
        modelWanne_.setLeftArmoring(WANNE_SIDE_ARMORING_VALUE_);
        modelWanne_.setRightArmoring(WANNE_SIDE_ARMORING_VALUE_);
        modelWanne_.setTopArmoring(WANNE_TOP_ARMORING_VALUE_);
        modelWanne_.setBottomArmoring(WANNE_BOTTOM_ARMORING_VALUE_);
        modelWanne_.setVorpunkt(nodePanzerwannenvorpunkt_);
        modelWanne_.setBoundingBox(bbWanne_);
        //
        controlWanne_ = new DefaultPanzerwanneControl(modelWanne_);
        nodeWanne_.addControl(controlWanne_);
        controlWanne_.setSpatial(nodeWanne_);
        //
        nodeWanne_.setShadowMode(RenderQueue.ShadowMode.Cast);
        
        // Turm Pivot Point
        nodeTurmPivot_ = new Node("TurmPivotNode");
        nodeTurmPivot_.setLocalTranslation(0f, 0f, 0f);
        
        // Turm
        nodeTurm_ = new Node("Turm");
        sptlTurm_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turm.mesh.j3o");
        nodeTurm_.attachChild(sptlTurm_);
        geomTurm_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurm_, "Turm");
        bbTurm_ = (BoundingBox) geomTurm_.getModelBound();
        centerTurm_ = bbTurm_.getCenter();
        //
        compoundTurm_ = new CompoundCollisionShape();
        //
        geomcshTurm_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Turm_CollisionShape.mesh.j3o"),
            "Turm_CollisionShape");
        bbcshTurm_ = (BoundingBox) geomcshTurm_.getModelBound();
        centercshTurm_ = bbcshTurm_.getCenter();
        cshTurm_ = new HullCollisionShape(geomcshTurm_.getMesh());
        compoundTurm_.addChildShape(cshTurm_, new Vector3f(
            centercshTurm_.getX(),
            centerTurm_.getY() - centercshTurm_.getY() + (bbcshTurm_.getYExtent() - bbTurm_.getYExtent()),
            centerTurm_.getZ() - centercshTurm_.getZ() + (bbcshTurm_.getZExtent() - bbTurm_.getZExtent())));
        //
        modelTurm_ = new DefaultPanzerungModel(this, nodeTurm_, panzerScale_, compoundTurm_);
        modelTurm_.setFrontArmoring(TURM_FRONT_ARMORING_VALUE_);
        modelTurm_.setRearArmoring(TURM_REAR_ARMORING_VALUE_);
        modelTurm_.setLeftArmoring(TURM_SIDE_ARMORING_VALUE_);
        modelTurm_.setRightArmoring(TURM_SIDE_ARMORING_VALUE_);
        modelTurm_.setTopArmoring(TURM_TOP_ARMORING_VALUE_);
        modelTurm_.setVorpunkt(nodeTurmvorpunkt_);
        modelTurm_.setBoundingBox(bbTurm_);
        //
        controlTurm_ = new DefaultTurmControl(modelTurm_, nodeTurmPivot_);
        // Sets the friction of this physics object.
        //controlTurret_.setFriction(0.2f);
        // Sets the sleeping thresholds, these define when the object gets deactivated
        // to save ressources. Low values keep the object active when it barely moves.
        // @param linear the linear sleeping threshold
        // @param angular the angular sleeping threshold
        //controlTurret_.setSleepingThresholds(0.7f, 0.7f);
        //controlTurret_.setDamping(1.0f, 1.0f);
        //controlTurret_.setAngularFactor(0.3f); // This is for better collisions.
        nodeTurm_.setShadowMode(RenderQueue.ShadowMode.Cast);
        nodeTurm_.addControl(controlTurm_);
        controlTurm_.setSpatial(nodeTurm_);

        // Turmkanone Pivot Point
        nodeTurmkanonePivot_ = new Node("TurmkanonePivotNode");
        nodeTurmkanonePivot_.setLocalTranslation(0f, 0f, 0f);
        
        // Turmkanoneneffektpunkt
        sptlTurmkanonenEffPktModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmkanoneneffektpunkt.mesh.j3o");
        geomTurmkanonenEffPktModel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanonenEffPktModel_, "Turmkanoneneffektpunkt");
        bbTurmkanonenEffPktModel_ = (BoundingBox) geomTurmkanonenEffPktModel_.getModelBound();
        centerTurmkanonenEffPktModel_ = bbTurmkanonenEffPktModel_.getCenter();
        //
        nodeTurmkanonenEffPkt_ = new Node("Turmkanoneneffektpunkt");
        sptlTurmkanonenEffPkt_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmkanoneneffektpunkt.mesh.j3o");
        nodeTurmkanonenEffPkt_.attachChild(sptlTurmkanonenEffPkt_);
        geomTurmkanonenEffPkt_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanonenEffPkt_, "Turmkanoneneffektpunkt");
        bbTurmkanonenEffPkt_ = (BoundingBox) geomTurmkanonenEffPkt_.getModelBound();
        centerTurmkanonenEffPkt_ = bbTurmkanonenEffPkt_.getCenter();
        nodeTurmkanonenEffPkt_.setLocalTranslation(
            0f, 0f, centerTurmkanonenEffPktModel_.getZ() / 2.0f); // Manual translation
        
        // Set transparent material for the effect point.
        //nodeTurmkanonenEffPkt_.setMaterial(matTransparent_);
        cshTurmkanonenEffPkt_ = new HullCollisionShape(geomTurmkanonenEffPkt_.getMesh());
        modelTurmkanonenEffPkt_ = new DefaultPanzerungModel(
            this, nodeTurmkanonenEffPkt_, panzerScale_, cshTurmkanonenEffPkt_);
        
        // Turmkanonenfeuerpunkt
        sptlTurmkanonenFrPktModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmkanonenfeuerpunkt.mesh.j3o");
        geomTurmkanonenFrPktModel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanonenFrPktModel_, "Turmkanonenfeuerpunkt");
        bbTurmkanonenFrPktModel_ = (BoundingBox) geomTurmkanonenFrPktModel_.getModelBound();
        centerTurmkanonenFrPktModel_ = bbTurmkanonenFrPktModel_.getCenter();
        //
        nodeTurmkanonenFrPkt_ = new Node("Turmkanonenfeuerpunkt");
        sptlTurmkanonenFrPkt_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmkanonenfeuerpunkt.mesh.j3o");
        nodeTurmkanonenFrPkt_.attachChild(sptlTurmkanonenFrPkt_);
        geomTurmkanonenFrPkt_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanonenFrPkt_, "Turmkanonenfeuerpunkt");
        bbTurmkanonenFrPkt_ = (BoundingBox) geomTurmkanonenFrPkt_.getModelBound();
        centerTurmkanonenFrPkt_ = bbTurmkanonenFrPkt_.getCenter();
        nodeTurmkanonenFrPkt_.setLocalTranslation(
            0f, 0f, centerTurmkanonenFrPktModel_.getZ() * 1.1f); // Manual translation
        // Set transparent material for the firing point.
        nodeTurmkanonenFrPkt_.setMaterial(matTransparent_);
        cshTurmkanonenFrPkt_ = new HullCollisionShape(geomTurmkanonenFrPkt_.getMesh());
        compoundTurmkanonenFrPkt_ = new CompoundCollisionShape();
        compoundTurmkanonenFrPkt_.addChildShape(
            cshTurmkanonenFrPkt_, new Vector3f(0f, 0f, -100.0f)); // Manual translation
        
        // Turmkanonenmaske
        sptlTurmkanonenmaskeModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmkanonenmaske.mesh.j3o");
        geomTurmkanonenmaskeModel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanonenmaskeModel_, "Turmkanonenmaske");
        bbTurmkanonenmaskeModel_ = (BoundingBox) geomTurmkanonenmaskeModel_.getModelBound();
        centerTurmkanonenmaskeModel_ = bbTurmkanonenmaskeModel_.getCenter();
        //
        nodeTurmkanonenmaske_ = new Node("Turmkanonenmaske");
        sptlTurmkanonenmaske_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmkanonenmaske.mesh.j3o");
        nodeTurmkanonenmaske_.attachChild(sptlTurmkanonenmaske_);
        geomTurmkanonenmaske_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanonenmaske_, "Turmkanonenmaske");
        bbTurmkanonenmaske_ = (BoundingBox) geomTurmkanonenmaske_.getModelBound();
        centerTurmkanonenmaske_ = bbTurmkanonenmaske_.getCenter();
        nodeTurmkanonenmaske_.getLocalTranslation().setZ(
            centerTurmkanonenmaskeModel_.getZ() - centerTurmkanonenmaske_.getZ());
        //
        geomcshTurmkanonenmaske_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Specials/Turmkanonenmaske_CollisionShape.mesh.j3o"),
            "Turmkanonenmaske_CollisionShape");
        bbcshTurmkanonenmaske_ = (BoundingBox) geomcshTurmkanonenmaske_.getModelBound();
        centercshTurmkanonenmaske_ = bbcshTurmkanonenmaske_.getCenter();
        cshTurmkanonenmaske_ = new HullCollisionShape(geomcshTurmkanonenmaske_.getMesh());
        compoundTurmkanonenmaske_ = new CompoundCollisionShape();
        compoundTurmkanonenmaske_.addChildShape(
            cshTurmkanonenmaske_, new Vector3f(0f, 0f, -15.0f)); // Manual translation
        //
        modelTurmkanonenmaske_ = new DefaultPanzerungModel(
            this, nodeTurmkanonenmaske_, panzerScale_, compoundTurmkanonenmaske_);
        modelTurmkanonenmaske_.setFrontArmoring(TURMKANONENMASKE_FRONT_ARMORING_VALUE_);
        modelTurmkanonenmaske_.setLeftArmoring(TURMKANONENMASKE_SIDE_ARMORING_VALUE_);
        modelTurmkanonenmaske_.setRightArmoring(TURMKANONENMASKE_SIDE_ARMORING_VALUE_);
        modelTurmkanonenmaske_.setTopArmoring(TURMKANONENMASKE_TOP_ARMORING_VALUE_);
        modelTurmkanonenmaske_.setBottomArmoring(TURMKANONENMASKE_BOTTOM_ARMORING_VALUE_);
        modelTurmkanonenmaske_.setBoundingBox(bbTurmkanonenmaske_);
        //
        controlTurmkanonenmaske_ = new DefaultKanonenmaskeControl(modelTurmkanonenmaske_);
        nodeTurmkanonenmaske_.addControl(controlTurmkanonenmaske_);
        controlTurmkanonenmaske_.setSpatial(nodeTurmkanonenmaske_);
        
        // Turmkanone
        sptlTurmkanoneModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmkanone.mesh.j3o");
        geomTurmkanoneModel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanoneModel_, "Turmkanone");
        bbTurmkanoneModel_ = (BoundingBox) geomTurmkanoneModel_.getModelBound();
        centerTurmkanoneModel_ = bbTurmkanoneModel_.getCenter();
        //
        nodeTurmkanone_ = new Node("Turmkanone");
        sptlTurmkanone_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmkanone.mesh.j3o");
        nodeTurmkanone_.attachChild(sptlTurmkanone_);
        geomTurmkanone_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmkanone_, "Turmkanone");
        bbTurmkanone_ = (BoundingBox) geomTurmkanone_.getModelBound();
        centerTurmkanone_ = bbTurmkanone_.getCenter();
        nodeTurmkanone_.setLocalTranslation(
            centerTurmkanoneModel_.getX() - centerTurmkanone_.getX(), 
            0f, 
            centerTurmkanoneModel_.getZ() - centerTurmkanone_.getZ());
        //
        geomcshTurmkanone_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Specials/Turmkanone_CollisionShape.mesh.j3o"),
            "Turmkanone_CollisionShape");
        bbcshTurmkanone_ = (BoundingBox) geomcshTurmkanone_.getModelBound();
        centercshTurmkanone_ = bbcshTurmkanone_.getCenter();
        cshTurmkanone_ = new HullCollisionShape(geomcshTurmkanone_.getMesh());
        //
        compoundTurmkanonenmaske_.addChildShape(
            cshTurmkanone_, new Vector3f(0f, 0f, -15.0f)); // Manual translation
        //
        modelTurmkanone_ = new DefaultPanzerungModel(
            this, nodeTurmkanone_, panzerScale_, cshTurmkanone_);
        modelTurmkanone_.setLeftArmoring(TURMKANONE_ARMORING_VALUE_);
        modelTurmkanone_.setRightArmoring(TURMKANONE_ARMORING_VALUE_);
        modelTurmkanone_.setTopArmoring(TURMKANONE_ARMORING_VALUE_);
        modelTurmkanone_.setBottomArmoring(TURMKANONE_ARMORING_VALUE_);
        modelTurmkanone_.setBoundingBox(bbTurmkanone_);

        // Turmmaschinengewehreffektpunkt
        sptlTurmMGEffPktModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmmaschinengewehreffektpunkt.mesh.j3o");
        geomTurmMGEffPktModel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmMGEffPktModel_, "Turmmaschinengewehreffektpunkt");
        bbTurmMGEffPktModel_ = (BoundingBox) geomTurmMGEffPktModel_.getModelBound();
        centerTurmMGEffPktModel_ = bbTurmMGEffPktModel_.getCenter();
        //
        nodeTurmMGEffPkt_ = new Node("Turmmaschinengewehreffektpunkt");
        sptlTurmMGEffPkt_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmmaschinengewehreffektpunkt.mesh.j3o");
        nodeTurmMGEffPkt_.attachChild(sptlTurmMGEffPkt_);
        geomTurmMGEffPkt_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmMGEffPkt_, "Turmmaschinengewehreffektpunkt");
        bbTurmMGEffPkt_ = (BoundingBox) geomTurmMGEffPkt_.getModelBound();
        centerTurmMGEffPkt_ = bbTurmMGEffPkt_.getCenter();
        nodeTurmMGEffPkt_.setLocalTranslation(
            0f, 0f, centerTurmMGEffPktModel_.getZ() / 2.0f); // Manual translation
        // Set transparent material for the effect point.
        //nodeTurmMGEffPkt_.setMaterial(matTransparent_);
        cshTurmMGEffPkt_ = new HullCollisionShape(geomTurmMGEffPkt_.getMesh());
        modelTurmMGEffPkt_ = new DefaultPanzerungModel(
            this, nodeTurmMGEffPkt_, panzerScale_, cshTurmMGEffPkt_);
        
        // Turmmaschinengewehr
        sptlTurmMGModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmmaschinengewehr.mesh.j3o");
        geomTurmMGModel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmMGModel_, "Turmmaschinengewehr");
        bbTurmMGModel_ = (BoundingBox) geomTurmMGModel_.getModelBound();
        centerTurmMGModel_ = bbTurmMGModel_.getCenter();
        //
        nodeTurmMG_ = new Node("Turmmaschinengewehr");
        sptlTurmMG_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmmaschinengewehr.mesh.j3o");
        nodeTurmMG_.attachChild(sptlTurmMG_);
        geomTurmMG_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmMG_, "Turmmaschinengewehr");
        bbTurmMG_ = (BoundingBox) geomTurmMG_.getModelBound();
        centerTurmMG_ = bbTurmMG_.getCenter();
        nodeTurmMG_.setLocalTranslation(new Vector3f(
            0f,
            centerTurmMG_.getY() - bbTurmMGModel_.getYExtent() - bbTurmMG_.getYExtent(),
            centerTurmMGModel_.getZ() - centerTurmMG_.getZ() + bbTurmMGModel_.getZExtent()));
        //
        geomcshTurmMG_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Specials/Turmmaschinengewehr_CollisionShape.mesh.j3o"),
            "Turmmaschinengewehr_CollisionShape");
        bbcshTurmMG_ = (BoundingBox) geomcshTurmMG_.getModelBound();
        centercshTurmMG_ = bbcshTurmMG_.getCenter();
        cshTurmMG_ = new HullCollisionShape(geomcshTurmMG_.getMesh());
        //
        modelTurmMG_ = new DefaultPanzerungModel(this, nodeTurmMG_, panzerScale_, cshTurmMG_);
        modelTurmMG_.setBoundingBox(bbTurmMG_);
        
        // controlTurmkanone
        modelTurmkanonenFrPkt_ = new DefaultPanzerkanoneModel(
            assetManager_, physicsSpace_, rootNode_, stateManager_, renderManager_, 
            this, nodeTurmkanonenFrPkt_, panzerScale_, compoundTurmkanonenFrPkt_, 
            nodeTurmkanonePivot_, nodeTurmkanonenEffPkt_);
        //
        controlTurmkanone_ = new KwK42(modelTurmkanonenFrPkt_);
        controlTurmkanone_.setMaxVerticalAngle(TURMKANONE_MAX_ELEVATION_ANGLE_);
        controlTurmkanone_.setMinVerticalAngle(TURMKANONE_MAX_DEPRESSION_ANGLE_);
        controlTurmkanone_.addAttachedEquipment(
            modelTurmkanonenmaske_,
            modelTurmkanone_,
            modelTurmMG_
        );
        controlTurmkanone_.addDistantEquipment(
            modelTurmkanonenEffPkt_,
            modelTurmMGEffPkt_
        );
        controlTurmkanone_.setMunitionScale(granateScale_);
        // Sets the friction of this physics object.
        //controlTurretGun_.setFriction(0.2f);
        // Sets the sleeping thresholds, these define when the object gets deactivated
        // to save ressources. Low values keep the object active when it barely moves.
        // @param linear the linear sleeping threshold
        // @param angular the angular sleeping threshold
        //controlTurretGun_.setSleepingThresholds(0.7f, 0.7f);
        //controlTurretGun_.setDamping(1.0f, 1.0f);
        //controlTurretGun_.setAngularFactor(0.3f); // This is for better collisions.
        nodeTurmkanonenFrPkt_.setShadowMode(RenderQueue.ShadowMode.Cast);
        nodeTurmkanonenFrPkt_.addControl(controlTurmkanone_);
        controlTurmkanone_.setSpatial(nodeTurmkanonenFrPkt_);
       
        // Turmmaschinengewehrfeuerpunkt
        sptlTurmMGFrPktModel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmmaschinengewehrfeuerpunkt.mesh.j3o");
        geomTurmMGFrPktModel_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlTurmMGFrPktModel_, "Turmmaschinengewehrfeuerpunkt");
        bbTurmMGFrPktModel_ = (BoundingBox) geomTurmMGFrPktModel_.getModelBound();
        centerTurmMGFrPktModel_ = bbTurmMGFrPktModel_.getCenter();
        //
        nodeTurmMGFrPkt_ = new Node("Turmmaschinengewehrfeuerpunkt");
        sptlTurmMGFrPkt_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Specials/Turmmaschinengewehrfeuerpunkt.mesh.j3o");
        nodeTurmMGFrPkt_.attachChild(sptlTurmMGFrPkt_);
        geomTurmMGFrPkt_ = DgaJmeUtils.getGeometryFromSpatialByName(sptlTurmMGFrPkt_, "Turmmaschinengewehrfeuerpunkt");
        bbTurmMGFrPkt_ = (BoundingBox) geomTurmMGFrPkt_.getModelBound();
        centerTurmMGFrPkt_ = bbTurmMGFrPkt_.getCenter();
        nodeTurmMGFrPkt_.setLocalTranslation(new Vector3f(
            centerTurmMGFrPktModel_.getX(),
            0f,
            centerTurmMGFrPktModel_.getZ() * 1.3f)); // Manual translation
        // Set transparent material for the firing point.
        nodeTurmMGFrPkt_.setMaterial(matTransparent_);
        cshTurmMGFrPkt_ = new HullCollisionShape(geomTurmMGFrPkt_.getMesh());
        compoundTurmMGFrPkt_ = new CompoundCollisionShape();
        compoundTurmMGFrPkt_.addChildShape(cshTurmMGFrPkt_, new Vector3f(0f, 0f, -100.0f)); // Manual translation
        compoundTurmMGFrPkt_.addChildShape(
            cshTurmMG_, new Vector3f(50.0f, 0f, -90.0f)); // Manual translation
        //
        nodeTurmMGEffPkt_.getLocalTranslation().setX(-25.0f); // Manual translation
        //
        modelTurmMGFrPkt_ = new DefaultMaschinengewehrModel(
            assetManager_, physicsSpace_, rootNode_, stateManager_, renderManager_, 
            this, nodeTurmMGFrPkt_, panzerScale_, compoundTurmMGFrPkt_, nodeTurmkanonePivot_, 
            nodeTurmMGEffPkt_);
        //
        controlTurmMG_ = new KwMG34(modelTurmMGFrPkt_);
        controlTurmMG_.setMaxVerticalAngle(TURM_MG_MAX_ELEVATION_ANGLE_);
        controlTurmMG_.setMinVerticalAngle(TURM_MG_MAX_DEPRESSION_ANGLE_);
        controlTurmMG_.setMaxFireSectorAngle(TURM_MG_FIRE_SECTOR_ANGLE_);
        controlTurmMG_.setMunitionScale(geschossScale_);
        // Sets the friction of this physics object.
        //controlTurretMachineGun_.setFriction(0.2f);
        // Sets the sleeping thresholds, these define when the object gets deactivated
        // to save ressources. Low values keep the object active when it barely moves.
        // @param linear the linear sleeping threshold
        // @param angular the angular sleeping threshold
        //controlTurretMachineGun_.setSleepingThresholds(0.7f, 0.7f);
        //controlTurretMachineGun_.setDamping(1.0f, 1.0f);
        //controlTurretMachineGun_.setAngularFactor(0.3f); // This is for better collisions.
        nodeTurmMGFrPkt_.setShadowMode(RenderQueue.ShadowMode.Cast);
        nodeTurmMGFrPkt_.addControl(controlTurmMG_);
        controlTurmMG_.setSpatial(nodeTurmMGFrPkt_);
        // Sets the collision group number for this physics object. The groups 
        // are integer bit masks and some pre-made variables are available in 
        // CollisionObject. All physics objects are by default in COLLISION_GROUP_01.
        // Two object will collide when one of the parties has the collisionGroup 
        // of the other in its collideWithGroups set.
        //controlTurmMG_.setCollisionGroup(PhysicsCollisionObject.COLLISION_GROUP_01);
        
        // Gleiskette Linke
        nodeGleisketteLinke_ = new Node("GleisketteLinke");
        sptlGleisketteLinke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/GleisketteLinke.mesh.j3o");
        nodeGleisketteLinke_.attachChild(sptlGleisketteLinke_);
        geomGleisketteLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlGleisketteLinke_, "GleisketteLinke");
        bbGleisketteLinke_ = (BoundingBox) geomGleisketteLinke_.getModelBound();
        centerGleisketteLinke_ = bbGleisketteLinke_.getCenter();
        //
        geomcshGleisketteLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/GleisketteLinke_CollisionShape.mesh.j3o"),
            "GleisketteLinke_CollisionShape");
        bbcshGleisketteLinke_ = (BoundingBox) geomcshGleisketteLinke_.getModelBound();
        centercshGleisketteLinke_ = bbcshGleisketteLinke_.getCenter();
        cshGleisketteLinke_ = new HullCollisionShape(geomcshGleisketteLinke_.getMesh());
        compoundGleisketteLinke_ = new CompoundCollisionShape();
        compoundGleisketteLinke_.addChildShape(cshGleisketteLinke_, new Vector3f(
            centerGleisketteLinke_.getX() - centercshGleisketteLinke_.getX(),
            centerGleisketteLinke_.getY() - centercshGleisketteLinke_.getY(),
            centerGleisketteLinke_.getZ() - centercshGleisketteLinke_.getZ()));
        //
        modelGleisketteLinke_ = new DefaultPanzerungModel(
            this, nodeGleisketteLinke_, panzerScale_, compoundGleisketteLinke_);
        modelGleisketteLinke_.setFrontArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteLinke_.setRearArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteLinke_.setTopArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteLinke_.setBottomArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteLinke_.setBoundingBox(bbGleisketteLinke_);
        //
        controlGleisketteLinke_ = new DefaultGleisketteControl(modelGleisketteLinke_);
        nodeGleisketteLinke_.addControl(controlGleisketteLinke_);
        controlGleisketteLinke_.setSpatial(nodeGleisketteLinke_);
        
        // Gleiskette Rechte
        nodeGleisketteRechte_ = new Node("GleisketteRechte");
        sptlGleisketteRechte_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/GleisketteRechte.mesh.j3o");
        nodeGleisketteRechte_.attachChild(sptlGleisketteRechte_);
        geomGleisketteRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlGleisketteRechte_, "GleisketteRechte");
        bbGleisketteRechte_ = (BoundingBox) geomGleisketteRechte_.getModelBound();
        centerGleisketteRechte_ = bbGleisketteRechte_.getCenter();
        //
        geomcshGleisketteRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/GleisketteRechte_CollisionShape.mesh.j3o"),
            "GleisketteRechte_CollisionShape");
        bbcshGleisketteRechte_ = (BoundingBox) geomcshGleisketteRechte_.getModelBound();
        centercshGleisketteRechte_ = bbcshGleisketteRechte_.getCenter();
        cshGleisketteRechte_ = new HullCollisionShape(geomcshGleisketteRechte_.getMesh());
        compoundGleisketteRechte_ = new CompoundCollisionShape();
        compoundGleisketteRechte_.addChildShape(cshGleisketteRechte_, new Vector3f(
            centerGleisketteRechte_.getX() - centercshGleisketteRechte_.getX(),
            centerGleisketteRechte_.getY() - centercshGleisketteRechte_.getY(),
            centerGleisketteRechte_.getZ() - centercshGleisketteRechte_.getZ()));
        //
        modelGleisketteRechte_ = new DefaultPanzerungModel(
            this, nodeGleisketteRechte_, panzerScale_, compoundGleisketteRechte_);
        modelGleisketteRechte_.setFrontArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteRechte_.setRearArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteRechte_.setTopArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteRechte_.setBottomArmoring(GLEISKETTE_ARMORING_VALUE_);
        modelGleisketteRechte_.setBoundingBox(bbGleisketteRechte_);
        //
        controlGleisketteRechte_ = new DefaultGleisketteControl(modelGleisketteRechte_);
        nodeGleisketteRechte_.addControl(controlGleisketteRechte_);
        controlGleisketteRechte_.setSpatial(nodeGleisketteRechte_);
        
        // Antriebsrad Linke
        nodeAntriebsradLinke_ = new Node("AntriebsradLinke");
        sptlAntriebsradLinke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/AntriebsradLinke.mesh.j3o");
        nodeAntriebsradLinke_.attachChild(sptlAntriebsradLinke_);
        geomAntriebsradLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlAntriebsradLinke_, "AntriebsradLinke");
        bbAntriebsradLinke_ = (BoundingBox) geomAntriebsradLinke_.getModelBound();
        centerAntriebsradLinke_ = bbAntriebsradLinke_.getCenter();
        cshAntriebsradLinke_ = new HullCollisionShape(geomAntriebsradLinke_.getMesh());
        compoundAntriebsradLinke_ = new CompoundCollisionShape();
        compoundAntriebsradLinke_.addChildShape(
            cshAntriebsradLinke_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelAntriebsradLinke_ = new DefaultPanzerungModel(
            this, nodeAntriebsradLinke_, panzerScale_, compoundAntriebsradLinke_);
        modelAntriebsradLinke_.setFrontArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradLinke_.setRearArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradLinke_.setLeftArmoring(ANTRIEBSRAD_SIDE_ARMORING_VALUE_);
        modelAntriebsradLinke_.setRightArmoring(ANTRIEBSRAD_SIDE_ARMORING_VALUE_);
        modelAntriebsradLinke_.setTopArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradLinke_.setBottomArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradLinke_.setBoundingBox(bbAntriebsradLinke_);
        //
        controlAntriebsradLinke_ = new DefaultAntriebsradControl(modelAntriebsradLinke_);
        nodeAntriebsradLinke_.addControl(controlAntriebsradLinke_);
        controlAntriebsradLinke_.setSpatial(nodeAntriebsradLinke_);
        
        // Laufrad Linke 1
        nodeLaufradLinke1_ = new Node("LaufradLinke1");
        sptlLaufradLinke1_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke1.mesh.j3o");
        nodeLaufradLinke1_.attachChild(sptlLaufradLinke1_);
        geomLaufradLinke1_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke1_, "LaufradLinke1");
        bbLaufradLinke1_ = (BoundingBox) geomLaufradLinke1_.getModelBound();
        centerLaufradLinke1_ = bbLaufradLinke1_.getCenter();
        cshLaufradLinke1_ = new HullCollisionShape(geomLaufradLinke1_.getMesh());
        compoundLaufradLinke1_ = new CompoundCollisionShape();
        compoundLaufradLinke1_.addChildShape(
            cshLaufradLinke1_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke1_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke1_, panzerScale_, compoundLaufradLinke1_);
        modelLaufradLinke1_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke1_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke1_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke1_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke1_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke1_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke1_.setBoundingBox(bbLaufradLinke1_);
        //
        controlLaufradLinke1_ = new DefaultLaufradControl(modelLaufradLinke1_);
        nodeLaufradLinke1_.addControl(controlLaufradLinke1_);
        controlLaufradLinke1_.setSpatial(nodeLaufradLinke1_);
        
        // Laufrad Linke 2
        nodeLaufradLinke2_ = new Node("LaufradLinke2");
        sptlLaufradLinke2_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke2.mesh.j3o");
        nodeLaufradLinke2_.attachChild(sptlLaufradLinke2_);
        geomLaufradLinke2_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke2_, "LaufradLinke2");
        bbLaufradLinke2_ = (BoundingBox) geomLaufradLinke2_.getModelBound();
        centerLaufradLinke2_ = bbLaufradLinke2_.getCenter();
        cshLaufradLinke2_ = new HullCollisionShape(geomLaufradLinke2_.getMesh());
        compoundLaufradLinke2_ = new CompoundCollisionShape();
        compoundLaufradLinke2_.addChildShape(
            cshLaufradLinke2_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke2_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke2_, panzerScale_, compoundLaufradLinke2_);
        modelLaufradLinke2_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke2_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke2_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke2_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke2_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke2_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke2_.setBoundingBox(bbLaufradLinke2_);
        //
        controlLaufradLinke2_ = new DefaultLaufradControl(modelLaufradLinke2_);
        nodeLaufradLinke2_.addControl(controlLaufradLinke2_);
        controlLaufradLinke2_.setSpatial(nodeLaufradLinke2_);
        
        // Laufrad Linke 3
        nodeLaufradLinke3_ = new Node("LaufradLinke3");
        sptlLaufradLinke3_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke3.mesh.j3o");
        nodeLaufradLinke3_.attachChild(sptlLaufradLinke3_);
        geomLaufradLinke3_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke3_, "LaufradLinke3");
        bbLaufradLinke3_ = (BoundingBox) geomLaufradLinke3_.getModelBound();
        centerLaufradLinke3_ = bbLaufradLinke3_.getCenter();
        cshLaufradLinke3_ = new HullCollisionShape(geomLaufradLinke3_.getMesh());
        compoundLaufradLinke3_ = new CompoundCollisionShape();
        compoundLaufradLinke3_.addChildShape(
            cshLaufradLinke3_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke3_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke3_, panzerScale_, compoundLaufradLinke3_);
        modelLaufradLinke3_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke3_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke3_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke3_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke3_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke3_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke3_.setBoundingBox(bbLaufradLinke3_);
        //
        controlLaufradLinke3_ = new DefaultLaufradControl(modelLaufradLinke3_);
        nodeLaufradLinke3_.addControl(controlLaufradLinke3_);
        controlLaufradLinke3_.setSpatial(nodeLaufradLinke3_);

        // Laufrad Linke 4
        nodeLaufradLinke4_ = new Node("LaufradLinke4");
        sptlLaufradLinke4_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke4.mesh.j3o");
        nodeLaufradLinke4_.attachChild(sptlLaufradLinke4_);
        geomLaufradLinke4_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke4_, "LaufradLinke4");
        bbLaufradLinke4_ = (BoundingBox) geomLaufradLinke4_.getModelBound();
        centerLaufradLinke4_ = bbLaufradLinke4_.getCenter();
        cshLaufradLinke4_ = new HullCollisionShape(geomLaufradLinke4_.getMesh());
        compoundLaufradLinke4_ = new CompoundCollisionShape();
        compoundLaufradLinke4_.addChildShape(
            cshLaufradLinke4_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke4_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke4_, panzerScale_, compoundLaufradLinke4_);
        modelLaufradLinke4_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke4_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke4_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke4_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke4_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke4_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke4_.setBoundingBox(bbLaufradLinke4_);
        //
        controlLaufradLinke4_ = new DefaultLaufradControl(modelLaufradLinke4_);
        nodeLaufradLinke4_.addControl(controlLaufradLinke4_);
        controlLaufradLinke4_.setSpatial(nodeLaufradLinke4_);

        // Laufrad Linke 5
        nodeLaufradLinke5_ = new Node("LaufradLinke5");
        sptlLaufradLinke5_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke5.mesh.j3o");
        nodeLaufradLinke5_.attachChild(sptlLaufradLinke5_);
        geomLaufradLinke5_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke5_, "LaufradLinke5");
        bbLaufradLinke5_ = (BoundingBox) geomLaufradLinke5_.getModelBound();
        centerLaufradLinke5_ = bbLaufradLinke5_.getCenter();
        cshLaufradLinke5_ = new HullCollisionShape(geomLaufradLinke5_.getMesh());
        compoundLaufradLinke5_ = new CompoundCollisionShape();
        compoundLaufradLinke5_.addChildShape(
            cshLaufradLinke5_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke5_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke5_, panzerScale_, compoundLaufradLinke5_);
        modelLaufradLinke5_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke5_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke5_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke5_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke5_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke5_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke5_.setBoundingBox(bbLaufradLinke5_);
        //
        controlLaufradLinke5_ = new DefaultLaufradControl(modelLaufradLinke5_);
        nodeLaufradLinke5_.addControl(controlLaufradLinke5_);
        controlLaufradLinke5_.setSpatial(nodeLaufradLinke5_);

        // Laufrad Linke 6
        nodeLaufradLinke6_ = new Node("LaufradLinke6");
        sptlLaufradLinke6_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke6.mesh.j3o");
        nodeLaufradLinke6_.attachChild(sptlLaufradLinke6_);
        geomLaufradLinke6_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke6_, "LaufradLinke6");
        bbLaufradLinke6_ = (BoundingBox) geomLaufradLinke6_.getModelBound();
        centerLaufradLinke6_ = bbLaufradLinke6_.getCenter();
        cshLaufradLinke6_ = new HullCollisionShape(geomLaufradLinke6_.getMesh());
        compoundLaufradLinke6_ = new CompoundCollisionShape();
        compoundLaufradLinke6_.addChildShape(
            cshLaufradLinke6_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke6_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke6_, panzerScale_, compoundLaufradLinke6_);
        modelLaufradLinke6_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke6_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke6_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke6_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke6_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke6_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke6_.setBoundingBox(bbLaufradLinke6_);
        //
        controlLaufradLinke6_ = new DefaultLaufradControl(modelLaufradLinke6_);
        nodeLaufradLinke6_.addControl(controlLaufradLinke6_);
        controlLaufradLinke6_.setSpatial(nodeLaufradLinke6_);

        // Laufrad Linke 7
        nodeLaufradLinke7_ = new Node("LaufradLinke7");
        sptlLaufradLinke7_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke7.mesh.j3o");
        nodeLaufradLinke7_.attachChild(sptlLaufradLinke7_);
        geomLaufradLinke7_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke7_, "LaufradLinke7");
        bbLaufradLinke7_ = (BoundingBox) geomLaufradLinke7_.getModelBound();
        centerLaufradLinke7_ = bbLaufradLinke7_.getCenter();
        cshLaufradLinke7_ = new HullCollisionShape(geomLaufradLinke7_.getMesh());
        compoundLaufradLinke7_ = new CompoundCollisionShape();
        compoundLaufradLinke7_.addChildShape(
            cshLaufradLinke7_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke7_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke7_, panzerScale_, compoundLaufradLinke7_);
        modelLaufradLinke7_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke7_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke7_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke7_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke7_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke7_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke7_.setBoundingBox(bbLaufradLinke7_);
        //
        controlLaufradLinke7_ = new DefaultLaufradControl(modelLaufradLinke7_);
        nodeLaufradLinke7_.addControl(controlLaufradLinke7_);
        controlLaufradLinke7_.setSpatial(nodeLaufradLinke7_);
        
        // Laufrad Linke 8
        nodeLaufradLinke8_ = new Node("LaufradLinke8");
        sptlLaufradLinke8_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradLinke8.mesh.j3o");
        nodeLaufradLinke8_.attachChild(sptlLaufradLinke8_);
        geomLaufradLinke8_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradLinke8_, "LaufradLinke8");
        bbLaufradLinke8_ = (BoundingBox) geomLaufradLinke8_.getModelBound();
        centerLaufradLinke8_ = bbLaufradLinke8_.getCenter();
        cshLaufradLinke8_ = new HullCollisionShape(geomLaufradLinke8_.getMesh());
        compoundLaufradLinke8_ = new CompoundCollisionShape();
        compoundLaufradLinke8_.addChildShape(
            cshLaufradLinke8_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradLinke8_ = new DefaultPanzerungModel(
            this, nodeLaufradLinke8_, panzerScale_, compoundLaufradLinke8_);
        modelLaufradLinke8_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke8_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke8_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke8_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradLinke8_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke8_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradLinke8_.setBoundingBox(bbLaufradLinke8_);
        //
        controlLaufradLinke8_ = new DefaultLaufradControl(modelLaufradLinke8_);
        nodeLaufradLinke8_.addControl(controlLaufradLinke8_);
        controlLaufradLinke8_.setSpatial(nodeLaufradLinke8_);
        
        // Leitrad Linke
        nodeLeitradLinke_ = new Node("LeitradLinke");
        sptlLeitradLinke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LeitradLinke.mesh.j3o");
        nodeLeitradLinke_.attachChild(sptlLeitradLinke_);
        geomLeitradLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLeitradLinke_, "LeitradLinke");
        bbLeitradLinke_ = (BoundingBox) geomLeitradLinke_.getModelBound();
        centerLeitradLinke_ = bbLeitradLinke_.getCenter();
        cshLeitradLinke_ = new HullCollisionShape(geomLeitradLinke_.getMesh());
        compoundLeitradLinke_ = new CompoundCollisionShape();
        compoundLeitradLinke_.addChildShape(
            cshLeitradLinke_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLeitradLinke_ = new DefaultPanzerungModel(
            this, nodeLeitradLinke_, panzerScale_, compoundLeitradLinke_);
        modelLeitradLinke_.setFrontArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradLinke_.setRearArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradLinke_.setLeftArmoring(LEITRAD_SIDE_ARMORING_VALUE_);
        modelLeitradLinke_.setRightArmoring(LEITRAD_SIDE_ARMORING_VALUE_);
        modelLeitradLinke_.setTopArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradLinke_.setBottomArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradLinke_.setBoundingBox(bbLeitradLinke_);
        //
        controlLeitradLinke_ = new DefaultLeitradControl(modelLeitradLinke_);
        nodeLeitradLinke_.addControl(controlLeitradLinke_);
        controlLeitradLinke_.setSpatial(nodeLeitradLinke_);
        
        // Antriebsrad Rechte
        nodeAntriebsradRechte_ = new Node("AntriebsradRechte");
        sptlAntriebsradRechte_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/AntriebsradRechte.mesh.j3o");
        nodeAntriebsradRechte_.attachChild(sptlAntriebsradRechte_);
        geomAntriebsradRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlAntriebsradRechte_, "AntriebsradRechte");
        bbAntriebsradRechte_ = (BoundingBox) geomAntriebsradRechte_.getModelBound();
        centerAntriebsradRechte_ = bbAntriebsradRechte_.getCenter();
        cshAntriebsradRechte_ = new HullCollisionShape(geomAntriebsradRechte_.getMesh());
        compoundAntriebsradRechte_ = new CompoundCollisionShape();
        compoundAntriebsradRechte_.addChildShape(
            cshAntriebsradRechte_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelAntriebsradRechte_ = new DefaultPanzerungModel(
            this, nodeAntriebsradRechte_, panzerScale_, compoundAntriebsradRechte_);
        modelAntriebsradRechte_.setFrontArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradRechte_.setRearArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradRechte_.setLeftArmoring(ANTRIEBSRAD_SIDE_ARMORING_VALUE_);
        modelAntriebsradRechte_.setRightArmoring(ANTRIEBSRAD_SIDE_ARMORING_VALUE_);
        modelAntriebsradRechte_.setTopArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradRechte_.setBottomArmoring(ANTRIEBSRAD_FRTB_ARMORING_VALUE_);
        modelAntriebsradRechte_.setBoundingBox(bbAntriebsradRechte_);
        //
        controlAntriebsradRechte_ = new DefaultAntriebsradControl(modelAntriebsradRechte_);
        nodeAntriebsradRechte_.addControl(controlAntriebsradRechte_);
        controlAntriebsradRechte_.setSpatial(nodeAntriebsradRechte_);
        
        // Laufrad Rechte 1
        nodeLaufradRechte1_ = new Node("LaufradRechte1");
        sptlLaufradRechte1_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte1.mesh.j3o");
        nodeLaufradRechte1_.attachChild(sptlLaufradRechte1_);
        geomLaufradRechte1_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte1_, "LaufradRechte1");
        bbLaufradRechte1_ = (BoundingBox) geomLaufradRechte1_.getModelBound();
        centerLaufradRechte1_ = bbLaufradRechte1_.getCenter();
        cshLaufradRechte1_ = new HullCollisionShape(geomLaufradRechte1_.getMesh());
        compoundLaufradRechte1_ = new CompoundCollisionShape();
        compoundLaufradRechte1_.addChildShape(
            cshLaufradRechte1_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte1_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte1_, panzerScale_, compoundLaufradRechte1_);
        modelLaufradRechte1_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte1_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte1_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte1_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte1_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte1_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte1_.setBoundingBox(bbLaufradRechte1_);
        //
        controlLaufradRechte1_ = new DefaultLaufradControl(modelLaufradRechte1_);
        nodeLaufradRechte1_.addControl(controlLaufradRechte1_);
        controlLaufradRechte1_.setSpatial(nodeLaufradRechte1_);
        
        // Laufrad Rechte 2
        nodeLaufradRechte2_ = new Node("LaufradRechte2");
        sptlLaufradRechte2_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte2.mesh.j3o");
        nodeLaufradRechte2_.attachChild(sptlLaufradRechte2_);
        geomLaufradRechte2_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte2_, "LaufradRechte2");
        bbLaufradRechte2_ = (BoundingBox) geomLaufradRechte2_.getModelBound();
        centerLaufradRechte2_ = bbLaufradRechte2_.getCenter();
        cshLaufradRechte2_ = new HullCollisionShape(geomLaufradRechte2_.getMesh());
        compoundLaufradRechte2_ = new CompoundCollisionShape();
        compoundLaufradRechte2_.addChildShape(
            cshLaufradRechte2_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte2_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte2_, panzerScale_, compoundLaufradRechte2_);
        modelLaufradRechte2_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte2_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte2_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte2_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte2_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte2_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte2_.setBoundingBox(bbLaufradRechte2_);
        //
        controlLaufradRechte2_ = new DefaultLaufradControl(modelLaufradRechte2_);
        nodeLaufradRechte2_.addControl(controlLaufradRechte2_);
        controlLaufradRechte2_.setSpatial(nodeLaufradRechte2_);
        
        // Laufrad Rechte 3
        nodeLaufradRechte3_ = new Node("LaufradRechte3");
        sptlLaufradRechte3_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte3.mesh.j3o");
        nodeLaufradRechte3_.attachChild(sptlLaufradRechte3_);
        geomLaufradRechte3_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte3_, "LaufradRechte3");
        bbLaufradRechte3_ = (BoundingBox) geomLaufradRechte3_.getModelBound();
        centerLaufradRechte3_ = bbLaufradRechte3_.getCenter();
        cshLaufradRechte3_ = new HullCollisionShape(geomLaufradRechte3_.getMesh());
        compoundLaufradRechte3_ = new CompoundCollisionShape();
        compoundLaufradRechte3_.addChildShape(
            cshLaufradRechte3_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte3_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte3_, panzerScale_, compoundLaufradRechte3_);
        modelLaufradRechte3_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte3_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte3_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte3_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte3_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte3_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte3_.setBoundingBox(bbLaufradRechte3_);
        //
        controlLaufradRechte3_ = new DefaultLaufradControl(modelLaufradRechte3_);
        nodeLaufradRechte3_.addControl(controlLaufradRechte3_);
        controlLaufradRechte3_.setSpatial(nodeLaufradRechte3_);

        // Laufrad Rechte 4
        nodeLaufradRechte4_ = new Node("LaufradRechte4");
        sptlLaufradRechte4_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte4.mesh.j3o");
        nodeLaufradRechte4_.attachChild(sptlLaufradRechte4_);
        geomLaufradRechte4_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte4_, "LaufradRechte4");
        bbLaufradRechte4_ = (BoundingBox) geomLaufradRechte4_.getModelBound();
        centerLaufradRechte4_ = bbLaufradRechte4_.getCenter();
        cshLaufradRechte4_ = new HullCollisionShape(geomLaufradRechte4_.getMesh());
        compoundLaufradRechte4_ = new CompoundCollisionShape();
        compoundLaufradRechte4_.addChildShape(
            cshLaufradRechte4_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte4_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte4_, panzerScale_, compoundLaufradRechte4_);
        modelLaufradRechte4_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte4_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte4_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte4_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte4_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte4_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte4_.setBoundingBox(bbLaufradRechte4_);
        //
        controlLaufradRechte4_ = new DefaultLaufradControl(modelLaufradRechte4_);
        nodeLaufradRechte4_.addControl(controlLaufradRechte4_);
        controlLaufradRechte4_.setSpatial(nodeLaufradRechte4_);

        // Laufrad Rechte 5
        nodeLaufradRechte5_ = new Node("LaufradRechte5");
        sptlLaufradRechte5_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte5.mesh.j3o");
        nodeLaufradRechte5_.attachChild(sptlLaufradRechte5_);
        geomLaufradRechte5_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte5_, "LaufradRechte5");
        bbLaufradRechte5_ = (BoundingBox) geomLaufradRechte5_.getModelBound();
        centerLaufradRechte5_ = bbLaufradRechte5_.getCenter();
        cshLaufradRechte5_ = new HullCollisionShape(geomLaufradRechte5_.getMesh());
        compoundLaufradRechte5_ = new CompoundCollisionShape();
        compoundLaufradRechte5_.addChildShape(
            cshLaufradRechte5_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte5_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte5_, panzerScale_, compoundLaufradRechte5_);
        modelLaufradRechte5_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte5_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte5_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte5_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte5_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte5_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte5_.setBoundingBox(bbLaufradRechte5_);
        //
        controlLaufradRechte5_ = new DefaultLaufradControl(modelLaufradRechte5_);
        nodeLaufradRechte5_.addControl(controlLaufradRechte5_);
        controlLaufradRechte5_.setSpatial(nodeLaufradRechte5_);

        // Laufrad Rechte 6
        nodeLaufradRechte6_ = new Node("LaufradRechte6");
        sptlLaufradRechte6_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte6.mesh.j3o");
        nodeLaufradRechte6_.attachChild(sptlLaufradRechte6_);
        geomLaufradRechte6_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte6_, "LaufradRechte6");
        bbLaufradRechte6_ = (BoundingBox) geomLaufradRechte6_.getModelBound();
        centerLaufradRechte6_ = bbLaufradRechte6_.getCenter();
        cshLaufradRechte6_ = new HullCollisionShape(geomLaufradRechte6_.getMesh());
        compoundLaufradRechte6_ = new CompoundCollisionShape();
        compoundLaufradRechte6_.addChildShape(
            cshLaufradRechte6_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte6_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte6_, panzerScale_, compoundLaufradRechte6_);
        modelLaufradRechte6_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte6_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte6_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte6_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte6_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte6_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte6_.setBoundingBox(bbLaufradRechte6_);
        //
        controlLaufradRechte6_ = new DefaultLaufradControl(modelLaufradRechte6_);
        nodeLaufradRechte6_.addControl(controlLaufradRechte6_);
        controlLaufradRechte6_.setSpatial(nodeLaufradRechte6_);

        // Laufrad Rechte 7
        nodeLaufradRechte7_ = new Node("LaufradRechte7");
        sptlLaufradRechte7_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte7.mesh.j3o");
        nodeLaufradRechte7_.attachChild(sptlLaufradRechte7_);
        geomLaufradRechte7_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte7_, "LaufradRechte7");
        bbLaufradRechte7_ = (BoundingBox) geomLaufradRechte7_.getModelBound();
        centerLaufradRechte7_ = bbLaufradRechte7_.getCenter();
        cshLaufradRechte7_ = new HullCollisionShape(geomLaufradRechte7_.getMesh());
        compoundLaufradRechte7_ = new CompoundCollisionShape();
        compoundLaufradRechte7_.addChildShape(
            cshLaufradRechte7_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte7_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte7_, panzerScale_, compoundLaufradRechte7_);
        modelLaufradRechte7_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte7_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte7_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte7_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte7_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte7_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte7_.setBoundingBox(bbLaufradRechte7_);
        //
        controlLaufradRechte7_ = new DefaultLaufradControl(modelLaufradRechte7_);
        nodeLaufradRechte7_.addControl(controlLaufradRechte7_);
        controlLaufradRechte7_.setSpatial(nodeLaufradRechte7_);
        
        // Laufrad Rechte 8
        nodeLaufradRechte8_ = new Node("LaufradRechte8");
        sptlLaufradRechte8_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LaufradRechte8.mesh.j3o");
        nodeLaufradRechte8_.attachChild(sptlLaufradRechte8_);
        geomLaufradRechte8_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLaufradRechte8_, "LaufradRechte8");
        bbLaufradRechte8_ = (BoundingBox) geomLaufradRechte8_.getModelBound();
        centerLaufradRechte8_ = bbLaufradRechte8_.getCenter();
        cshLaufradRechte8_ = new HullCollisionShape(geomLaufradRechte8_.getMesh());
        compoundLaufradRechte8_ = new CompoundCollisionShape();
        compoundLaufradRechte8_.addChildShape(
            cshLaufradRechte8_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLaufradRechte8_ = new DefaultPanzerungModel(
            this, nodeLaufradRechte8_, panzerScale_, compoundLaufradRechte8_);
        modelLaufradRechte8_.setFrontArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte8_.setRearArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte8_.setLeftArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte8_.setRightArmoring(LAUFRAD_SIDE_ARMORING_VALUE_);
        modelLaufradRechte8_.setTopArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte8_.setBottomArmoring(LAUFRAD_FRTB_ARMORING_VALUE_);
        modelLaufradRechte8_.setBoundingBox(bbLaufradRechte8_);
        //
        controlLaufradRechte8_ = new DefaultLaufradControl(modelLaufradRechte8_);
        nodeLaufradRechte8_.addControl(controlLaufradRechte8_);
        controlLaufradRechte8_.setSpatial(nodeLaufradRechte8_);
        
        // Leitrad Rechte
        nodeLeitradRechte_ = new Node("LeitradRechte");
        sptlLeitradRechte_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/LeitradRechte.mesh.j3o");
        nodeLeitradRechte_.attachChild(sptlLeitradRechte_);
        geomLeitradRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlLeitradRechte_, "LeitradRechte");
        bbLeitradRechte_ = (BoundingBox) geomLeitradRechte_.getModelBound();
        centerLeitradRechte_ = bbLeitradRechte_.getCenter();
        cshLeitradRechte_ = new HullCollisionShape(geomLeitradRechte_.getMesh());
        compoundLeitradRechte_ = new CompoundCollisionShape();
        compoundLeitradRechte_.addChildShape(
            cshLeitradRechte_, new Vector3f(0f, 0f, 0f)); // Manual translation
        //
        modelLeitradRechte_ = new DefaultPanzerungModel(
            this, nodeLeitradRechte_, panzerScale_, compoundLeitradRechte_);
        modelLeitradRechte_.setFrontArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradRechte_.setRearArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradRechte_.setLeftArmoring(LEITRAD_SIDE_ARMORING_VALUE_);
        modelLeitradRechte_.setRightArmoring(LEITRAD_SIDE_ARMORING_VALUE_);
        modelLeitradRechte_.setTopArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradRechte_.setBottomArmoring(LEITRAD_FRTB_ARMORING_VALUE_);
        modelLeitradRechte_.setBoundingBox(bbLeitradRechte_);
        //
        controlLeitradRechte_ = new DefaultLeitradControl(modelLeitradRechte_);
        nodeLeitradRechte_.addControl(controlLeitradRechte_);
        controlLeitradRechte_.setSpatial(nodeLeitradRechte_);

        // Fahrersehklappe
        nodeFahrersehklappe_ = new Node("Fahrersehklappe");
        sptlFahrersehklappe_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Fahrersehklappe.mesh.j3o");
        nodeFahrersehklappe_.attachChild(sptlFahrersehklappe_);
        geomFahrersehklappe_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlFahrersehklappe_, "Fahrersehklappe");
        bbFahrersehklappe_ = (BoundingBox) geomFahrersehklappe_.getModelBound();
        centerFahrersehklappe_ = bbFahrersehklappe_.getCenter();
        geomcshFahrersehklappe_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Fahrersehklappe_CollisionShape.mesh.j3o"),
            "Fahrersehklappe_CollisionShape");
        bbcshFahrersehklappe_ = (BoundingBox) geomcshFahrersehklappe_.getModelBound();
        centercshFahrersehklappe_ = bbcshFahrersehklappe_.getCenter();
        cshFahrersehklappe_ = new HullCollisionShape(geomcshFahrersehklappe_.getMesh());
        compoundFahrersehklappe_ = new CompoundCollisionShape();
        compoundFahrersehklappe_.addChildShape(cshFahrersehklappe_, new Vector3f(
            centerFahrersehklappe_.getX() - centercshFahrersehklappe_.getX(), 
            centerFahrersehklappe_.getY() - centercshFahrersehklappe_.getY(), 
            centerFahrersehklappe_.getZ() - centercshFahrersehklappe_.getZ()));
        //
        modelFahrersehklappe_ = new DefaultPanzerungModel(
            this, nodeFahrersehklappe_, panzerScale_, compoundFahrersehklappe_);
        modelFahrersehklappe_.setFrontArmoring(FAHRERSEHKLAPPE_ARMORING_VALUE_);
        modelFahrersehklappe_.setRearArmoring(FAHRERSEHKLAPPE_ARMORING_VALUE_);
        modelFahrersehklappe_.setBoundingBox(bbFahrersehklappe_);
        //
        controlFahrersehklappe_ = new DefaultKlappeControl(modelFahrersehklappe_);
        nodeFahrersehklappe_.addControl(controlFahrersehklappe_);
        controlFahrersehklappe_.setSpatial(nodeFahrersehklappe_);
        
        // Maschinengewehrklappe
        nodeMGklappe_ = new Node("Maschinengewehrklappe");
        sptlMGklappe_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Maschinengewehrklappe.mesh.j3o");
        nodeMGklappe_.attachChild(sptlMGklappe_);
        geomMGklappe_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlMGklappe_, "Maschinengewehrklappe");
        bbMGklappe_ = (BoundingBox) geomMGklappe_.getModelBound();
        centerMGklappe_ = bbMGklappe_.getCenter();
        geomcshMGklappe_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Maschinengewehrklappe_CollisionShape.mesh.j3o"),
            "Maschinengewehrklappe_CollisionShape");
        bbcshMGklappe_ = (BoundingBox) geomcshMGklappe_.getModelBound();
        centercshMGklappe_ = bbcshMGklappe_.getCenter();
        cshMGklappe_ = new HullCollisionShape(geomcshMGklappe_.getMesh());
        compoundMGklappe_ = new CompoundCollisionShape();
        compoundMGklappe_.addChildShape(cshMGklappe_, new Vector3f(
            centerMGklappe_.getX() - centercshMGklappe_.getX(), 
            centerMGklappe_.getY() - centercshMGklappe_.getY(), 
            centerMGklappe_.getZ() - centercshMGklappe_.getZ()));
        //
        modelMGklappe_ = new DefaultPanzerungModel(
            this, nodeMGklappe_, panzerScale_, compoundMGklappe_);
        modelMGklappe_.setFrontArmoring(MGKLAPPE_ARMORING_VALUE_);
        modelMGklappe_.setRearArmoring(MGKLAPPE_ARMORING_VALUE_);
        modelMGklappe_.setBoundingBox(bbMGklappe_);
        //
        controlMGklappe_ = new DefaultKlappeControl(modelMGklappe_);
        nodeMGklappe_.addControl(controlMGklappe_);
        controlMGklappe_.setSpatial(nodeMGklappe_);
        
        // Kettenabdeckung Linke
        nodeKettenabdeckungLinke_ = new Node("KettenabdeckungLinke");
        sptlKettenabdeckungLinke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/KettenabdeckungLinke.mesh.j3o");
        nodeKettenabdeckungLinke_.attachChild(sptlKettenabdeckungLinke_);
        geomKettenabdeckungLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlKettenabdeckungLinke_, "KettenabdeckungLinke");
        bbKettenabdeckungLinke_ = (BoundingBox) geomKettenabdeckungLinke_.getModelBound();
        centerKettenabdeckungLinke_ = bbKettenabdeckungLinke_.getCenter();
        geomcshKettenabdeckungLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/KettenabdeckungLinke_CollisionShape.mesh.j3o"),
            "KettenabdeckungLinke_CollisionShape");
        bbcshKettenabdeckungLinke_ = (BoundingBox) geomcshKettenabdeckungLinke_.getModelBound();
        centercshKettenabdeckungLinke_ = bbcshKettenabdeckungLinke_.getCenter();
        cshKettenabdeckungLinke_ = new HullCollisionShape(geomcshKettenabdeckungLinke_.getMesh());
        compoundKettenabdeckungLinke_ = new CompoundCollisionShape();
        compoundKettenabdeckungLinke_.addChildShape(cshKettenabdeckungLinke_, new Vector3f(
            centerKettenabdeckungLinke_.getX() - centercshKettenabdeckungLinke_.getX(), 
            centerKettenabdeckungLinke_.getY() - centercshKettenabdeckungLinke_.getY(), 
            centerKettenabdeckungLinke_.getZ() - centercshKettenabdeckungLinke_.getZ()));
        //
        modelKettenabdeckungLinke_ = new DefaultPanzerungModel(
            this, nodeKettenabdeckungLinke_, panzerScale_, compoundKettenabdeckungLinke_);
        modelKettenabdeckungLinke_.setFrontArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungLinke_.setRearArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungLinke_.setLeftArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungLinke_.setRightArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungLinke_.setTopArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungLinke_.setBottomArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungLinke_.setBoundingBox(bbKettenabdeckungLinke_);
        //
        controlKettenabdeckungLinke_ = new DefaultAbdeckungControl(modelKettenabdeckungLinke_);
        nodeKettenabdeckungLinke_.addControl(controlKettenabdeckungLinke_);
        controlKettenabdeckungLinke_.setSpatial(nodeKettenabdeckungLinke_);

        // Kettenabdeckung Rechte
        nodeKettenabdeckungRechte_ = new Node("KettenabdeckungRechte");
        sptlKettenabdeckungRechte_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/KettenabdeckungRechte.mesh.j3o");
        nodeKettenabdeckungRechte_.attachChild(sptlKettenabdeckungRechte_);
        geomKettenabdeckungRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlKettenabdeckungRechte_, "KettenabdeckungRechte");
        bbKettenabdeckungRechte_ = (BoundingBox) geomKettenabdeckungRechte_.getModelBound();
        centerKettenabdeckungRechte_ = bbKettenabdeckungRechte_.getCenter();
        geomcshKettenabdeckungRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/KettenabdeckungRechte_CollisionShape.mesh.j3o"),
            "KettenabdeckungRechte_CollisionShape");
        bbcshKettenabdeckungRechte_ = (BoundingBox) geomcshKettenabdeckungRechte_.getModelBound();
        centercshKettenabdeckungRechte_ = bbcshKettenabdeckungRechte_.getCenter();
        cshKettenabdeckungRechte_ = new HullCollisionShape(geomcshKettenabdeckungRechte_.getMesh());
        compoundKettenabdeckungRechte_ = new CompoundCollisionShape();
        compoundKettenabdeckungRechte_.addChildShape(cshKettenabdeckungRechte_, new Vector3f(
            centerKettenabdeckungRechte_.getX() - centercshKettenabdeckungRechte_.getX(), 
            centerKettenabdeckungRechte_.getY() - centercshKettenabdeckungRechte_.getY(), 
            centerKettenabdeckungRechte_.getZ() - centercshKettenabdeckungRechte_.getZ()));
        //
        modelKettenabdeckungRechte_ = new DefaultPanzerungModel(
            this, nodeKettenabdeckungRechte_, panzerScale_, compoundKettenabdeckungRechte_);
        modelKettenabdeckungRechte_.setFrontArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungRechte_.setRearArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungRechte_.setLeftArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungRechte_.setRightArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungRechte_.setTopArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungRechte_.setBottomArmoring(KETTENABDECKUNG_ARMORING_VALUE_);
        modelKettenabdeckungRechte_.setBoundingBox(bbKettenabdeckungRechte_);
        //
        controlKettenabdeckungRechte_ = new DefaultAbdeckungControl(modelKettenabdeckungRechte_);
        nodeKettenabdeckungRechte_.addControl(controlKettenabdeckungRechte_);
        controlKettenabdeckungRechte_.setSpatial(nodeKettenabdeckungRechte_);

        // Gepaeckkasten Linke
        nodeGepaeckkastenLinke_ = new Node("GepaeckkastenLinke");
        sptlGepaeckkastenLinke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/GepaeckkastenLinke.mesh.j3o");
        nodeGepaeckkastenLinke_.attachChild(sptlGepaeckkastenLinke_);
        geomGepaeckkastenLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlGepaeckkastenLinke_, "GepaeckkastenLinke");
        bbGepaeckkastenLinke_ = (BoundingBox) geomGepaeckkastenLinke_.getModelBound();
        centerGepaeckkastenLinke_ = bbGepaeckkastenLinke_.getCenter();
        geomcshGepaeckkastenLinke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/GepaeckkastenLinke_CollisionShape.mesh.j3o"),
            "GepaeckkastenLinke_CollisionShape");
        bbcshGepaeckkastenLinke_ = (BoundingBox) geomcshGepaeckkastenLinke_.getModelBound();
        centercshGepaeckkastenLinke_ = bbcshGepaeckkastenLinke_.getCenter();
        cshGepaeckkastenLinke_ = new HullCollisionShape(geomcshGepaeckkastenLinke_.getMesh());
        compoundGepaeckkastenLinke_ = new CompoundCollisionShape();
        compoundGepaeckkastenLinke_.addChildShape(cshGepaeckkastenLinke_, new Vector3f(
            centerGepaeckkastenLinke_.getX() - centercshGepaeckkastenLinke_.getX(), 
            centerGepaeckkastenLinke_.getY() - centercshGepaeckkastenLinke_.getY(), 
            centerGepaeckkastenLinke_.getZ() - centercshGepaeckkastenLinke_.getZ()));
        //
        modelGepaeckkastenLinke_ = new DefaultPanzerungModel(
            this, nodeGepaeckkastenLinke_, panzerScale_, compoundGepaeckkastenLinke_);
        modelGepaeckkastenLinke_.setFrontArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenLinke_.setRearArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenLinke_.setLeftArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenLinke_.setRightArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenLinke_.setTopArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenLinke_.setBottomArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenLinke_.setBoundingBox(bbGepaeckkastenLinke_);
        //
        controlGepaeckkastenLinke_ = new DefaultKasteControl(modelGepaeckkastenLinke_);
        nodeGepaeckkastenLinke_.addControl(controlGepaeckkastenLinke_);
        controlGepaeckkastenLinke_.setSpatial(nodeGepaeckkastenLinke_);

        // Gepaeckkasten Rechte
        nodeGepaeckkastenRechte_ = new Node("GepaeckkastenRechte");
        sptlGepaeckkastenRechte_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/GepaeckkastenRechte.mesh.j3o");
        nodeGepaeckkastenRechte_.attachChild(sptlGepaeckkastenRechte_);
        geomGepaeckkastenRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlGepaeckkastenRechte_, "GepaeckkastenRechte");
        bbGepaeckkastenRechte_ = (BoundingBox) geomGepaeckkastenRechte_.getModelBound();
        centerGepaeckkastenRechte_ = bbGepaeckkastenRechte_.getCenter();
        geomcshGepaeckkastenRechte_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/GepaeckkastenRechte_CollisionShape.mesh.j3o"),
            "GepaeckkastenRechte_CollisionShape");
        bbcshGepaeckkastenRechte_ = (BoundingBox) geomcshGepaeckkastenRechte_.getModelBound();
        centercshGepaeckkastenRechte_ = bbcshGepaeckkastenRechte_.getCenter();
        cshGepaeckkastenRechte_ = new HullCollisionShape(geomcshGepaeckkastenRechte_.getMesh());
        compoundGepaeckkastenRechte_ = new CompoundCollisionShape();
        compoundGepaeckkastenRechte_.addChildShape(cshGepaeckkastenRechte_, new Vector3f(
            centerGepaeckkastenRechte_.getX() - centercshGepaeckkastenRechte_.getX(), 
            centerGepaeckkastenRechte_.getY() - centercshGepaeckkastenRechte_.getY(), 
            centerGepaeckkastenRechte_.getZ() - centercshGepaeckkastenRechte_.getZ()));
        //
        modelGepaeckkastenRechte_ = new DefaultPanzerungModel(
            this, nodeGepaeckkastenRechte_, panzerScale_, compoundGepaeckkastenRechte_);
        modelGepaeckkastenRechte_.setFrontArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenRechte_.setRearArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenRechte_.setLeftArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenRechte_.setRightArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenRechte_.setTopArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenRechte_.setBottomArmoring(GEPAECKKASTEN_ARMORING_VALUE_);
        modelGepaeckkastenRechte_.setBoundingBox(bbGepaeckkastenRechte_);
        //
        controlGepaeckkastenRechte_ = new DefaultKasteControl(modelGepaeckkastenRechte_);
        nodeGepaeckkastenRechte_.addControl(controlGepaeckkastenRechte_);
        controlGepaeckkastenRechte_.setSpatial(nodeGepaeckkastenRechte_);
        
        // Kommandantenkuppel
        nodeKommandantenkuppel_ = new Node("Kommandantenkuppel");
        sptlKommandantenkuppel_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Kommandantenkuppel.mesh.j3o");
        nodeKommandantenkuppel_.attachChild(sptlKommandantenkuppel_);
        geomKommandantenkuppel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlKommandantenkuppel_, "Kommandantenkuppel");
        bbKommandantenkuppel_ = (BoundingBox) geomKommandantenkuppel_.getModelBound();
        centerKommandantenkuppel_ = bbKommandantenkuppel_.getCenter();
        geomcshKommandantenkuppel_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Kommandantenkuppel_CollisionShape.mesh.j3o"),
            "Kommandantenkuppel_CollisionShape");
        bbcshKommandantenkuppel_ = (BoundingBox) geomcshKommandantenkuppel_.getModelBound();
        centercshKommandantenkuppel_ = bbcshKommandantenkuppel_.getCenter();
        cshKommandantenkuppel_ = new HullCollisionShape(geomcshKommandantenkuppel_.getMesh());
        compoundKommandantenkuppel_ = new CompoundCollisionShape();
        compoundKommandantenkuppel_.addChildShape(cshKommandantenkuppel_, new Vector3f(
            centerKommandantenkuppel_.getX() - centercshKommandantenkuppel_.getX(), 
            centerKommandantenkuppel_.getY() - centercshKommandantenkuppel_.getY(), 
            centerKommandantenkuppel_.getZ() - centercshKommandantenkuppel_.getZ()));
        //
        modelKommandantenkuppel_ = new DefaultPanzerungModel(
            this, nodeKommandantenkuppel_, panzerScale_, compoundKommandantenkuppel_);
        modelKommandantenkuppel_.setFrontArmoring(KOMMANDANTENKUPPEL_SIDE_ARMORING_VALUE_);
        modelKommandantenkuppel_.setRearArmoring(KOMMANDANTENKUPPEL_SIDE_ARMORING_VALUE_);
        modelKommandantenkuppel_.setLeftArmoring(KOMMANDANTENKUPPEL_SIDE_ARMORING_VALUE_);
        modelKommandantenkuppel_.setRightArmoring(KOMMANDANTENKUPPEL_SIDE_ARMORING_VALUE_);
        modelKommandantenkuppel_.setTopArmoring(KOMMANDANTENKUPPEL_TOP_ARMORING_VALUE_);
        modelKommandantenkuppel_.setBoundingBox(bbKommandantenkuppel_);
        //
        controlKommandantenkuppel_ = new DefaultKommandantenkuppelControl(modelKommandantenkuppel_);
        nodeKommandantenkuppel_.addControl(controlKommandantenkuppel_);
        controlKommandantenkuppel_.setSpatial(nodeKommandantenkuppel_);
        
        // Notausstiegluke
        nodeNotausstiegluke_ = new Node("Notausstiegluke");
        sptlNotausstiegluke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Notausstiegluke.mesh.j3o");
        nodeNotausstiegluke_.attachChild(sptlNotausstiegluke_);
        geomNotausstiegluke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlNotausstiegluke_, "Notausstiegluke");
        bbNotausstiegluke_ = (BoundingBox) geomNotausstiegluke_.getModelBound();
        centerNotausstiegluke_ = bbNotausstiegluke_.getCenter();
        geomcshNotausstiegluke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Notausstiegluke_CollisionShape.mesh.j3o"),
            "Notausstiegluke_CollisionShape");
        bbcshNotausstiegluke_ = (BoundingBox) geomcshNotausstiegluke_.getModelBound();
        centercshNotausstiegluke_ = bbcshNotausstiegluke_.getCenter();
        cshNotausstiegluke_ = new HullCollisionShape(geomcshNotausstiegluke_.getMesh());
        compoundNotausstiegluke_ = new CompoundCollisionShape();
        compoundNotausstiegluke_.addChildShape(cshNotausstiegluke_, new Vector3f(
            centerNotausstiegluke_.getX() - centercshNotausstiegluke_.getX(), 
            centerNotausstiegluke_.getY() - centercshNotausstiegluke_.getY(),
            centerNotausstiegluke_.getZ() - centercshNotausstiegluke_.getZ()));
        //
        modelNotausstiegluke_ = new DefaultPanzerungModel(
            this, nodeNotausstiegluke_, panzerScale_, compoundNotausstiegluke_);
        modelNotausstiegluke_.setFrontArmoring(NOTAUSSTIEGLUKE_ARMORING_VALUE_);
        modelNotausstiegluke_.setRearArmoring(NOTAUSSTIEGLUKE_ARMORING_VALUE_);
        modelNotausstiegluke_.setBoundingBox(bbNotausstiegluke_);
        //
        controlNotausstiegluke_ = new DefaultLukeControl(modelNotausstiegluke_);
        nodeNotausstiegluke_.addControl(controlNotausstiegluke_);
        controlNotausstiegluke_.setSpatial(nodeNotausstiegluke_);

        // Turmluke
        nodeTurmluke_ = new Node("Turmluke");
        sptlTurmluke_ = assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/LPModel/Turmluke.mesh.j3o");
        nodeTurmluke_.attachChild(sptlTurmluke_);
        geomTurmluke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            sptlTurmluke_, "Turmluke");
        bbTurmluke_ = (BoundingBox) geomTurmluke_.getModelBound();
        centerTurmluke_ = bbTurmluke_.getCenter();
        geomcshTurmluke_ = DgaJmeUtils.getGeometryFromSpatialByName(
            assetManager_.loadModel(
            "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPantherAusfD_FP/CSModel/Turmluke_CollisionShape.mesh.j3o"),
            "Turmluke_CollisionShape");
        bbcshTurmluke_ = (BoundingBox) geomcshTurmluke_.getModelBound();
        centercshTurmluke_ = bbcshTurmluke_.getCenter();
        cshTurmluke_ = new HullCollisionShape(geomcshTurmluke_.getMesh());
        compoundTurmluke_ = new CompoundCollisionShape();
        compoundTurmluke_.addChildShape(cshTurmluke_, new Vector3f(
            centerTurmluke_.getX() - centercshTurmluke_.getX(), 
            centerTurmluke_.getY() - centercshTurmluke_.getY(), 
            centerTurmluke_.getZ() - centercshTurmluke_.getZ()));
        //
        modelTurmluke_ = new DefaultPanzerungModel(
            this, nodeTurmluke_, panzerScale_, compoundTurmluke_);
        modelTurmluke_.setTopArmoring(TURMLUKE_ARMORING_VALUE_);
        modelTurmluke_.setBottomArmoring(TURMLUKE_ARMORING_VALUE_);
        modelTurmluke_.setBoundingBox(bbTurmluke_);
        //
        controlTurmluke_ = new DefaultLukeControl(modelTurmluke_);
        nodeTurmluke_.addControl(controlTurmluke_);
        controlTurmluke_.setSpatial(nodeTurmluke_);
        
        // Engine effect
        engineEffect_ = super.getEngineEffect();
        nodeEngineEffect_ = engineEffect_.getNode();
        peExhaustLeft_ = engineEffect_.getExhaustOne();
        peExhaustLeft_.setLocalTranslation(35.0f, 190.0f, -400.0f); // Manual translation
        peExhaustRight_ = engineEffect_.getExhaustTwo();
        peExhaustRight_.setLocalTranslation(-35.0f, 190.0f, -400.0f); // Manual translation
        peExhaustBlastLeft_ = engineEffect_.getBlastOne();
        peExhaustBlastLeft_.setLocalTranslation(35.0f, 190.0f, -430.0f); // Manual translation
        peExhaustBlastRight_ = engineEffect_.getBlastTwo();
        peExhaustBlastRight_.setLocalTranslation(-35.0f, 190.0f, -430.0f); // Manual translation
        anodePzStanding_ = engineEffect_.getStandingSound();
        anodePzRunning_ = engineEffect_.getRunningSound();

        //
        nodeTurmkanonePivot_.setLocalTranslation(
            0f, centerTurmkanonenmaskeModel_.getY() - centerTurmkanone_.getY(), 0f);
        nodeTurmkanonePivot_.attachChild(nodeTurmkanonenmaske_);
        nodeTurmkanonePivot_.attachChild(nodeTurmkanone_);
        nodeTurmkanonePivot_.attachChild(nodeTurmMG_);
        nodeTurmkanonePivot_.attachChild(nodeTurmkanonenFrPkt_);
        nodeTurmkanonePivot_.attachChild(nodeTurmMGFrPkt_);
        nodeTurmkanonePivot_.attachChild(nodeTurmkanonenEffPkt_);
        nodeTurmkanonePivot_.attachChild(nodeTurmMGEffPkt_);
        nodeTurmPivot_.attachChild(nodeTurmkanonePivot_);
        nodeTurmPivot_.attachChild(nodeTurm_);
        nodeTurmPivot_.attachChild(nodeKommandantenkuppel_);
        nodeTurmPivot_.attachChild(nodeNotausstiegluke_);
        nodeTurmPivot_.attachChild(nodeTurmluke_);
        nodeTurmPivot_.attachChild(nodeTurmvorpunkt_);
        nodePanzer_.attachChild(nodeWanne_);
        nodePanzer_.attachChild(nodeFahrersehklappe_);
        nodePanzer_.attachChild(nodeMGklappe_);
        nodePanzer_.attachChild(nodeKettenabdeckungLinke_);
        nodePanzer_.attachChild(nodeKettenabdeckungRechte_);
        nodePanzer_.attachChild(nodeGepaeckkastenLinke_);
        nodePanzer_.attachChild(nodeGepaeckkastenRechte_);
        nodePanzer_.attachChild(nodePanzerwannenvorpunkt_);
        nodePanzer_.attachChild(nodeTurmPivot_);
        nodePanzer_.attachChild(nodeGleisketteLinke_);
        nodePanzer_.attachChild(nodeGleisketteRechte_);
        nodePanzer_.attachChild(nodeAntriebsradLinke_);
        nodePanzer_.attachChild(nodeLaufradLinke1_);
        nodePanzer_.attachChild(nodeLaufradLinke2_);
        nodePanzer_.attachChild(nodeLaufradLinke3_);
        nodePanzer_.attachChild(nodeLaufradLinke4_);
        nodePanzer_.attachChild(nodeLaufradLinke5_);
        nodePanzer_.attachChild(nodeLaufradLinke6_);
        nodePanzer_.attachChild(nodeLaufradLinke7_);
        nodePanzer_.attachChild(nodeLaufradLinke8_);
        nodePanzer_.attachChild(nodeLeitradLinke_);
        nodePanzer_.attachChild(nodeAntriebsradRechte_);
        nodePanzer_.attachChild(nodeLaufradRechte1_);
        nodePanzer_.attachChild(nodeLaufradRechte2_);
        nodePanzer_.attachChild(nodeLaufradRechte3_);
        nodePanzer_.attachChild(nodeLaufradRechte4_);
        nodePanzer_.attachChild(nodeLaufradRechte5_);
        nodePanzer_.attachChild(nodeLaufradRechte6_);
        nodePanzer_.attachChild(nodeLaufradRechte7_);
        nodePanzer_.attachChild(nodeLaufradRechte8_);
        nodePanzer_.attachChild(nodeLeitradRechte_);
        nodePanzer_.attachChild(nodeEngineEffect_);
        // Stop timer
        final long buildStop_ = System.currentTimeMillis();
        final float buildTime_ = ((float) (buildStop_ - buildStart_)) / 1000.0f;
        System.out.println("Panzer build time: " + buildTime_ + " sec." );
    }
    
    /**
     * The method initializes the armoured fighting carriage's target cursor.
     */
    @Override
    public void initTargetCursor() {
        if (modelTC_ != null) {
            geomTC_ = modelTC_.getGeometry();
        } else {
            quadTC_ = new Quad(64.0f, 64.0f);
            geomTC_ = new Geometry("TargetGeom", quadTC_);
            texTC_ = assetManager_.loadTexture(
                "Models/Kriegsteilnehmer/Kampfpanzer/DEU/PzKpfwVPanther/Cursors/PantherTargetGreen.png");
            matTC_ = new Material(assetManager_, "Common/MatDefs/Misc/Unshaded.j3md");
            matTC_.setTexture("ColorMap", texTC_);
            matTC_.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);  // Activate transparency
            geomTC_.setQueueBucket(RenderQueue.Bucket.Transparent);
            geomTC_.setMaterial(matTC_);
            modelTC_ = new DefaultPanzerwagenTargetCursorModel(
                geomTC_, DefaultDgaJmeScale.KOELN_TARGET_CURSOR_SCALE);
        }
        bbTC_ = (BoundingBox) geomTC_.getModelBound();
        centerTC_ = bbTC_.getCenter();
        geomTC_.setLocalTranslation(new Vector3f(
            centerTurmkanone_.getX() + centerTC_.getX(),
            centerTurmkanone_.getY() - centerTC_.getY(),
            centerTurmkanone_.getZ() * TARGET_OFFSET_));
        geomTC_.setLocalRotation(new Quaternion().fromAngleAxis(FastMath.PI, Vector3f.UNIT_Y));
        //
        modelTC_.setOffset(centerTurmkanone_.getZ() * TARGET_OFFSET_);
        modelTC_.setPivotPoint(nodeTurmkanonePivot_);
        controlTC_ = new DefaultPanzerwagenTargetCursor(modelTC_);
        controlTC_.setMaxVerticalAngle(TURMKANONE_MAX_ELEVATION_ANGLE_);
        controlTC_.setMinVerticalAngle(TURMKANONE_MAX_DEPRESSION_ANGLE_);
        geomTC_.addControl(controlTC_);
        controlTC_.setSpatial(geomTC_);
        nodeTurmkanonePivot_.attachChild(geomTC_);
    }
    
    /**
     * The method adds nodes, controls, and other required parts to the scene.
     */
    @Override
    public void addToScene() {
        nodeTurm_.addControl(this.getPanzerCamera());
        //
        // The jMonkeyEngine v.3.2 has a strange bug with scaling that appears
        // as scaling is applying twice for spatials however only once for collision 
        // shapes. The following block of code is the workaround for this bug.
        float s_ = panzerScale_.getFloatValue();
        Vector3f sv_ = new Vector3f(s_, s_, s_);
        controlPanzer_.getSpatial().scale(s_);
        controlPanzer_.getCollisionShape().setScale(sv_);
        controlWanne_.getCollisionShape().setScale(sv_);
        controlFahrersehklappe_.getCollisionShape().setScale(sv_);
        controlKettenabdeckungLinke_.getCollisionShape().setScale(sv_);
        controlKettenabdeckungRechte_.getCollisionShape().setScale(sv_);
        controlMGklappe_.getCollisionShape().setScale(sv_);
        controlGepaeckkastenLinke_.getCollisionShape().setScale(sv_);
        controlGepaeckkastenRechte_.getCollisionShape().setScale(sv_);
        controlTurm_.getCollisionShape().setScale(sv_);
        controlKommandantenkuppel_.getCollisionShape().setScale(sv_);
        controlNotausstiegluke_.getCollisionShape().setScale(sv_);
        controlTurmluke_.getCollisionShape().setScale(sv_);
        controlTurmkanonenmaske_.getCollisionShape().setScale(sv_);
        controlTurmkanone_.getCollisionShape().setScale(sv_);
        controlTurmMG_.getCollisionShape().setScale(sv_);
        controlGleisketteLinke_.getCollisionShape().setScale(sv_);
        controlGleisketteRechte_.getCollisionShape().setScale(sv_);
        controlAntriebsradLinke_.getCollisionShape().setScale(sv_);
        controlLaufradLinke1_.getCollisionShape().setScale(sv_);
        controlLaufradLinke2_.getCollisionShape().setScale(sv_);
        controlLaufradLinke3_.getCollisionShape().setScale(sv_);
        controlLaufradLinke4_.getCollisionShape().setScale(sv_);
        controlLaufradLinke5_.getCollisionShape().setScale(sv_);
        controlLaufradLinke6_.getCollisionShape().setScale(sv_);
        controlLaufradLinke7_.getCollisionShape().setScale(sv_);
        controlLaufradLinke8_.getCollisionShape().setScale(sv_);
        controlLeitradLinke_.getCollisionShape().setScale(sv_);
        controlAntriebsradRechte_.getCollisionShape().setScale(sv_);
        controlLaufradRechte1_.getCollisionShape().setScale(sv_);
        controlLaufradRechte2_.getCollisionShape().setScale(sv_);
        controlLaufradRechte3_.getCollisionShape().setScale(sv_);
        controlLaufradRechte4_.getCollisionShape().setScale(sv_);
        controlLaufradRechte5_.getCollisionShape().setScale(sv_);
        controlLaufradRechte6_.getCollisionShape().setScale(sv_);
        controlLaufradRechte7_.getCollisionShape().setScale(sv_);
        controlLaufradRechte8_.getCollisionShape().setScale(sv_);
        controlLeitradRechte_.getCollisionShape().setScale(sv_);
        //
        rootNode_.attachChild(nodePanzer_);
        physicsSpace_.add(controlPanzer_);
        physicsSpace_.add(controlWanne_);
        physicsSpace_.add(controlFahrersehklappe_);
        physicsSpace_.add(controlKettenabdeckungLinke_);
        physicsSpace_.add(controlKettenabdeckungRechte_);
        physicsSpace_.add(controlMGklappe_);
        physicsSpace_.add(controlGepaeckkastenLinke_);
        physicsSpace_.add(controlGepaeckkastenRechte_);
        physicsSpace_.add(controlTurm_);
        physicsSpace_.add(controlKommandantenkuppel_);
        physicsSpace_.add(controlNotausstiegluke_);
        physicsSpace_.add(controlTurmluke_);
        physicsSpace_.add(controlTurmkanonenmaske_);
        physicsSpace_.add(controlTurmkanone_);
        physicsSpace_.add(controlTurmMG_);
        physicsSpace_.add(controlGleisketteLinke_);
        physicsSpace_.add(controlGleisketteRechte_);
        physicsSpace_.add(controlAntriebsradLinke_);
        physicsSpace_.add(controlLaufradLinke1_);
        physicsSpace_.add(controlLaufradLinke2_);
        physicsSpace_.add(controlLaufradLinke3_);
        physicsSpace_.add(controlLaufradLinke4_);
        physicsSpace_.add(controlLaufradLinke5_);
        physicsSpace_.add(controlLaufradLinke6_);
        physicsSpace_.add(controlLaufradLinke7_);
        physicsSpace_.add(controlLaufradLinke8_);
        physicsSpace_.add(controlLeitradLinke_);
        physicsSpace_.add(controlAntriebsradRechte_);
        physicsSpace_.add(controlLaufradRechte1_);
        physicsSpace_.add(controlLaufradRechte2_);
        physicsSpace_.add(controlLaufradRechte3_);
        physicsSpace_.add(controlLaufradRechte4_);
        physicsSpace_.add(controlLaufradRechte5_);
        physicsSpace_.add(controlLaufradRechte6_);
        physicsSpace_.add(controlLaufradRechte7_);
        physicsSpace_.add(controlLaufradRechte8_);
        physicsSpace_.add(controlLeitradRechte_);
    }

    /**
     * The method is called when an input to which this listener is registered
     * to is invoked.
     *
     * @param binding The name of the mapping that was invoked.
     * @param isPressed True if the action is "pressed", false otherwise.
     * @param tpf The time per frame value.
     */
    @Override
    public void onAction(final String binding, final boolean isPressed, final float tpf) {
        if (binding.equals(PANZER_MOVE_FORWARD_KEY.getName())) {
            isPanzerMoveForward_ = isPressed;
            this.setIsOnCombat(true);
        } else if (binding.equals(PANZER_MOVE_BACKWARD_KEY.getName())) {
            isPanzerMoveBackward_ = isPressed;
            this.setIsOnCombat(true);
        } else if (binding.equals(PANZER_MOVE_BREAK_KEY.getName())) {
            isPanzerBreak_ = isPressed;
        } else if (binding.equals(PANZER_ROTATE_LEFT_KEY.getName())) {
            isPanzerRotateLeftward_ = isPressed;
            this.setIsOnCombat(true);
        } else if (binding.equals(PANZER_ROTATE_RIGHT_KEY.getName())) {
            isPanzerRotateRightward_ = isPressed;
            this.setIsOnCombat(true);
        } else if ((binding.equals(TURMKANONE_SHOOT_KEY.getName()) || 
                    binding.equals(TURMKANONE_SHOOT_MOUSE.getName())) & isPressed == true) {
            if (this.isOnCombat() == true) {
                controlTurmkanone_.fire();
            }
        }
    }

    /**
     * The method is called to update this application state. This method will
     * be called every render pass if the <code>AppState</code> is both attached
     * and enabled.
     *
     * @param tpf Time since the last call to update(), in seconds.
     */
    @Override
    public void update(final float tpf) {
        super.update(tpf);
        Vector3f modelDir_ = nodePanzer_.getWorldRotation().mult(Vector3f.UNIT_Z);
        MOVE_DIRECTION_.set(0, 0, 0);
        if (isPanzerMoveForward_ == true) {
            if (isStopForward_ == true) {
                MOVE_DIRECTION_.addLocal(modelDir_.mult(Vector3f.ZERO));
                isStopForward_ = false;
            } else {
                MOVE_DIRECTION_.addLocal(modelDir_.mult(PANZER_SPEED_));
            }
        } else if (isPanzerMoveBackward_ == true) {
            if (isStopBackward_ == true) {
                MOVE_DIRECTION_.addLocal(modelDir_.mult(Vector3f.ZERO));
                isStopBackward_ = false;
            } else {
                MOVE_DIRECTION_.addLocal(modelDir_.mult(PANZER_SPEED_).negate());
            }
        } else if (isPanzerBreak_ == true) {
            MOVE_DIRECTION_.addLocal(modelDir_.mult(Vector3f.ZERO));
        }
        nodePanzer_.move(
            tpf * MOVE_DIRECTION_.getX(),
            tpf * MOVE_DIRECTION_.getY(),
            tpf * MOVE_DIRECTION_.getZ()
        );
        if (isPanzerRotateLeftward_ == true) {
            if (isStopLeftward_ == false) {
                Quaternion rotateL_ = new Quaternion().fromAngleAxis(FastMath.PI * tpf, Vector3f.UNIT_Y);
                rotateL_.multLocal(VIEW_DIRECTION_);
                nodePanzer_.rotate(0, tpf * VIEW_DIRECTION_.getY(), 0);
            } else {
                isStopLeftward_ = false;
            }
        } else if (isPanzerRotateRightward_ == true) {
            if (isStopRightward_ == false) {
                Quaternion rotateR_ = new Quaternion().fromAngleAxis(-FastMath.PI * tpf, Vector3f.UNIT_Y);
                rotateR_.multLocal(VIEW_DIRECTION_);
                nodePanzer_.rotate(0, -tpf * VIEW_DIRECTION_.getY(), 0);
            } else {
                isStopRightward_ = false;
            }
        }
        this.charge(isPanzerMoveForward_ || isPanzerMoveBackward_ || 
            isPanzerRotateLeftward_ || isPanzerRotateRightward_);
    }

    /**
     * The method realizes the analog handler for mouse movement events. It is
     * assumed that we want horizontal movements to turn the character, while
     * vertical movements only make the camera rotate up or down.
     *
     * @param binding The name of the mapping that was invoked.
     * @param value Value of the axis, from 0 to 1.
     * @param tpf The time per frame value.
     */
    @Override
    public void onAnalog(final String binding, final float value, final float tpf) {
        if (this.isOnCombat() == true) {
            Quaternion turnQuat_ = null;
            Vector3f sightDir_ = null;
            if (binding.equals(PANZERTURM_TURN_LEFT_KEY.getName()) || 
                binding.equals(PANZERTURM_TURN_LEFT_MOUSE.getName())) {
                turnQuat_ = new Quaternion();
                turnQuat_.fromAngleAxis(TURM_ROTATION_SPEED_ * value, Vector3f.UNIT_Y);
                sightDir_ = turnQuat_.mult(controlTurm_.getViewDirection());
                controlTurm_.setViewDirection(sightDir_);
            } else if (binding.equals(PANZERTURM_TURN_RIGHT_KEY.getName()) || 
                       binding.equals(PANZERTURM_TURN_RIGHT_MOUSE.getName())) {
                turnQuat_ = new Quaternion();
                turnQuat_.fromAngleAxis(-TURM_ROTATION_SPEED_ * value, Vector3f.UNIT_Y);
                sightDir_ = turnQuat_.mult(controlTurm_.getViewDirection());
                controlTurm_.setViewDirection(sightDir_);
            } else if (binding.equals(TURMKANONE_TURN_UP_KEY.getName()) || 
                       binding.equals(TURMKANONE_TURN_UP_MOUSE.getName())) {
                controlTurmkanone_.setVerticalRotation(TURMKANONE_ROTATION_SPEED_ * value);
                controlTurmMG_.setVerticalRotation(TURMKANONE_ROTATION_SPEED_ * value);
                controlTC_.setVerticalRotation(TURMKANONE_ROTATION_SPEED_ * value);
            } else if (binding.equals(TURMKANONE_TURN_DOWN_KEY.getName()) || 
                       binding.equals(TURMKANONE_TURN_DOWN_MOUSE.getName())) {
                controlTurmkanone_.setVerticalRotation(-TURMKANONE_ROTATION_SPEED_ * value);
                controlTurmMG_.setVerticalRotation(-TURMKANONE_ROTATION_SPEED_ * value);
                controlTC_.setVerticalRotation(-TURMKANONE_ROTATION_SPEED_ * value);
            } else if (binding.equals(PANZERTURM_MG_SHOOT_KEY.getName())) {
                controlTurmMG_.fire();
            }
        }
    }
    
    /**
     * The methdo is called by {@link AppStateManager} when transitioning this
     * <code>AppState</code> from <i>terminating</i> to <i>detached</i>. This
     * method is called the following render pass after the <code>AppState</code> 
     * has been detached and is always called once and only once for each time
     * <code>initialize()</code> is called. Either when the <code>AppState</code>
     * is detached or when the application terminates (if it terminates normally).
     */
    @Override
    public void cleanup() {
        super.cleanup();
        anodePzStanding_.stop();
        anodePzRunning_.stop();
    }

    /**
     * The method returns the spatial of the model.
     * 
     * @return Spatial The spatial of the model.
     */
    @Override
    public Spatial getSpatial() {
        return nodePanzer_;
    }
    
    /**
     * The method returns the game character's collision shape.
     * 
     * @return CollisionShape The game character's collision shape.
     */
    @Override
    public CollisionShape getCollisionShape() {
        return compoundPanzer_;
    }
    
    /**
     * The method returns the node to which the application camera can follow.
     * 
     * @return Node The node to which the application camera can follow.
     */
    public Node getCameraFollowsNode() {
        return nodeTurm_;
    }
    
    /**
     * The method returns an object appearing in the time of collision between 
     * the game character and some affected obstacle, and then disappearing after 
     * the effect of the game character on the obstacle has gone.
     * 
     * @return PhysicsGhostObject The collision object.
     */
    @Override
    public PhysicsGhostObject getCollisionObject() {
        return controlPanzer_;
        //return new PhysicsGhostObject(compoundPanzer_);
    }
    
    /**
     * The method stops the forward movement.
     */
    @Override
    public void stopForward() {
        isStopForward_ = true;
    }
    
    /**
     * The method stops the backward movement.
     */
    @Override
    public void stopBackward() {
        isStopBackward_ = true;
    }
    
    /**
     * The method stops the leftward movement.
     */
    @Override
    public void stopLeftward() {
        isStopLeftward_ = true;
    }
    
    /**
     * The method stops the rightward movement.
     */
    @Override
    public void stopRightward() {
        isStopRightward_ = true;
    }
    
    /**
     * The method stops the left forward movement.
     */
    @Override
    public void stopLeftForward() {
        isStopLeftForward_ = true;
    }
    
    /**
     * The method stops the right forward movement.
     */
    @Override
    public void stopRightForward() {
        isStopRightForward_ = true;
    }
    
    /**
     * The method stops the left backward movement.
     */
    @Override
    public void stopLeftBackward() {
        isStopLeftBackward_ = true;
    }
    
    /**
     * The method stops the right backward movement.
     */
    @Override
    public void stopRightBackward() {
        isStopRightBackward_ = true;
    }
    
    /**
     * The method stops the upward movement.
     */
    @Override
    public void stopUpward() {
        isStopUpward_ = false;
    }
    
    /**
     * The method stops the downward movement.
     */
    @Override
    public void stopDownward() {
        isStopDownward_ = false;
    }
    //
    // *************************** Package Methods *****************************
    //
    //
    // *************************** Protected Methods ***************************
    //
    //
    // *************************** Private Methods *****************************
    //
    /**
     * The method switches the march's smoke emitters and sounds.
     * 
     * @param isOnCharge The flag switching the march's smoke emitters and sounds.
     */
    private void charge(final boolean isOnCharge) {
        if (isOnCharge == true) {
            peExhaustLeft_.setLowLife(1.0f);
            peExhaustLeft_.setHighLife(2.0f);
            peExhaustRight_.setLowLife(1.0f);
            peExhaustRight_.setHighLife(2.0f);
            peExhaustBlastLeft_.setLowLife(3.0f);
            peExhaustBlastLeft_.setHighLife(4.0f);
            peExhaustBlastRight_.setLowLife(3.0f);
            peExhaustBlastRight_.setHighLife(4.0f);
            anodePzRunning_.play();
            anodePzStanding_.stop();
        } else {
            peExhaustLeft_.setLowLife(0.1f);
            peExhaustLeft_.setHighLife(0.2f);
            peExhaustRight_.setLowLife(0.1f);
            peExhaustRight_.setHighLife(0.2f);
            peExhaustBlastLeft_.setLowLife(0.1f);
            peExhaustBlastLeft_.setHighLife(0.2f);
            peExhaustBlastRight_.setLowLife(0.1f);
            peExhaustBlastRight_.setHighLife(0.2f);
            anodePzRunning_.stop();
            anodePzStanding_.play();
        }
    }
    //
    // *************************** Inner Classes *******************************
    //
    //
    // *************************************************************************
    //
    //
    // *************************************************************************
}
