# FLEISS Software Foundation

# DGA MMORPG

DGA is a free/libre and open-source, cross-platform massively multiplayer online role-playing video game featuring weapons. The first type of weapons being realized in DGA is land combat vehicles. DGA is designed to be cross-platform by used technologies and platforms, however, the DGA project aims at providing a full-featured MMORPG on Linux operating system.

## DGA Game Client Prototype

DGA Game Client's Prototype is intended to model, test, and work out the DGA game client's functions and modes running within the jMonkeyEngine game engine only.

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

#### Prerequisites

What things you need to install the software and how to install them.

#### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

### Technologies

Description of technologies used to build the program.

### Documentation

Information about the project's documentation.

### Contributing

Information for contributors.

### Versioning

Information about the current version.

### License

Licensing information.

### References

* Hat tip to anyone whose code was used
* Inspiration
* etc
